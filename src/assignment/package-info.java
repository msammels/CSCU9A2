/**
 * <p><b>Java Assignment</b></p>
 * <p><b>A simple interactive product sales database</b></p>
 * <p><b>
 * This description and starter code can be found on the module Canvas pages, and also on
 * {@code Groups on Wide\CSCU9A2\Assignment}
 * </b></p>
 * 
 * <b><i>Please read this document right through before starting your work.</i></b>
 * 
 * <p><b>Introduction</b></p>
 * 
 * In this assignment you will be completing a Java program that runs a very simple interactive product sales database – as
 * in the example shown: the program holds up to 10 product names and sales data. A table of the sales data is always
 * displayed, together with a coloured pie chart showing the relative proportions of the sales of the different products.
 * At the top of the window there are buttons and text fields for functions for managing the products and sales data – it
 * is the <i>methods and algorithms</i> that implement these functions behind the scene that you are required to complete.
 * 
 * <br /><br />
 * 
 * <img src="{@docRoot}/resources/assignment-img01.jpg" alt ="assignment-img01" height="25%" width="25%" />
 * 
 * <br /><br />
 * 
 * <i>
 * You are given an almost-complete Java program, <b>{@code ProductDatabase.java}</b>, on the Canvas Assignment page, and
 * in the folder <b>{@code Groups on Wide\CSCU9A2\Assignment}. You must take your own copy of this program to work on in a
 * BlueJ project in the usual way. In these locations you will also find a collection of images illustrating the intended
 * functions of the program, to be viewed in conjunction with the description that follows.</b>
 * </i>
 * 
 * <ul>
 *     <li>
 *     The user interface and event handling are fully complete and correct, and <i>you must not change them</i>.
 *     </li>
 *     <li>
 *     Behind the scenes, inside the program, the product database is held in a collection of arrays – again the arrays
 *     are complete, <i>and you must not change their declarations</i>.
 *     </li>
 *     <li>
 *     The event handlers for the advanced function buttons use <b>six different methods</b> whose purpose is to access
 *     or update the data held in the arrays (for example to add a new product, or to update the sales figure for a
 *     specified product). These methods are present in the code but are <i>incomplete</i>, and it is your task to complete
 *     as many of them as you can: to function correctly, and in good programming style. <b><i>The method headers for
 *     these methods must not be changed (as they are called from the event handlers), but you may add further helper
 *     methods if you wish</i></b>.
 *     </li>
 *     <li>
 *     There is a <b>seventh method</b> ({@link ProductDatabase#computePercentages computePercentages}) that is not
 *     called directly, but needs to be called from your methods to calculate the percentage sales of each product
 *     (see the example above). Again, this method is present in the code but is incomplete, and it is your task to
 *     complete it to function correctly, and in good programming style.
 *     </li>
 * </ul>
 * 
 * Your submission for the assignment should complete as many of the incomplete methods as possible — you do not need to
 * complete all of them in order to get credit. The seven methods contribute up to the following number of marks each
 * to the overall mark for the assignment:
 * 
 * <br /><br />
 * 
 * <img src="{@docRoot}/resources/assignment-img02.jpg" alt ="assignment-img02" height="50%" width="50%" />
 * 
 * <br /><br />
 * 
 * Your solution for each of the seven methods will be assessed according to the following criteria (based on the
 * University’s Common Marking Scheme):
 * 
 * <br /><br />
 * 
 * <img src="{@docRoot}/resources/assignment-img03.jpg" alt ="assignment-img03" height="50%" width="50%" />
 * 
 * <br /><br />
 * 
 * In judging whether the Java code that you have written is of “high quality” we shall take into account the following
 * <b>Assessment criteria</b>:
 * 
 * <ul>
 *     <li>correctness of operation,</li>
 *     <li>appropriate use of Java constructs,</li>
 *     <li>suitably informative choices of variable and method names,</li>
 *     <li>consistency, legibility and tidiness of program layout,</li>
 *     <li>effective use of comment text.</li>
 * </ul>
 * 
 * <p><b>Independent work</b></p>
 * 
 * <strong>
 * Note that we require that the work you submit is your own, independent work. “Plagiarism” means presenting the work of
 * others as your own. The University takes a very serious view of plagiarism, and the penalties can be severe. The
 * University has a formal policy on plagiarism which can be found at
 * 
 * <br /><br />
 * 
 * <a href="http://stir.ac.uk/1x0" target="_blank">http://stir.ac.uk/1x0</a>
 * </strong>
 * 
 * <br /><br />
 * 
 * However, this should not exclude discussion of general issues. Please read the “Note on Independent Work”
 * available via the CSCU9A2 Canvas assignment page. Be warned: copying or borrowing code does not necessarily guarantee 
 * quality product, and may damage your grade, as may showing or giving your code to someone else! We will be submitting
 * all the solutions to a “similarity” checking service, and <b>any apparent cases of collusion will be investigated
 * carefully, and the appropriate penalties applied</b>.
 *  
 * <p><b>General description of the problem</b></p>
 *  
 * The product sales database program contains a ‘database’ of products: their names, the most recent sales figure for the
 * product, and the percentage that product has of the total sales. The database is quite small in this version, but in
 * principle it could be quite large. Of course, in this exercise, the ‘database’ will be a little unrealistic: the
 * information is built-in to the program (whereas in a ‘serious’ system it would, perhaps, be read in from a file or held
 * in a proper database). The database inside the program is a parallel collection of arrays of Strings (for the names),
 * ints (for the sales figures) and floats (for the percentages). In order to avoid a product "number" of 0, the
 * arrays are only used from index 1 onwards. The values in the elements at index 1 of each array comprise the information
 * about one product, the values in elements at index 2 comprise the information about another product, and so on. There
 * is a single variable, {@code actualProducts}, that holds the number of products for which details are currently held in
 * the database, and a single variable, {@code totalSales}, that should always be updated to contain the current total of
 * all sales in the table.
 *  
 * User interface buttons are provided to allow the user:
 *  
 * <ul>
 *     <li>to add a new named product (with zero sales initially)</li>
 *     <li>to update the sales figure for a particular product</li>
 *     <li>to delete a specified product</li>
 *     <li>to clear all the sales data (but keeping the product names)</li>
 *     <li>to sort (re-order) the sales table in alphabetic order of product names</li>
 *     <li>to sort (re-order) the sales table in descending order of the sales figures</li>
 * </ul>
 *  
 * The <b>{@code newProductButton newProductButton} Add new product button</b> almost functions correctly – it does not
 * detect when the user is attempting to add another product to a full database. This is because the method called by the
 * button’s event handler is incomplete.
 * 
 * The other five buttons <i>do not</i> work: The user interface code for them is fine, but it calls core data processing
 * methods to carry out the necessary accesses/updates to the data in the arrays,
 * <b><i>and those methods require completing</i></b>. The incomplete methods are listed in the table at the top of page 2.
 * <b><i>You will find them in the code below line 473 (see {@code ///////////////////////////})</i></b>.
 * 
 * <p><b>You should insert your student number in place of 1234567 in line 111.</b></p>
 * 
 * <b>All other work is on the seven method bodies</b>: You must complete the
 * {@link ProductDatabase#addNewProduct addNewProduct} method, and implement full method bodies for
 * {@link ProductDatabase#computePercentages computePercentages}, {@link ProductDatabase#updateSalesAction updateSales},
 * {@link ProductDatabase#deleteProductAction deleteProduct}, {@link ProductDatabase#clearAllSales clearAllSales},
 * {@link ProductDatabase#sortByName sortByName} and
 * {@link ProductDatabase#sortBySalesDescending sortBySalesDescending}.
 * 
 * <br /><br />
 * 
 * <b>Note: You MUST NOT alter any other parts of the program</b>:
 * 
 * <ul>
 *     <li>the GUI parts are complete and correct</li>
 *     <li>the array declarations are complete and correct</li>
 *     <li>the method headers are complete and correct</li>
 * </ul>
 * 
 * <p><b>Advice — Incremental Development</b></p>
 * 
 * Be very wary of tackling programming using the “Big Bang” approach: don't try to create the entire final program before
 * entering it and testing out your ideas. That way lies frustration and poor progress.
 * 
 * We advise that you develop programs one step at a time – in this assignment perhaps one method at a time. At each step
 * you can design and enter a little bit more of the final program. Of course, when you run the program at each step it
 * won't do everything that the final program is supposed to, but you can check that the design so far is functioning
 * correctly. [Further, if you run out of time doing the assignment, then you can submit your most recent correctly
 * functioning version, confident that it does something properly — a large but non-functioning program is hard to give
 * credit for.] This is an important technique in developing big programs: we can build a complete, compilable and runnable
 * program without having designed all the details.
 * 
 * <br /><br />
 * 
 * The incremental development approach is valuable for various reasons: If you have a working version of the program and
 * you add only a little new code at a time, when something turns out to be faulty then it should be fairly clear where to
 * look for the fault! (Clearly, if you have just typed in 100 lines of Java and it doesn't work, then locating the fault
 * might be like looking for a needle in the proverbial haystack!) It is also very reassuring to see some facilities
 * coming “on-line” at an early stage — rather than working to complete the whole program before trying it out!
 * Good psychology and good methodology.
 * 
 * <p><b>Keeping backups of your work</b></p>
 * 
 * <b>I very strongly recommend</b> that you make backup copies of your work from time to time — perhaps after each
 * successfully completed development step, or more frequently such as every day. It is easy to do this: find the folder
 * <i>containing</i> your project folder, click <i>once</i> on the icon for your project folder to highlight it,
 * select <b>Copy</b> from the <b>Organize</b> menu (or right-click, <b>Copy</b>), and then select <b>Paste</b> from the
 * <b>Organize</b> menu. You will see a new folder appear called {@code Copy of ...} (or {@code Copy (2)...}, etc for
 * subsequent copies). If you ever need to backtrack, you can just copy your {@code .java} file from the most recent
 * {@code Copy...} folder to the main project folder. I suggest that you treat the {@code Copy ...} folders as your <i>safe
 * archive — leave them alone and continue your work in your main project folder</i>.
 * 
 * <br /><br />
 * 
 * <b>Keeping regular print-outs of your work can also be a life saver.
 * 
 * <br /><br />
 * 
 * In past years a number of students have found themselves in extremely distressing circumstances as a result of ignoring
 * the advice above</b>.
 * 
 * <br /><br />
 * 
 * Please remember that computers and related devices are not perfectly reliable, and people do make mistakes, so simple
 * precautions are appropriate. If you carry out this assignment work on your own computer, remember to keep backups on
 * removable disks, pen drives or in the cloud, but do not keep your only copy of your work on removable disks or
 * pen drives.
 * 
 * <p><b>Submission instructions</b></p>
 * 
 * You must submit your assignment work through Canvas: <i>you should submit exactly one file, your final version of
 * {@code ProductDatabase.java}</i>, through the assignment submission link on the CSCU9A2 assignment page before the
 * deadline stated above. [Note that this is not a Turnitin submission, as it does not deal with code, and so there will be
 * no originality report.]
 * 
 * <br /><br />
 * 
 * <i>We will compile and test your applications</i>, so you must make sure that what you submit compiles without
 * errors — we are <i>not</i> going to correct errors nor delete incomplete code in order to see if anything works
 * properly! You can delete or comment out incomplete code before submitting your application (check that it does then
 * compile). If you submit an application that doesn’t compile, then we will do our best to award some credit for what we
 * can see in the Java text, but you are unlikely to be awarded a mark higher than 40.
 * 
 * <br /><br />
 * 
 * <b>Anonymous marking</b>: you are required above to include your student number in a String in the
 * {@link ProductDatabase#main main} method, but you <b><i>must not</i></b> include your name nor your
 * username.
 * 
 * <p><b>Extensions and non-submission</b></p>
 * 
 * If you need an extension for a good reason (perhaps for medical reasons) then this can be arranged, but it must be
 * discussed with the module organizer, Dr Simon Jones, as soon as possible after the problem becomes known.
 * 
 * <br /><br />
 * 
 * In accordance with University policy, assessed coursework submitted late will be accepted up to seven days after the
 * submission date (or expiry of any agreed extension) but the mark will be lowered by three marks per day or part thereof.
 * After seven days the piece of work will be deemed a non-submission, the student will have failed to meet module
 * requirements, <i>and this will result in the award of No Grade for the module as a whole</i>.
 */
package assignment;