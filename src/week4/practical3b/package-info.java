/**
 * <p><b>Graphical User Interfaces 2</b></p>
 * 
 * <hr />
 * 
 * <p><b>JSlider Documentation</b></p>
 * 
 * Remember that information about Swing <b>{@code JSlider}</b> widgets can be found on the Oracle Java website: in
 * BlueJ select the <b>Help</b> menu -> <b>Java Class Libraries</b>, wait for the browser window to appear, scroll
 * down on the lower left frame to find <b>{@code JSlider}</b> and click on it - the documentation will appear in
 * the right hand frame.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Sliders: The WindowBlind program</b></p>
 * 
 * At this step, you will build a new project with a main program class that displays a simple empty frame, and then
 * experiment with adjusting its settings. Finally you will extend it by adding three text fields to display two
 * numbers and their sum.
 * 
 * <br /><br />
 * 
 * First you need to create a new basic application from an existing file as you have done before:
 * 
 * <ul>
 *     <li>
 *     Create a new <code><b>WindowBlind</b></code> folder in your CSCU9A2 working folder. Copy the file 
 *     <code><b>WindowBlind.java</b></code> from the <code><b>Groups on Wide (V:)\CSCU9A2\Java</b></code> to your
 *     <code><b>WindowBlind</b></code> folder.
 *     </li>
 *     <li>
 *     Launch BlueJ. Using the <b>Project</b> menu, <b>Open Non BlueJ...</b> create a BlueJ project in your
 *     <code><b>WindowBlind</b></code> folder - remember, when you navigate to it click once on the folder name then 
 *     <b>Open in BlueJ</b> - do not double click to open the folder.
 *     </li>
 *     <li>Compile and run the program.</li>
 * </ul>
 * 
 * This is an example from the early editions of Java for Students. It draws a picture of a window (just a rectangular
 * frame), and a blind is moved up and down with the window as a slider is adjusted. The blind is drawn by a
 * {@code fillRect}, with the height of the filled rectangle being determined by the slider's value. All the
 * drawing is carried out in the {@code paintScreen} method, which is called automatically when a screen refresh
 * occurs.
 * 
 * <br /><br />
 * 
 * Examine the program, and note its structure. It has a {@code stateChanged} method, where interactive events are
 * handled, and {@code paintScreen} method that actually draws on the graphics panel. It also has the usual two
 * other methods, {@code main} and {@code createGUI}, which launch the program and set up the graphical user
 * interface.
 * 
 * <br /><br />
 * 
 * Here are some more experiments for you to try:
 * 
 * <ul>
 *     <li>Change the slider so that it is vertical (guess how!). Choose which orientation you prefer.</li>
 *     <li>
 *     Draw the window <i>outline</i> in black and the <i>adjustable blind</i> in a <i>different colour</i>. You may
 *     need to exchange the sequential order of the {@code drawRect} and the {@code fillRect} to make the best
 *     picture <b><i>with the black outline fully visible</i></b> - try both orders.
 *     </li>
 *     <li>
 *     Try altering the <i>initial setting</i> of the slider when it is constructed in {@code createGUI} (the fourth
 *     parameter of the {@code new JSlider} constructor): for example, 9 or 100. Look at the effect in each case.
 *     [Change back to 50 afterwards.]
 *     </li>
 *     <li>
 *     Try altering the <i>range</i> of values for the slider when its constructed in {@code createGUI} (the second
 *     and third parameters of the {@code new JSlider} constructor): for example: 20-80 or 0-150 - in each case you
 *     will also need to choose an appropriate <i>initial</i> setting for the slider. Look at the effect in each case.
 *     [Change back to 0-100 afterwards.]
 *     </li>
 * </ul>
 * 
 * Now you are going to create a new method {@code drawWindow} to draw a window frame and its blind:
 * 
 * <ul>
 *     <li>
 *     The method header line should be:
 *     
 *     <pre>
 *     private void drawWindow(Graphics g, Color c,
 *                             int x, int y, int level) {
 *     </pre>
 *     
 *     The parameters are going to be used as follows: {@code g} is the usual graphics area information, {@code c}
 *     gives the colour to be used for the blind ({@code Color} is a <i>type</i> name - you can also have variables
 *     of this type), {@code x} and {@code y} are the position where the top left corner should be drawn, and
 *     {@code level} gives the value indicating how far down the blind is to be filled.
 *     </li>
 *     <li>
 *     You can obtain the program statements for the <i>body</i> of {@code drawWindow} by <i>moving</i> the code
 *     from the body of {@code paintScreen} into the body of {@code drawWindow}.
 *     </li>
 *     <li>
 *     To make sure that the method's parameters are used correctly by the method body, you must make minor edits
 *     to introduce the parameter names where appropriate: {@code x} should be used in place of {@code 120};
 *     {@code y} should be used in place of {@code 80}; {@code level} should be used instead of {@code blindHeight}
 *     for drawing the blind rectangle; and the parameter {@code c} should be used in {@code g.setColor} instruction
 *     just before the filled bind rectangle is drawn instead of an explicit colour.
 *     </li>
 *     <li>
 *     Also add a statement to the body of {@code paintScreen} to call {@code drawWindow} instead of drawing the
 *     window explicitly. You will need a call like this:
 *     
 *     <pre>drawWindow(g, Color.red, 120, 80, blindHeight);</pre>
 *     
 *     where the appropriate information for the window and blind to be drawn are given as <i>actual parameters</i>.
 *     The output should look the same as before.
 *     </li>
 * </ul>
 * 
 * Now, by adding a second call of your {@code drawWindow} method, draw a second window on the screen, controlled
 * by the same slider, <b><i>but with the second blind drawn in a different colour, and at a different position on
 * the screen</i></b>. The two  blinds should move up and down together as you adjust the slider - compile and make
 * sure that the program runs properly.
 * 
 * <br /><br />
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Adding a Second Slider</b></p>
 * 
 * In this exercise, you will enable the two blinds to be controlled separately:
 * 
 * <br /><br />
 * 
 * Add a <i>new slider</i> to control the <i>second</i> window blind, and alter {@code paintScreen} to draw the two
 * windows using one slider to control each. Here is how to do it:
 * 
 * <br /><br />
 * 
 * You will have to declare a new global variable at this step for the new slider itself, say {@code slider2}. <br />
 * You will have to set up the slider in {@code createGUI}: creating it with appropriate constructor parameters,
 * adding it to the display, and registering the program as responding to its adjustment events. <br />
 * Alter {@code paintScreen} so that the second window blind is controlled by the <i>second slider</i>. <br />
 * <i>You can make these changes by carefully copying, pasting and editing the code already present</i> - be careful
 * to make all the necessary edits to the pasted code!
 * 
 * <br /><br />
 * 
 * Introduce some {@code JLabel} widgets to identify the different sliders. You might need to widen the program
 * window by changing the panel and frame sizes.
 * 
 * <br /><br />
 * 
 * Add diagnostic statements (using {@code System.out.println}) <i>just at the start and just at the end</i> of
 * <b>each</b> method body, displaying messages like "Entering stateChanged" and "Returning from stageChanged".
 * 
 * <br /><br />
 * 
 * When you run the program you will now see a complete summary of the "dynamic" path of execution through all of the
 * methods, displayed in the BlueJ terminal window.
 * 
 * <br /><br />
 * 
 * <b>Remember this trick</b>. It's very helpful when you are developing your own programs, to give you some idea of
 * what is happening at runtime (especially if your program isn't working properly!)
 * 
 * <br /><br />
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * @author  Michael Sammels
 * @version 07.02.2019
 * @since   1.0
 */
package week4.practical3b;