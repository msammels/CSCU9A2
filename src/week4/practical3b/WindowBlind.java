package week4.practical3b;

import java.awt.*;
import java.io.Serializable;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * CSCU9A2 - Week 4 <br />
 * Practical 3B <br />
 * <code>WindowBlind.java</code>
 * 
 * <p>
 * This program is a Java application showing a single slider and drawing a window frame with a blind adjustable
 * using this slider.
 * </p>
 * 
 * @author  Michael Sammels
 * @version 07.02.2019
 * @since   1.0
 */

public class WindowBlind extends JFrame implements ChangeListener, Serializable {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = -8709130476676756732L;

    /**
     * Constructor.
     */
    public WindowBlind() {}
    
    /**
     * This is one of the sliders that appear in the display.
     */
    private JSlider sliderA;
    
    /**
     * This is one of the sliders that appear in the display.
     */
    private JSlider sliderB;
    
    /**
     * This holds the current height of the first blind, initially 50.
     */
    private int blindHeightA = 50;
    
    /**
     * This holds the current height of the second blind, initially 50.
     */
    private int blindHeightB = 50;
    
    /**
     * The main launcher method.
     * This is the main launch action for the {@link WindowBlind} program.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        // Diagnostics
        System.out.println("Entering main method...");
        
        WindowBlind frame = new WindowBlind();
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);
        frame.createGUI();
        frame.setVisible(true);
        
        // Diagnostics
        System.out.println("Returning from main method...");
    }
    
    /**
     * The {@link #createGUI} method is called by the {@link #main} method to set up the graphical user interface.
     */
    private void createGUI() {
        // Diagnostics
        System.out.println("Entering createGUI method...");
        
        // Set up the main window characteristics
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new FlowLayout());
        
        // Create the adjustable slider, with correct initial settings
        sliderA = new JSlider(JSlider.HORIZONTAL, 0, 100, blindHeightA);
        sliderB = new JSlider(JSlider.HORIZONTAL, 0, 100, blindHeightB);
        
        // These are the labels for the sliders
        JLabel labelA = new JLabel("Slider A"), labelB = new JLabel("Slider B");
        
        sliderA.setName("sliderA"); sliderB.setName("sliderB");
        
        window.add(sliderA); window.add(labelA);
        window.add(sliderB); window.add(labelB);
        
        sliderA.addChangeListener(this); sliderB.addChangeListener(this);
        
        // Create the panel for drawing on
        JPanel panel = new JPanel() {
            private static final long serialVersionUID = 5579969259777501267L;

            // paintComponent is called automatically whenever a screen refresh is needed
            public void paintComponent(Graphics g) {
                // g is a cleared panel area
                super.paintComponent(g);    // Paint the panel's background
                paintScreen(g);             // Then the required graphics
            }
        };
        
        panel.setPreferredSize(new Dimension(300, 300));
        panel.setBackground(Color.cyan);
        window.add(panel);
        
        // Diagnostics
        System.out.println("Returning from createGUI method...");
    }
    
    /**
     * This method is used for creating and drawing our blinds.
     * @param g     the panel to be painted.
     * @param c     the colour to be painted.
     * @param x     the position the top left corner should be drawn in.
     * @param y     the position the top left corner should be drawn in.
     * @param level how far down the blind is to be filled.
     */
    private void drawWindow(Graphics g, Color c, int x, int y, int level) {
        // Diagnostics
        System.out.println("Entering drawWindow method...");
        
        g.setColor(c);
        g.drawRect(x,  80,  60,  100);
        g.fillRect(x,  80,  60,  level);
        
        // Diagnostics
        System.out.println("Returning from drawWindow method...");
    }

    /**
     * Redraw the graphics panel when the screen is refreshed: a window frame with a blind partially covering the
     * opening.
     * @param g the panel to be painted.
     */
    private void paintScreen(Graphics g) {
        // Diagnostics
        System.out.println("Entering paintScreen method...");
        
        drawWindow(g, Color.red, 120, 80, blindHeightA);    // First slider
        drawWindow(g, Color.blue, 20, 80, blindHeightB);    // Second slider
        
        // Diagnostics
        System.out.println("Returning from printScreen method...");
    }

    /**
     * The {@link #stateChanged} method is called automatically when the slider is adjusted: fetch the setting,
     * update {@code blindHeightA} and {@code blindHeightB} and refresh the screen.
     */
    public void stateChanged(ChangeEvent e) {
        // Diagnostics
        System.out.println("Entering stateChanged method...");
        
        JSlider source;
        source = (JSlider)e.getSource();
        
        if (source.getName().equals("sliderA")) {
            blindHeightA = sliderA.getValue();
        } else if (source.getName().equals("sliderB")) {
            blindHeightB = sliderB.getValue();
        }
        repaint();  // Forces a screen refresh
    }
}
