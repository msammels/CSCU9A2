/**
 * <p>Practicals 3A and 3B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 3A</b><br />
 * This practical contains first exercises in building Java applications with Graphical User Interfaces (GUIs) -
 * the usual way that we are all used to using computer software. In the first exercise you will experiment with a
 * basic application with an empty window (frame), and then build a simple GUI for an application that prompts
 * the user in BlueJ's <b><i>terminal window</i></b> for two numbers and then displays them and their sum in a GUI
 * window. In the second exercise, you will extend the event-driven Car Park counter application. 
 * 
 * <br /><br />
 * 
 * <b>Practical 3B</b><br />
 * This practical is based on the WindowBlind examples. You will experiment with the event handling associated with
 * a {@code JSlider} and then extend the program with an extra window blind. Then you will extend the application with
 * a second slider to control the second blind separately.
 * 
 * @author  Michael Sammels
 * @version 04.02.2019 - 07.02.2019
 * @since   1.0
 */
package week4;