package week4.practical3a;

import java.awt.*;
import java.io.Serializable;
import java.util.Scanner;
import javax.swing.*;

/**
 * CSCU9A2 - Week 4 <br />
 * Practical 3A <br />
 * <code>SimpleFrame.java</code>
 * 
 * <p>This program display an empty frame.</p>
 * 
 * @author  Michael Sammels
 * @version 04.02.2019
 * @since   1.0
 */

public class SimpleFrame extends JFrame implements Serializable {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = -7646085818174371699L;

    /**
     * Constructor.
     */
    public SimpleFrame() {}
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        SimpleFrame frame = new SimpleFrame();
        
        final int FRAME_WIDTH = 640, FRAME_HEIGHT = 480;
        
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.createGUI();
        frame.setVisible(true);
    }
    
    /**
     * This method sets up the graphical user interface.
     */
    private void createGUI() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new FlowLayout());
        
        JTextField textA, textB, textC;
        
        // Nothing in the window yet!
        textA = new JTextField(15);
        textA.setFont(new Font("Arial", Font.BOLD, 40));
        textA.setEditable(false);
        
        textB = new JTextField(15);
        textB.setFont(new Font("Arial", Font.BOLD, 40));
        textB.setEditable(false);
        
        textC = new JTextField(15);
        textC.setFont(new Font("Arial", Font.BOLD, 40));
        textC.setEditable(false);
        
        window.add(textA); window.add(textB); window.add(textC);
        
        /*
         * Variables to hold sum declaration
         * 
         * a = first value, b = second value, c = sum of a + b
         */
        int a, b, c;
        
        // Pulling in the numbers
        a = readInteger(); b = readInteger();
        
        // Calculating the sum
        c = a + b;
        
        textA.setText("a is " + a); textB.setText("b is " + b); textC.setText("c is " + c);
    }

    /**
     * A standard method:
     * 
     * <br /><br />
     * 
     * Display the prompt and then read the lines repeatedly until a valid integer is found, and return it.
     * @return  A valid integer.
     */
    private static int readInteger() {
        try (Scanner scan = new Scanner(System.in)) {
            // Ask the user to enter an integer
            System.out.println("Enter an integer: ");
            while (!scan.hasNextInt()) {
                scan.next();
            }
            
            // Store the input in a variable
            return scan.nextInt();
        }
    }
}
