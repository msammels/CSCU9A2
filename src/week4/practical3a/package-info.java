/**
 * <p><b>Graphical User Interfaces 1</b></p>
 * 
 * <hr />
 * 
 * <p><b>How to find essential Java Documentation</b></p>
 * 
 * All Java programmers make frequent reference to the Java API documentation (API means: "Application Programming
 * Interface"). This is a comprehensive, but sometimes hard to navigate, resource containing a detailed description
 * of all the Java library classes, and all the methods that they contain. 
 * <span style="font-variant: small-caps">Unfortunately it is <b>NOT</b> a tutorial about Java</span>.
 * 
 * <br /><br />
 * 
 * You will find the API documentation useful if you cannot remember precisely what a library method is called, or
 * what parameters it requires, or just to explore for other useful methods.
 * 
 * <br /><br />
 * 
 * To access the documentation when using BlueJ: in the <b>Help</b> menu select <b>Java Class Libraries</b>. This
 * will launch a web browser - you will be connected to Oracle's Java website, and a dazzling screen of information
 * will be displayed. Scroll down the long list in the pane at the lower to find the <i>class</i> that you are
 * interested in - for example, look for <b>{@link java.util.Scanner Scanner}</b>, which you know quite well (you think!).
 * Click on the class name when you find it to see the documentation in the main pane to the right. Take a look at
 * it now - just a quick look to see what it's like - there is too much there to read! The 
 * <b>{@link java.util.Scanner Scanner}</b> class does start with a reasonably useful tutorial section, but that is not
 * common (some descriptions contain links to Oracle's actual Java tutorials). Then there is a Method Summary that
 * lists all the methods available to be used with <b>{@link java.util.Scanner Scanner}</b> (for example: 
 * <b>{@code hasNext}</b>, <b>{@code nextInt}</b>, etc.), and each method name is a hot-link to a more detailed
 * description further down the page.
 * 
 * <br /><br />
 * 
 * You can see that the documentation is extensive, detailed and complex (much like this one). It is best to use it
 * for looking up the details of something that you already know about but of which you have forgotten the details.
 * Remember to look at the API documentation when it might help.
 * 
 * <br /><br />
 * 
 * In the exercises below you will see another way to find out information about the classes you are working with.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Experimenting with an Empty Frame - no events yet!</b></p>
 * 
 * At this step you will build a new project with a main program class that displays a simple empty frame, and then
 * experiment with adjusting its settings. Finally you will extend it by adding three text fields to display two
 * numbers and their sum.
 * 
 * <br /><br />
 * 
 * Create a new <code><b>SimpleFrame</b></code> folder in your CSCU9A2 working folder. Copy the file 
 * <code><b>SimpleFrame.java</b></code> from <code><b>Groups on Wide (V:)\CSCU9A2\Java</b></code> to your
 * <code><b>SimpleFrame</b></code> folder (or copy and paste the code below).
 * 
 * <pre>
 * import java.awt.*;
 * import java.awt.event.*;
 * import javax.swing.*;
 * 
 * {@literal} /**
 *  * This program displays an empty frame
 *  *&#47;
 * public class SimpleFrame extends JFrame
 * {
 *     {@literal /**}
 *      * The main launcher method
 *      *&#47;
 *     public static void main(String[] args)
 *     {
 *         SimpleFrame frame = new SimpleFrame();
 *         
 *         final int FRAME_WIDTH = 30;
 *         final int FRAME_HEIGHT = 40;
 *         
 *         frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
 *         frame.setTitle("An empty frame");
 *         
 *         frame.createGUI();
 *         
 *         frame.setVisible(true);
 *     }
 *     
 *     {@literal /**}
 *      * This method sets up the graphical user interface.
 *      *&#47;
 *     private void createGUI()
 *     {
 *         setDefaultCloseOperation(EXIT_ON_CLOSE);
 *         Container window = getContentPane();
 *         window.setLayout(new FlowLayout());
 *         
 *         // Nothing in the window yet!
 *     }
 * }
 * </pre>
 * 
 * Launch BlueJ. Using the <b>Project</b> menu, <b>Open Non BlueJ...</b> create a BlueJ project in your 
 * <code><b>SimpleFrame</b></code> folder - remember, when you navigate to it, click <b><i>once</i></b> on the
 * folder name then <b>Open</b> in BlueJ - do not double click to open the folder.
 * 
 * <br /><br />
 * 
 * Open the <code><b>SimpleFrame</b></code> class in BlueJ's editor and take a look at the program.
 * 
 * <br /><br />
 * 
 * Compile the program and run it in the usual way. A <i>very small</i> and rather unsatisfactory window will appear
 * at the top left of the screen - only just large enough to hold the required buttons in the window's title bar
 * (no space even for the title to be displayed). When you have finished inspecting it, close it by clicking the
 * X at the top right.
 * 
 * <br /><br />
 * 
 * Here are some adjustments to try out - experiment with them, <i>recompiling and running after each change</i>:
 * 
 * <ul>
 *     <li>
 *     Alter the values given to the two variables {@code FRAME_WIDTH} and {@code FRAME_HEIGHT} to give a reasonable
 *     size window. (You should then see the title displayed).
 *     </li>
 *     <li>Give the window a new title.</li>
 *     <li>
 *     There is another {@code JFrame} library method, {@code setLocation}, that expects to {@code int} parameters
 *     for an x and y coordinate identifying the point on the screen where the top left corner of the window is to
 *     be placed. Use a call of this method position the window somewhere central on the screen.
 *     </li>
 *     <li>
 *     Rather than use explicit numbers for the parameters of {@code setLocation}, follow the pattern illustrated by
 *     the {@code setSize} method call: declare two new variables, assign them values, and use them as the actual
 *     parameters of {@code setLocation}. Make sure that you choose sensible names for the variables (simply calling
 *     them {@code x} and {@code y} is not informative enough). [<i>Note: the modifier keyword {@code final} indicates
 *     that these are not really "variables" as this is the final assignment that is allowed to them. In effect,
 *     they are just names for specific values - often called "constants". Providing informative names for "key
 *     values" like this within a program is good practice</i>.]
 *     </li>
 *     <li>
 *     To see what happens, move the {@code setVisible} method call to just after the statement that creates the
 *     {@code new SimpleFrame}. When you launch the program, you might see the window jumping around a bit before
 *     settling down (it depends on the computer's speed and other factors - the effect may be more visible when you
 *     are adding more items to the frame later on, so try again then)! Move the {@code setVisible} back to where
 *     it was.
 *     </li>
 *     <li>
 *     In the method {@code createGUI}, alter the parameter of {@code setDefaultCloseOperation} to
 *     {@code DO_NOTHING_ON_CLOSE}. When you run the program, it will now not be possible to close it by clicking the
 *     X in the top right corner! You can close the program by right clicking on the stripy bar in BlueJ's main
 *     window and selecting <b>Reset Java Virtual Machine</b> - if you are running Java at the command prompt, then
 *     typing Ctrl+C will close the program. If you omit the {@code setDefaultCloseOperation} completely (e.g. by
 *     placing comment bars before it), then the default action occurs which is {@code HIDE_ON_CLOSE} - the window
 *     disappears but the program is still running behind the scenes, which perhaps seems rather strange!
 *     </li>
 *     <li>
 *     You can play with the "hidden but still running" property like this: <br />
 *     After the {@code setVisible} call in {@code main}, <b>type</b> in these lines:
 *     
 *     <pre>
 *     int n = 0;
 *     while (true)
 *     {
 *         System.out.println(n++);
 *     }
 *     </pre>
 *     
 *     This is an infinite loop that will count up continuously in BlueJ's terminal window. When the default close
 *     operation is {@code EXIT_ON_CLOSE}, then the counting stops when you click the X. When it is
 *     {@code HIDE_ON_CLOSE}, then the counting <i>continues</i> even though the window has disappeared!
 *     
 *     <br /><br />
 *     
 *     Try it, then delete, or comment out the infinite loop, and reinstate {@code EXIT_ON_CLOSE}.
 *     </li>
 *     <li>
 *     At the start of a new line anywhere in {@code main}, <b>type</b> {@code frame.} (the dot . is essential!) then
 *     <b>Ctrl+Space</b>. BlueJ will automatically look up all the possible methods that could be used at that point
 *     (this is, {@code JFrame} library methods), and display them in a scrolling list - it might take a short
 *     while. Click twice and a method call will be inserted in your program, with hints where the parameters are
 *     required.
 *     </li>
 *     <li>
 *     Look at the many <b>{@code JFrame}</b> methods that are available. Many are very obscure, but some
 *     interesting/useful ones are {@code setResizable(true/false)}, {@code setAlwaysOnTop(true/false)},
 *     {@code setUndecorated(true/false)} - click once on each in the list to see their descriptions, and try them
 *     out. {@code setUndecorated(true)} has the unfortunate effect of showing a window with no X to click! To close
 *     such a window, right click the stripy bar in BlueJ's main window and select <b>Reset Java Virtual Machine</b>.
 *     </li>
 * </ul>
 * 
 * Now to make the application a little more interesting: it will input two integers from the terminal, and display
 * their sum in the GUI window:
 * 
 * <br /><br />
 * 
 * Find the method from one of your previous projects which when called returns an integer from the console (terminal)
 * input, and paste a copy of it into this program. You will need to add the <b>{@code import}</b> statement for the
 * <b>{@link java.util.Scanner Scanner}</b> library class - look it up in a previous example.
 * 
 * <br /><br />
 * 
 * Just before the end of <b>{@code createGUI}</b>, add statements to declare two new {@code int} variables
 * (say {@code a} and {@code b} and read values into them (with a prompt followed by two calls of the method that you
 * pasted in the previous step). Declare one further {@code int} variable and assign it the same of the first two.
 * 
 * <br /><br />
 * 
 * Now, following the pattern shown in the <b>{@link CarPark#CarPark CarPark}</b> example, add to the empty frame
 * <i>three</i> new {@code JTextField} that initially display fifteen spaces as their text. Compile and run to make sure
 * that you see the three text fields.
 * 
 * <br /><br />
 * 
 * Now, using the {@code setText} method of a {@code JTextField}, add <i>three</i> statements like this to display
 * the two input values and their sum in the three text fields: {@code tf.setText("a is " + a);}
 * 
 * <br /><br />
 * 
 * Make sure that the window is an appropriate size, and has an appropriate title.
 * 
 * <br /><br />
 * 
 * Compile and test.
 * 
 * <br /><br />
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * <br /><br />
 * 
 * To see the effect of the <b>{@code FlowLayout}</b> manager at work, drag the bottom right corner of the program's
 * window to make the window wider, and then narrower and taller.
 * 
 * <br /><br />
 * 
 * To see what happens if you <i>do not</i> request <b>{@code FlowLayout}</b>: comment out the {@code setLayout}
 * statement in {@code createGUI}. Compile and run: you will see just <i>one</i> button, the last one, and it occupies
 * the whole window! The default layout manager for a <b>{@code JFrame}</b> is <b>{@code BorderLayout}</b>: this
 * places items in the NORTH, EAST, SOUTH, WEST and CENTER (CENTER is the default). If you would like to see how to
 * use <b>{@code BorderLayout}</b>, you can look it up in the API documentation.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Extending the Car Park counter application</b></p>
 * 
 * In this exercise, you will extend the functionality of an event-driven GUI:
 * 
 * <br /><br />
 * 
 * Create a new <code><b>CarPark</b></code> folder in your CSCU9A2 working folder. Copy the file
 * <code><b>CarPark.java</b></code> from <code><b>Groups on Wide (V:)\CSCU9A2\Java</b></code> to your
 * <code><b>CarPark</b></code> folder (or copy and paste the code below).
 * 
 * <pre>
 * import java.awt.*;
 * import java.awt.event.*;
 * import javax.swing.*;
 * 
 * {@literal /**}
 *  * This programs counts the cars entering and leaving a car park, always displaying
 *  * the current number of cars in the car park.
 *  *&#47;
 * public class CarPark extends JFrame implements ActionListener
 * {
 *     {@literal /**}
 *      * Globally accessible counter for the number of cars in the car park
 *      *&#47;
 *     private int carCount = 0;
 *     
 *     {@literal /**}
 *      * Buttons to simulate cars entering and leaving the car park.
 *      *&#47;
 *     private JButton enter, exit;
 *     
 *     {@literal /**}
 *      * Text field where the current number of cars is displayed
 *      *&#47;
 *     private JTextField text;
 *     
 *     {@literal /**}
 *      * The main launch method
 *      *&#47;
 *     public static void main(String[] args)
 *     {
 *         CarPark frame = new CarPark();
 *         frame.setSize(300, 200);
 *         frame.setLocation(150, 150);
 *         frame.setTitle("Car Park");
 *         frame.createGUI();
 *         frame.setVisible(true);
 *     }
 *     
 *     {@literal /**}
 *      * Helper method to build up the GUI
 *      *&#47;
 *     private void createGUI()
 *     {
 *         setDefaultCloseOperation(EXIT_ON_CLOSE);
 *         Container window = getContentPane();
 *         window.setLayout(new FlowLayout());
 *         
 *         enter = new JButton("Car Entering");
 *         window.add(enter);
 *         enter.addActionListener(this);
 *         
 *         text = new JTextField("0   ");
 *         text.setFont(new Font("Arial", Font.BOLD, 40));
 *         window.add(text);
 *         
 *         exit = new JButton("Car Exiting");
 *         window.add(exit);
 *         exit.addActionListener(this);
 *     }
 *     
 *     {@literal /**}
 *      * React to a GUI button press by adjusting the car counter correctly,
 *      * and then updating the counter display
 *      *&#47;
 *     public void actionPerformed(ActionEvent event)
 *     {
 *         if (event.getSource() == enter)
 *         {
 *             carCount = carCount + 1;
 *         }
 *         if (event.getSource() == exit)
 *         {
 *             carCount = carCount-1;
 *         }
 *         text.setText(Integer.toString(carCount));
 *     }
 * }
 * </pre>
 * 
 * Launch BlueJ. Using the <b>Project</b> menu, <b>Open Non BlueJ...</b> create a BlueJ project in your <b>CarPark</b>
 * folder - remember, when you navigate to it, click <b><i>once</i></b> on the folder name then <b>Open in BlueJ</b> -
 * do not double click to open the folder.
 * 
 * <br /><br />
 * 
 * Open the <b>{@code CarPark}</b> class in BlueJ's editor and take a look at the program.
 * 
 * <br /><br />
 * 
 * Compile the project and run it. A window is displayed with two buttons and a text field. When you click either
 * button, the number displayed in the text field changes as specified in the {@code actionPerformed} method.
 * 
 * <br /><br />
 * 
 * A simple experiment: put a comment marker at the start of <i>each line</i> of {@code ...addActionListener...} in
 * <b>{@code createGUI}</b>, recompile and test. Now the program should <i>not</i> react when you click the button.
 * Remove the comment markers. Those statements are necessary to configure the buttons so that they notify the
 * program when they are clicked.
 * 
 * <br /><br />
 * 
 * Improvise the following extensions:
 * 
 * <ul>
 *     <li>Add two more buttons and a text field for recording minibuses separately from cars.</li>
 *     <li>
 *     Add a text field displaying the number of <i>spaces remaining</i> in the car park - you decide what the
 *     overall number of spaces is.
 *     </li>
 * </ul>
 * 
 * Compile and test.
 * 
 * <br /><br />
 * 
 * For your interest: adapt so that when the car park is full, no further vehicles can enter, where there are no cars
 * in the car park, none can leave, and where there are no minibuses in the car park, none can leave (the button clicks
 * should cause no change to the counters).
 * 
 * <br /><br />
 * 
 * Investigate disabling and enabling the buttons as an alternative technique to prevent cars entering and leaving
 * the car park.
 * 
 * <br /><br />
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * @author  Michael Sammels
 * @version 04.02.2019
 * @since   1.0
 */
package week4.practical3a;