/**
 * <p>Practical 5B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 5B</b><br />
 * In this practical, you will build an interactive GUI for a simple address book. Then you will carry out a series
 * of array exercises.
 * 
 * @author  Michael Sammels
 * @version 28.02.2019
 * @since   1.0
 */
package week7;