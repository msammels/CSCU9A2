/**
 * <p>Practical 6A</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 6A</b><br />
 * Today we have a "design tour" of the Java development tool <b>BlueJ</b>, where we take a close look at BlueJ to see
 * examples of design principles in real use. Thus this practical has the following aims and objectives:
 * 
 * <ul>
 *     <li>To increase your understanding of User Accessibility by experiencing examples of design in action.</li>
 *     <li>To let you see how design principles correspond with good Java programming style.</li>
 * </ul>
 * 
 * For handy ease of reference, here at the <i>Computing Design Principles</i>:
 * 
 * <ol>
 *     <li>
 *     Let the User Feel in Control
 *     <ul>
 *         <li>Clear layout</li>
 *         <li>Flexibility/choices</li>
 *         <li>Shortcuts</li>
 *         <li>Feedback</li>
 *     </ul>
 *     </li>
 *     <li>
 *     Don't Overload the User's Memory
 *     <ul>
 *         <li>Consistency</li>
 *         <li>Help files</li>
 *     </ul>
 *     </li>
 *     <li>
 *     Try to Prevent and Fix Errors
 *     <ul>
 *         <li>Undo</li>
 *     </ul>
 *     </li>
 *     <li>Know Thy User</li>
 * </ol> 
 * 
 * @author  Michael Sammels
 * @version 04.03.2019
 * @since   1.0
 */
package week8;