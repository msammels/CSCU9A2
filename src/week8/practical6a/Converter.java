package week8.practical6a;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.Serializable;

/**
 * CSCU9A2 - Week 8 <br />
 * Practical 6A <br />
 * <code>Converter.java</code>
 * 
 * <p>This is a simple temperature converter program.</p> 
 * 
 * @author  Michael Sammels
 * @version 04.03.2019
 * @since   1.0
 */

public class Converter extends JFrame implements ActionListener, Serializable {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = -1587251893460864967L;

    /**
     * Constructor.
     * 
     * <br /><br />
     * 
     * Now to create the GUI interface, using Swing components.
     */
    public Converter() {
        /*
         * Using BorderLayout means that widgets can either be added to the centre of this panel or around 
         * the edges
         */
        JPanel guiPanel = new JPanel();
        guiPanel.setLayout(new BorderLayout());
        
        /*
         * This "invisible panel" will hold the button and text field and it will lay components out from left
         * to right
         */
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());
        
        convertTemp = new JButton(new ImageIcon("fc.gif"));
        
        convertTemp.addActionListener(this);    // Now we can listen to events from our button
        topPanel.add(convertTemp);              // Adding the converTemp button to the top panel
        
        tempF = new JTextField(10);
        tempF.addActionListener(this);          // Now we can listen to events from our text field
        topPanel.add(tempF);                    // Adding it to the top panel
        
        // GUI components for font size adjustment
        
        // The label displaying the current font size
        JLabel fontLabel = new JLabel("Font size: ", SwingConstants.NORTH);
        topPanel.add(fontLabel);
        
        // Listens out for the action events which get generated when we select from the choices
        fontSizeChoice.addActionListener(this);
        
        topPanel.add(fontSizeChoice);
        
        // This adds the top panel to the top of the GUI
        guiPanel.add(topPanel, BorderLayout.NORTH);
        resultLabel = new JLabel("", SwingConstants.CENTER);
        
        // The result label will appear in the centre of the GUI
        guiPanel.add(resultLabel, BorderLayout.CENTER);
        
        // Adds the panel to the application
        getContentPane().add(guiPanel);
    }
    
    /**
     * An array of font sizes we might use.
     */
    String[] fontSizes = {"8", "10", "11", "12", "14", "16", "18", "20"};
    
    /**
     * Creating the text field to put our temperature into.
     */
    JTextField tempF;
    
    /**
     * This label is where the result of the temperature conversion will appear.
     */
    JLabel resultLabel;
    
    /**
     * This is the button we'll press to convert the temperature from Fahrenheit to Celsius.
     */
    JButton convertTemp;
    
    /**
     * This {@link javax.swing.JComboBox JComboBox} offers a drop-down menu of choices.
     */
    JComboBox<String> fontSizeChoice = new JComboBox<>(fontSizes);
    
    /**
     * A form feed character for clearing the screen in BlueJ.
     */
    char deg = '\u00B0';
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        Converter myConv = new Converter();     // Creates the Swing frame and container
        myConv.setTitle("Temperature Converter");
        myConv.setSize(400, 300);
        myConv.setDefaultCloseOperation(EXIT_ON_CLOSE);
        myConv.setLocationRelativeTo(null);
        myConv.setVisible(true);
    }
    
    /**
     * This method adjusts the font size for various components.
     * @param fontSize  the font size the user selects.
     */
    public void setComponentFonts(int fontSize) {
        // Sets the font on the button
        Font f = new Font("Helvetica", Font.BOLD, fontSize);
        convertTemp.setFont(f);
    }
    
    /**
     * React to button presses, or when Enter when in the text field.
     */
    public void actionPerformed(ActionEvent e) {
        // If a button is pressed or if the return key is pressed (event from the text field)
        if (e.getSource() == convertTemp || e.getSource() == tempF) {
            try {
                // Interpret (parse) ˚F as a double and convert to ˚C
                double tempInF = Double.parseDouble(tempF.getText());
                int tempInC = (int)((tempInF - 32) / 1.8);
                
                // Set resultLabel to the new value
                resultLabel.setText(tempF.getText() + "˚F converts to " + tempInC + "˚C");
            } catch (Exception ex) {
                // Catch to handle the case where the user didn't type in an understandable number
                System.err.println("No number in text field");
            }
        }
        
        if (e.getSource() == fontSizeChoice) {
            int fontSize = Integer.parseInt((String)fontSizeChoice.getSelectedItem());
            setComponentFonts(fontSize);
        }
    }
}
