/**
 * <p><b>UID 1: Design Principles and BlueJ</b></p> 
 * <p><b>Starting Up</b></p>
 * 
 * <ol>
 *     <li>
 *     Copy the folder <code><b>Groups on Wide (V:)\CSCU9A2\BlueJUsability</b></code> to somewhere suitable in
 *     your home folder, such as <i>your</i> <code><b>CSCU9A2\Practicals</b></code> folder (or copy and paste the code
 *     below).
 *     
 *     <pre>
 *     import java.awt.*;
 *     import java.awt.event.*;
 *     import javax.swing.*;
 *     
 *     {@literal /**}
 *      * This is a simple temperature converter program
 *      *&#47;
 *      
 *     public class Converter extends JFrame 
 *     implements ActionListener 
 *     {
 *         JPanel topPanel, guiPanel;
 *         
 *         JTextField tempF;
 *         
 *         JLabel resultLabel;
 *         
 *         JLabel fontLabel;
 *         
 *         JButton convertTemp;
 *         
 *         JComboBox fontSizeChoice;
 *         
 *         char deg = '\u00B0';
 *         
 *         {@literal /**}
 *          *  main method, called when the program runs
 *          *&#47;
 *         public static void main(String[] args) 
 *         {
 *             Converter myConv = new Converter();        // Creates the Swing frame and container.
 *             myConv.setTitle("Temperature Converter");
 *             myConv.setSize(400, 300);
 *             myConv.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 *             myConv.setVisible(true);
 *         }
 *         
 *         public Converter()   // Constructor
 *         {
 *             // Now to create the GUI interface, using Swing components.
 *             
 *             guiPanel = new JPanel();
 *             guiPanel.setLayout(new BorderLayout()); // Using BorderLayout means that widgets can
 *                                                     // either be added to the centre
 *                                                     // of this panel or round the edges
 *                                                     
 *             topPanel = new JPanel();               // this "invisible panel" will hold the button and textfield
 *             topPanel.setLayout(new FlowLayout());  // and it will lay components out from left to right
 *             
 *             convertTemp = new JButton("Convert");  // this is the button we'll press to convert the
 *                                                    // temperature from Fahrenheit to Celsius
 *                                                    
 *             // convertTemp = new JButton(new ImageIcon("fc.gif"));
 *             
 *             convertTemp.addActionListener(this); // Now we can listen to events from our button.
 *             
 *             topPanel.add(convertTemp);           // adding the convertTemp button to the top panel
 *             
 *             tempF = new JTextField(10);          // creating the text field tempF
 *             tempF.addActionListener(this);       // Now we can listen to events from our text field.
 *             topPanel.add(tempF);                 // adding it to the top panel
 *             
 *     {@literal /*}
 *             // GUI components for font size adjustment
 *             fontLabel = new JLabel("Font size:",SwingConstants.RIGHT);
 *             topPanel.add(fontLabel);
 *             
 *             // firstly setting up an array of font sizes we might use
 *             String[] fontSizes = {"8","10","11","12","14","16","18", "20"};
 *             
 *             fontSizeChoice = new JComboBox(fontSizes); // this JComboBox offers a drop-down menu of choices
 *             fontSizeChoice.addActionListener(this);    // listens out for action events which get generated when 
 *                                                        // we select from the choices
 *             topPanel.add(fontSizeChoice);
 *     *&#47;
 *     
 *             guiPanel.add(topPanel,BorderLayout.NORTH);    // This adds the top panel to the top of the GUI
 *             
 *             resultLabel = new JLabel("", SwingConstants.CENTER);
 *             // This label is where the result of the
 *             // temperature conversion will appear
 *             guiPanel.add(resultLabel,BorderLayout.CENTER);// the result label will appear in the centre of the GUI
 *             getContentPane().add(guiPanel);               // Adds the panel to the application.
 *         }
 *         
 *     {@literal /*}
 *         // This method adjusts the font size for various components
 *         private void setComponentFonts(int fontSize) 
 *         {
 *             // sets the font on the button
 *             Font f = new Font("Helvetica", Font.BOLD, fontSize);
 *             convertTemp.setFont(f);
 *         }
 *     *&#47;
 *     
 *         public void actionPerformed(ActionEvent e)  // React to button press, or Enter in the textfield
 *         {
 *             if (e.getSource() == convertTemp         // if button pressed
 *                 || e.getSource() == tempF)           // or if the return key pressed (event from the textfield)
 *             { 
 *                 try 
 *                 {   // Interpret (parse) degrees Fahrenheit as a double and convert to Celsius.
 *                 
 *                     double tempInF = Double.parseDouble(tempF.getText());
 *                     
 *                     int tempInC = (int) ((tempInF-32)/1.8);
 *                     
 *                     //  Set resultLabel to new value
 *                     resultLabel.setText(tempF.getText()+" degrees F  converts to  "+tempInC+" degrees C");
 *                 }
 *                 catch (Exception ex) 
 *                 {   // catch to handle the case where the user didn't type in
 *                     // an understandable number
 *                     System.out.println("No number in textfield");
 *                 }
 *             }
 *             
 *     {@literal /*}
 *             if (e.getSource() == fontSizeChoice)    // if a choice has been made from the JComboBox
 *             {
 *                 int fontSize = Integer.parseInt((String)fontSizeChoice.getSelectedItem());
 *                 setComponentFonts(fontSize);
 *             }
 *     *&#47;
 *     
 *         }
 *     }
 *     </pre>
 *     </li>
 *     <li>Launch BlueJ from the Windows Start menu.</li>
 *     <li>
 *     In the usual way, create a new Non-BlueJ project in your <code>BlueJUsability</code> folder. You will see a
 *     window that looks like this:
 *     
 *     <br /><br />
 *     
 *     <img src="{@docRoot}/resources/week8-prac6a-img01.jpg" alt ="week8-prac6a-img01" />
 *     
 *     <br /><br />
 *     
 *     </li>
 *     <li>
 *     Double click on the "Converter" class icon to bring up the code in BlueJ's editor window. You should now see
 *     this also:
 *     
 *     <br /><br />
 *     
 *     <img src="{@docRoot}/resources/week8-prac6a-img02.jpg" alt ="week8-prac6a-img02" />
 *     
 *     <br /><br />
 *     
 *     </li>
 *     <li>
 *     Compile this application and run it in the usual way. The following window should appear:
 *     
 *     <br /><br />
 *     
 *     <img src="{@docRoot}/resources/week8-prac6a-img03.jpg" alt ="week8-prac6a-img03" />
 *     </li>
 * </ol>
 * 
 * This is a simple temperature conversion program. Try typing a number (e.g. 128) into the text box and clicking
 * "Convert" to see what happens. In the final design practical for CSCU9A2 we are going to turn this into a more
 * usable program. For now we are just going to use it to investigate issues of good design, both in Java
 * programming style and in the user interface of BlueJ.
 * 
 * <ol start="6">
 *     <li>Click the close button on the "Temperature Converter" window when you have finished playing with it.</li>
 * </ol>
 * BlueJ lets you edit, compile, run and debug your Java code. Later we will use the editor to examine and maybe change
 * the code in {@code Converter.java}. Now we will look at some of the features of the BlueJ interface.
 * 
 * <p><b>Editing</b></p>
 * 
 * <ol start="7">
 *     <li>
 *     The BlueJ editor provides a number of ways to select text (like most text editors), following the flexibility
 *     design guideline. Try out the following:
 *     <br /><br />
 *     <ol type="i">
 *         <li>
 *         Hold down the mouse button and drag across the text (dragging down the screen selects whole lines)
 *         </li>
 *         <li>
 *         Double clicking on a word selects that word; triple clicking on a line will select the whole line
 *         </li>
 *         <li>Hold the Shift key down and use the four cursor movement keys</li>
 *         <li>Hold down the Shift key and click with the mouse to extend or shorten the current selection.</li>
 *     </ol>
 *     <br />
 *     Which do you prefer? Note that providing option (iii) is particularly helping for physically disabled
 *     people who may have poor motor control, and can much more easily use a keyboard than a mouse.
 *     </li>
 *     <li>
 *     There are numerous things you can do with selected text. For example, you may wish to cut-and-paste the text
 *     somewhere else in the document. Again following the guideline of flexibility (and "Know thy user"), this editor
 *     provides three different ways you can cut-and-paste text - see if you can find all three ways.
 *     
 *     <br /><br />
 *     
 *     This is a very simple editor, but it does provide a few useful features that help with the easy formatting of
 *     your program.
 *     </li>
 *     <li>
 *     Select several consecutive lines e.g., the declarations of the global variables, starting with the
 *     {@code JPanel}. From the Edit drop-down menu, try out the Comment/Uncomment and Indent more/Indent less options.
 *     
 *     <br /><br />
 *     
 *     You can also change some of the ways the editor displays your program.
 *     </li>
 *     <li>
 *     Select Options -> Preferences... and click on the Editor tab in the window that appears. Here you will find
 *     the display preferences you can influence, such as font size, line numbering, syntax highlighting. Try
 *     changing some of these options. You need to click "OK" to see the changes take effect (and for the syntax
 *     highlighting you will need to re-launch BlueJ). Do you prefer syntax highlighting on or off?
 * </ol>
 * 
 * <p><b>Programming</b></p>
 * 
 * BlueJ provides some features to help you develop your Java application. Our converter program contains only a
 * single class, but most applications will have many classes. The main BlueJ window shows a schematic representation
 * of classes and their relationships. We will not explore this in detail here, but let's try adding a new class to
 * see how easy it is.
 * 
 * <ol start="11">
 *     <li>
 *     Click "New Class..." on the left-hand toolbar of the main window. Type in a class name (NOT Converter!) and
 *     click "OK". A new class box with your given name appears in the main window.
 *     </li>
 *     <li>
 *     Open your new class in the editor. Examine the class template that BlueJ provides for your new class. You
 *     will find dummy comments and method definitions. Do you think this is useful, or will it be irritating having
 *     to change all the comments and definitions?
 *     </li>
 *     <li>
 *     When you have finished looking at this template, close the editor window and remove your new class (can you
 *     work out how to do this?)
 *     </li>
 * </ol>
 * 
 * <p><b>Good Java Programming Style</b></p>
 * <p><b>Identifiers</b></p>
 * 
 * <ol start="14">
 *     <li>
 *     Remember what you have learnt about identifiers?
 *     
 *     <br /><br />
 *     
 *     <b>Naming Variables</b>
 *     
 *     <br /><br />
 *     
 *     <span style="color: blue;">
 *     Start subsequent word segments with an upper case letter <br />
 *     Examples:
 *     </span>
 *     <span style="color: brown;">numberOfCars totalWeight</span> <br />
 *     <span style="color: blue;">
 *     Meaningful to the reader! <br />
 *     Examples:
 *     </span>
 *     <span style="color: brown;">numberOfCars</span> <span style="color: blue;">but not</span>
 *     <span style="color: brown;">nOC</span>
 *     </li>
 * </ol>
 * 
 * As for regards to the rules for choosing variable, method and class names, these help to follow Principle 2:
 * Don't Overload the User's Memory. If you have variable names that make sense, they are easier to remember!
 * 
 * <ol start="15">
 *     <li>
 *     Check the {@link Converter#Converter Converter} program. Have sensible variable names been chosen? If not, try to
 *     work out the meaning of each badly named variable and give it a better name. Then recompile and rerun the program to
 *     make sure it still works!
 *     
 *     <br /><br />
 *     
 *     Can you remember the purpose(s) of the variables in your program from last week's practical? If not, maybe
 *     you didn't choose very sensible names for the variables?
 *     </li>
 * </ol>
 * 
 * Comments in program code are also effective in reducing memory load. Do you comment your programs well? It is a lot
 * easier to comment program code <i>at the time you write it</i> than to try and fill in comments later. Again, this
 * practice reduces the load on your memory!
 * 
 * <ol start="16">
 *     <li>
 *     Check the Converter program. Does it have sensible comments in it? (If you can work out what the program
 *     does from the comments, then they are pretty good!)
 *     </li>
 * </ol>
 * 
 * BlueJ provides two other ways to document your program which will certainly act as an aid to both yourself and to
 * anyone else who may be altering the program or wants to make use of your Java classes in their own program.
 * 
 * <ol start="17">
 *     <li>
 *     The first of these is the file README.TXT (or README.MD). A blank template is provided with any new BlueJ
 *     project. Open this file for the Converter project by double clicking on the page symbol (text file) to the
 *     left of the Converter class box in the main BlueJ window.
 *     </li>
 *     <li>
 *     A new editor window will appear containing README.TXT. Spend a few minutes editing this file to fill in some
 *     details for this project...
 *     </li>
 * </ol>
 * 
 * Secondly, BlueJ provides an easy way to generate Javadoc Documentation. This is the form of documentation provided
 * by the Java developers for Java itself (the Java API). We can generate such documentation for our application by
 * adding specially formatted comments to {@code Converter.java}.
 * 
 * <ol start="19">
 *     <li>
 *     To generate basic Javadoc documentation for {@code Converter.java}, open this file in the BlueJ editor (if it
 *     is not already open). Then in the drop-down menu in the top right of the editor window (which currently says
 *     "Source Code"), select "Documentation". A long document detailing the Converter class will appear.
 *     </li>
 *     <li>
 *     Have a look through this document. It already contains a lot of information about Converter's parent classes,
 *     such as {@code JFrame}. However, if you look closely you will see that it also lists details about the global
 *     variables (called Fields) and the methods in the {@code Converter} class. Make sure you find these in the
 *     documentation.
 *     </li>
 *     <li>
 *     Now we will add some comments to {@code Converter.java} that will provide extra information in the Javadoc
 *     about the fields and methods in Converter. Reselect "Source Code" in the editor window. Just before the
 *     declaration of the {@code JButton convertTemp;}, place these lines:
 *     
 *     <pre>
 *     {@literal /**}
 *      * Button that initiates conversion.
 *      *&#47;
 *     </pre>
 *     </li>
 *     <li>
 *     And just before the definition of the class constructor {@code public Converter()} put:
 *     
 *     <pre>
 *     {@literal /**}
 *      * Program to convert temperature from
 *      * Fahrenheit to Celsius.
 *      *&#47;
 *     </pre>
 *     </li>
 *     <li>Now select "Documentation" again and find where these new comments have appeared in the Javadoc.</li>
 *     <li>
 *     Add similar comments for one more field (global variable) and one more method. Re-examine the Javadoc again to
 *     see where your comments have appeared.
 *     </li>
 * </ol>
 * 
 * @author  Michael Sammels
 * @version 04.03.2019
 * @since   1.0
 */
package week8.practical6a;