/**
 * <p>Practical 8B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 8B</b><br />
 * Following from Practical <i>UID 2: Web Design Principles</i>, in which you used the WebAIM WAVE accessibility
 * checker, this practical lets you try out some other approaches to accessibility testing. By the end of this
 * practical, students should be able to:
 * 
 * <ul>
 *     <li>Use various tools to test the quality of their web page</li>
 *     <li>Use Krug's testing method to evaluate the navigation of a web page</li>
 * </ul>
 * 
 * This practical is not about writing web pages: it is about testing your web pages to ensure quality. You'll cover
 * useful resources for help with web page testing:
 * 
 * <ul>
 *     <li>Taking screenshots</li>
 *     <li>Accessibility Testing</li>
 *     <li>Colour Blindness Testing</li>
 *     <li>Viewing your website in other browsers (including text only browsers)</li>
 *     <li>Using validating services to check your HTML and CSS are correct</li>
 * </ul>
 * 
 * @author  Michael Sammels
 * @version 21.03.2019
 * @since   1.0
 */
package week10;