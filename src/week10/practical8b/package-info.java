/**
 * <p><b>UID 3: Web Design: Testing accessibility</b></p>
 * <p><b>Taking Screenshots</b></p>
 * 
 * Screenshots can be useful for various reasons:
 * <ul>
 *     <li>Testing the layout of your web page by showing someone a printout.</li>
 *     <li>Testing out colour choices by printing out in black and white.</li>
 *     <li>Including a screenshot (or part of one) in a report.</li>
 * </ul>
 * 
 * Simply press <code><b>Alt + Print Screen</b></code> to capture the active window to the clipboard. You could then
 * <b>paste</b> into a Word document or similar.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Accessibility Testing - W3C</b></p>
 * 
 * The World Wide Web consortium (W3C) have published a detailed set of prioritised guidelines for how to ensure that
 * web pages are accessible to as many people as possible. The USA also has the Americans with Disabilities Act,
 * specifically Section 508, to deal with accessibility.
 * 
 * <br /><br />
 * 
 * A useful introduction to accessibility is: <br />
 * <a href="http://www.w3.org/standards/webdesign/accessibility">
 *     http://www.w3.org/standards/webdesign/accessibility
 * </a>
 * 
 * <br /><br />
 * 
 * W3C have guidelines on how to do a preliminary review of accessibility for a website: <br />
 * <a href="http://www.w3.org/WAI/ER/tools/Overview.html">http://www.w3.org/WAI/ER/tools/Overview.html</a>
 * 
 * <br /><br />
 * 
 * W3C have guidelines on how to do a preliminary review of accessibility for a website: <br />
 * <a href="http://www.w3.org/WAI/eval/preliminary.html">http://www.w3.org/WAI/eval/preliminary.html</a>
 * 
 * <br /><br />
 * 
 * In this practical, we'll follow the steps below to evaluate the accessibility of a few websites.
 * 
 * <ol>
 *     <li>Select a representative sample of pages.</li>
 *     <li>Examine the selected pages using graphical browsers.</li>
 *     <li>Examine the selected pages using specialised browsers.</li>
 *     <li>Use automated Web accessibility evaluation tools.</li>
 * </ol>
 * 
 * <b>Select a representative page sample</b>
 * 
 * <br /><br />
 * 
 * For the exercises below we will use the home page of the University of Stirling
 * <a href="www.stir.ac.uk">www.stir.ac.uk</a>, the Division's home page
 * <a href="www.cs.stir.ac.uk">www.cs.stir.ac.uk</a>, Amazon <a href="www.amazon.co.uk">www.amazon.co.uk</a>, and a
 * web page of your own, though you may choose other websites if you wish.
 * 
 * <br /><br />
 * 
 * <b>Examine pages using graphical browsers</b>
 * 
 * <br /><br />
 * 
 * Using your usual browser, try adjusting the settings to see what happens to your web pages.
 * <ol>
 *     <li>
 *     Turn off the sound (if at home; it is already turned off in the labs), and check whether any audio content
 *     is still available through text equivalents.
 *     </li>
 *     <li>
 *     Use browser controls to vary font-size (often this is {@code Ctrl+}, {@code Ctrl + -}, {@code Ctrl+0} for
 *     enlarge, shrink to normal): verify the font size changes on the screen accordingly; and that the page is still
 *     usable at larger font sizes (the layout is not broken).
 *     </li>
 *     <li>Try resizing the application window to verify that horizontal scrolling is not required.</li>
 *     <li>
 *     Without using the mouse, use the keyboard to navigate through the links and form controls on a page (for
 *     example, using the "<i>Tab</i>" key), making sure that you can access all links and form controls, and that
 *     the links clearly indicate what they lead to.
 *     </li>
 * </ol>
 * 
 * How easy was it? Did you find it easy to use the web page while changing these settings?
 * 
 * <p><b>Examine pages using a variety of browsers</b></p>
 * 
 * You should always test your web pages in different (graphical) browsers. The University labs provide Internet
 * Explorer, Firefox and Chrome. Visit the web page(s) that you have selected using various browsers. You may have
 * Opera, Safari, etc., on your own smartphones or computers.
 * 
 * <p><b>Use automated Web accessibility evaluation tools</b></p>
 * 
 * A very useful thing is to test in a <b><i>text-only browser</i></b> such as Lynx (see
 * <a href="https://en.wikipedia.org/wiki/Lynx_(web_browser)">https://en.wikipedia.org/wiki/Lynx_(web_browser)</a>.
 * <i>If a web page is easily navigable when you can only see the text, then it is more likely to be easily navigable
 * in the normal view, and it is more like to be easily navigable for someone with impaired vision using a screen
 * reader</i>.
 * 
 *  <br /><br />
 *  
 *  Lynx is not installed on the University's PC labs, but there is an executable version of it in the module folder
 *  <code><b>Groups on Wide\CSCU9A2\</b></code>: navigate to that folder and double click the shortcut
 *  <code>Run_Lynx</code>. A command window will open to execute Lynx, and you interact with it via the keyboard.
 *  
 *  <br /><br />
 *  
 *  Here is a brief summary of the basic use of Lynx:
 *  <ul>
 *      <li>Type <code><b>?</b></code> to see help information. Press the <code><b>&#8592;</b></code> to go back.</li>
 *      <li>
 *      Type <code><b>g</b></code> to go to a web page: Lynx prompts for a URL at the foot of the window. Type the
 *      URL, then press <b>Enter</b>.
 *      </li>
 *      <li>
 *      When the page is displayed (in text only!), use the <code><b>&#8593; &#8595;</b></code> keys to navigate
 *      between the links in the displayed page. When a link is selected, press <b>Enter</b> or
 *      <code><b>&#8594;</b></code> to go to the page linked to.
 *      </li>
 *      <li>Press the <code><b>&#8592;</b></code> key to back to the previous page.</li>
 *      <li>Type <code><b>q</b></code> to quit.</li>
 *  </ul>
 *  
 *  By default the window is quite small. Use the Properties, Layout setting (click the black cmd icon at the top
 *  left) to increase the <i>Window size</i> height to, say, 50, then <i>close</i> and re-open Lynx.
 *  
 *  <br /><br />
 *  
 *  Try using Lynx to view the Computing Science and Mathematics website at
 *  <a href="http://www.cs.stir.ac.uk/">http://www.cs.stir.ac.uk/>http://www.cs.stir.ac.uk/</a>, or some other web
 *  page of interest (note that Lynx will <i>not</i> accept <code>https:</code> URLs). Try using it to view your own
 *  personal website, if you have created one, or your Red Hat web page from the earlier practical.
 *  
 *  <br /><br />
 *  
 *  In an earlier practical you saw another very useful tool: the <b>WAVE</b> tool at
 *  <a href="https://wave.webaim.org">https://wave.webaim.org</a>, which gives a graphical report combining features
 *  of W3C guidelines and Section 508 Guidelines.
 *  
 *  <br /><br />
 *  
 *  Try checking various pages with WAVE: perhap's the Divsion's homepage at
 *  <a href="http://cs.stir.ac.uk">http://cs.stir.ac.uk</a>, Amazon
 *  (<a href="https://www.amazon.co.uk">www.amazon.co.uk</a>) and a web page of your own.
 *  
 *  <br /><br />
 *  
 *  <i>Keep your Lynx window open for now</i>.
 *  
 *  <p><b>Colour Blindness Testing</b></p>
 *  
 *  Colour usage can be particularly problematic for people with some form of colour blindness. There are several
 *  tools to help you choose effective colour contrasts, for example, see:
 *  
 *  <br /><br />
 *  
 *  <a href="http://gmazzocato.altervista.org/colorwheel/wheel.php">
 *  http://gmazzocato.altervista.org/colorwheel/wheel.php
 *  </a>
 *  
 *  <br /><br />
 *  
 *  This will allow you to choose foreground and background colours and see how these colours appear to people with
 *  different kinds of colour blindness.
 *  
 *  <br /><br />
 *  
 *  You can also try the Coblis Color Blindness Simulator:
 *  
 *  <a href="http://www.color-blindness.com/coblis-color-blindness-simulator/">
 *  http://www.color-blindness.com/coblis-color-blindness-simulator/
 *  </a>
 *  
 *  <br /><br />
 *  
 *  This allows you to view images that you provide with a colour blindness simulation applied. You can check out web
 *  pages by taking a screenshot and viewing the image with Coblis.
 *  
 *  <p><b>Validation</b></p>
 *  
 *  Of course, you should also <b>validate</b> your web pages to make sure the HTML is correct, using the W3C
 *  validator: <a href="https://validator.w3.org">https://validator.w3.org</a><br />
 *  
 *  Go to the validator and try out the following websites:
 *  
 *  <ul style="list-style-type: none;">
 *      <li>www.cs.stir.ac.uk</li>
 *      <li>www.amazon.co.uk</li>
 *      <li>Your Red Hat page</li>
 *  </ul>
 *  
 *  <p><b>The Trunk Test</b></p>
 *  
 *  Steve Krug's Trunk Test is a test of site navigation. Given a previously unseen web page, locate the following
 *  items as quickly as possible:
 *  
 *  <ul>
 *      <li>Site ID (what site is this?)</li>
 *      <li>Page Name (where am I?)</li>
 *      <li>Sections (are the sites major sections outlined?)</li>
 *      <li>Local Navigation</li>
 *      <li>Where am I (is there a "You are here"?)</li>
 *      <li>How can I search (for large sites only)</li>
 *  </ul>
 *  
 *  Once you are satisfied that all of the items included in this test are clearly and quickly identifiable at a
 *  glance, you can be fairly sure that the site has a workable navigation system.
 *  
 *  <br /><br />
 *  
 *  <i>Go to <a href="https://weblium.com/blog/21-bad-website-examples-of-2018/">
 *  https://weblium.com/blog/21-bad-website-examples-of-2018/</a><br />
 *  Try the trunk test on a couple of examples</i>.
 *  
 *  <br /><br />
 *  
 *  Steve Krug has a nice video about usability testing:
 *  
 *  <br /><br />
 *  
 *  <a href="http://www.sensible.com/rsme.html">http://www.sensible.com/rsme.html</a>
 *  
 *  <br /><br />
 *  
 *  You will need headphones for this (there is sound).
 *  
 * @author  Michael Sammels
 * @version 21.03.2019
 * @since   1.0
 */
package week10.practical8b;