/**
 * <p>Practical 9B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 9B</b><br />
 * 
 * So far in the User Accessibility part of the course, you've had the chance to look at usability and accessibility
 * for web pages, but not for programs. Today, you are presented with a small working Java program, and you'll get
 * the chance to make a few small changes to improve its user interface. The program uses interface components
 * from the {@link javax.swing Swing} package. We're using Swing because it offers a lot for interfaces and user
 * accessibility.
 * 
 * Overall, the idea is that this practical will:
 * 
 * <ul>
 *     <li>give you the chance to have a little more experience of the {@link javax.swing Swing} package,</li>
 *     <li>show you more of what Java can do,</li>
 *     <li>give you the chance to have a go at producing a more sophisticated interface,</li>
 *     <li>show you a small example of how programmers can make usable and accessible programs</li>
 * </ul>
 * 
 * Most of the code is written already, and you are just asked to make minor changes. These are the improvements,
 * accessibility issues and customisation we shall look at:
 * 
 * <ul>
 *     <li>
 *     <b>Changing font sizes</b>
 *     <ul style="list-style-type: square;">
 *         <li>useful when a user's eyesight isn't compatible with the default font size</li>
 *     </ul>
 *     </li>
 *     <li>
 *     <b>Meaningful Buttons</b>
 *     <ul style="list-style-type: square;">
 *         <li>useful meaningful icons can produce a clearer interface, thus reducing memory load</li>
 *     </ul>
 *     </li>
 *     <li>
 *     <b>Keyboard Shortcuts</b>
 *     <ul style="list-style-type: square;">
 *         <li>useful when a user prefers not to (or cannot) use the mouse</li>
 *     </ul>
 *     </li>
 *     <li>
 *     <b>Unicode and Internationalisation</b>
 *     <ul style="list-style-type: square;">
 *         <li>
 *         what cultural considerations should an interface designer take into account, and what does Java offer
 *         in the way of help?
 *         </li>
 *     </ul>
 * </ul>
 *     
 * @author  Michael Sammels
 * @version 28.03.2019
 * @since   1.0
 */
package week11;