/**
 * <p><b>UID 4: Java and Accessibility</b></p>
 * <p><b>Getting Started</b></p>
 * 
 * <ol>
 *     <li>
 *     In the same way that you have done in previous practicals, copy the <b><i>folder</i> Converter</b> from
 *     <code><b>Groups on Wide (V:)\CSCU9A2\Java</b></code> to your CSCU9A2 working folder.
 *     </li>
 *     <li>
 *     Launch BlueJ. Using the <b>Project</b> menu, <b>Open Non BlueJ</b>... create a BlueJ project in your
 *     <code><b>Converter</b></code> folder - remember, when you navigate to it click once on the folder name then
 *     Open in BlueJ - do not double click to open the folder. Check that program compiles and runs (you can also copy
 *     and paste the code from below).
 *     
 *     <pre>
 *     {@literal /*}
 *      * This is a very small application which converts a
 *      * temperature in degrees Fahrenheit to degrees
 *      * Celsius.
 *      *
 *      * If it converted back again too, that would be even more useful. 
 *      * This is a small application, but it does illustrate
 *      * a few Swing widgets, and shows what you can do with them.
 *      * 
 *      * Simon Jones, April 2014
 *      *
 *      *&#47;
 *      
 *     import java.awt.*;
 *     import java.awt.event.*;
 *     import javax.swing.*;
 *     
 *     public class Converter extends JFrame 
 *     implements ActionListener 
 *     {
 *         JPanel topPanel, guiPanel;           // some "invisible panels", or "layout areas"
 *                                              // for laying out the interface
 *                                              
 *         JTextField tempF;                    // a textfield to type in the temperature
 *         
 *         JLabel resultLabel;                  // a label to display the converted temperature
 *         
 *         JLabel fontLabel;                    // for later, when font size choice is implemented
 *         
 *         JButton convertTemp;                 // the idea is, when you press the button, it
 *                                              // converts the temperature for you
 *         
 *         JComboBox fontSizeChoice;            // to choose the size of font
 *         
 *         char deg = '\u00B0';                 // a useful Unicode character for later on...
 *         
 *         // main method, called when the program runs
 *         public static void main(String[] args) 
 *         {
 *             Converter myConv = new Converter();        // Creates the Swing frame and container.
 *             myConv.setTitle("Temperature Converter");
 *             myConv.setSize(400, 300);
 *             myConv.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 *             myConv.setVisible(true);
 *         }
 *         
 *         // class constructor 
 *         // (is called at new Converter() instantiation)
 *         public Converter() 
 *         {
 *             // Now to create the GUI interface, using Swing components.
 *             
 *             guiPanel = new JPanel();
 *             guiPanel.setLayout(new BorderLayout()); // Using BorderLayout means that widgets can
 *                                                     // either be added to the centre
 *                                                     // of this panel or round the edges
 *             
 *             topPanel = new JPanel();               // this "invisible panel" will hold the button and textfield
 *             topPanel.setLayout(new FlowLayout());  // and it will lay components out from left to right
 *             
 *             convertTemp = new JButton("Convert");  // this is the button we'll press to convert the
 *                                                    // temperature from Fahrenheit to Celsius
 *             
 *             // convertTemp = new JButton(new ImageIcon("fc.gif"));
 *             
 *             convertTemp.addActionListener(this); // Now we can listen to events from our button.
 *             
 *             topPanel.add(convertTemp);           // adding the convertTemp button to the top panel
 *             
 *             tempF = new JTextField(10);          // creating the text field tempF
 *             tempF.addActionListener(this);       // Now we can listen to events from our text field.
 *             topPanel.add(tempF);                 // adding it to the top panel
 *     
 *     {@literal /*}
 *             // GUI components for font size adjustment
 *             fontLabel = new JLabel("Font size:",SwingConstants.RIGHT);
 *             topPanel.add(fontLabel);
 *             
 *             // firstly setting up an array of font sizes we might use
 *             String[] fontSizes = {"8","10","11","12","14","16","18", "20"};
 *             
 *             fontSizeChoice = new JComboBox(fontSizes); // this JComboBox offers a drop-down menu of choices
 *             fontSizeChoice.addActionListener(this);    // listens out for action events which get generated when 
 *                                                        // we select from the choices
 *             
 *             topPanel.add(fontSizeChoice);
 *     *&#47;
 *     
 *             guiPanel.add(topPanel,BorderLayout.NORTH);    // This adds the top panel to the top of the GUI
 *              
 *             resultLabel = new JLabel("", SwingConstants.CENTER);
 *             // This label is where the result of the
 *             // temperature conversion will appear
 *             guiPanel.add(resultLabel,BorderLayout.CENTER);// the result label will appear in the centre of the GUI
 *             getContentPane().add(guiPanel);               // Adds the panel to the application.
 *         }
 *         
 *     {@literal /*}
 *         // This method adjusts the font size for various components
 *         private void setComponentFonts(int fontSize) 
 *         {
 *             // sets the font on the button
 *             Font f = new Font("Helvetica", Font.BOLD, fontSize);
 *             convertTemp.setFont(f);
 *         }
 *     *&#47;
 *     
 *         // React to button press, or Enter in the textfield
 *         public void actionPerformed(ActionEvent e) 
 *         {
 *             if (e.getSource() == convertTemp         // if button pressed
 *                 || e.getSource() == tempF)           // or if the return key pressed (event from the textfield)
 *             { 
 *                 try 
 *                 {   // Interpret (parse) degrees Fahrenheit as a double and convert to Celsius.
 *                 
 *                     double tempInF = Double.parseDouble(tempF.getText());
 *                     
 *                     int tempInC = (int) ((tempInF-32)/1.8);
 *                     
 *                     //  Set resultLabel to new value
 *                     resultLabel.setText(tempF.getText()+" degrees F  converts to  "+tempInC+" degrees C");
 *                 }
 *                 catch (Exception ex) 
 *                 {   // catch to handle the case where the user didn't type in
 *                     // an understandable number
 *                     System.out.println("No number in textfield");
 *                 }
 *             }
 *             
 *     {@literal /*}
 *             if (e.getSource() == fontSizeChoice)    // if a choice has been made from the JComboBox
 *             {
 *                 int fontSize = Integer.parseInt((String)fontSizeChoice.getSelectedItem());
 *                 setComponentFonts(fontSize);
 *             }
 *     *&#47;
 *         }
 *     }
 *     </pre>
 *     </li>
 *     <li>
 *     The program is an application that will convert Fahrenheit temperature into Celsius. It is only a very small
 *     program with a little functionality, but nevertheless is sufficient to demonstrate various aspects of interface
 *     design. It's also very useful for cooks who are reading old or American recipes and can't remember how hot
 *     475˚F is in Celsius! (Fun fact, it's 246.11˚C)
 *     
 *     <br /><br />
 *     
 *     When the program is running, check what it actually does. You should be able to type a number in the text
 *     field (representing the temperature in ˚F), press the button, and see the result of the conversion.
 *     </li>
 *     </ol>
 *     
 *     <b>Program Tour</b>
 *     
 *     <ol start="4">
 *     <li>
 *     Now have a good look around the program code. Hopefully you should be able to follow pretty much what it is
 *     doing and which bits correspond to what, as the program is extensively commented. A few points of interest:
 *     
 *     <ul>
 *         <li>Some code is commented out (we'll use this later).</li>
 *         <li>
 *         Note that when the button <b>{@code convertTemp}</b> is pressed, and thus the <b>{@code actionPerformed}</b>
 *         gets called, there is a <b>{@code try ... catch}</b> clock there. What exception is it handling? Well, the
 *         user might write something silly in the text field instead of a number, so the
 *         <b>{@code Integer.parseInt}</b> method might throw a <b>{@code NumberFormatException}</b>. Try doing that
 *         just now (running the program, writing something silly in the text field, and pressing the <b>Convert</b>
 *         button) and see what happens in the terminal window. Check that you see how that corresponds to the program
 *         code. [Note: the exception type <b>{@link java.lang.Exception Exception}</b> is an "umbrella" that <i>includes</i>
 *         <b>{@link java.lang.NumberFormatException NumberFormatException}</b>.]
 *         </li>
 *         <li>
 *         You should be able to see that the Fahrenheit to Celsius conversion also happens in the
 *         <b>{@link Converter#actionPerformed actionPerformed}</b> method, and the
 *         <b>{@code resultLabel resultLabel}</b> is relabelled with an appropriate <b>{@link java.lang.String String}</b>.
 *         </li>
 *     </ul>
 *     
 *     </li>
 *     <li>
 *     Have a look at how the GUI is set up. There are 3 main components in there, a button, a text field and a label.
 *     They are placed on invisible <i>panels</i>, which are used for layout purposes. The way they are set up is like this 
 *     (drawing dashed lines for the perimeters for clarity):
 *     
 *     <br /><br />
 *     
 *     <img src="{@docRoot}/resources/week11-prac9b-img01.jpg" alt ="week11-prac9b-img01" height="50%" width="50%" />
 *     
 *     <br /><br />
 *     
 *     If you look through the code in <b>{@link Converter#Converter Converter}</b> constructor, you should see that we create the two panels
 *     first. Then the <b>{@code convertTemp}</b> button is created and then added to the top panel. Then the text field
 *     <b>{@code tempF}</b> is created and also added to the top panel. Then the line
 *     
 *     <br /><br />
 *     
 *     <b>{@code guiPanel.add(topPanel, BorderLayout.NORTH);}</b>
 *     
 *     <br /><br />
 *     
 *     ensures that the top panel gets added to the <i>top</i> (north) of the GUI panel. Then finally the results label
 *     gets added to the <i>centre</i> of the GUI panel.
 *     
 *     <br /><br />
 *     
 *     I hope you now have a rough idea from the code and the diagram above, how the interface has been laid out. The
 *     interface is a bit basic at the moment. Let's now start improving it!
 *     </li>
 * </ol>
 * 
 * <p><b>Changing Font Sizes</b></p>
 * 
 * Not everyone has perfect eyesight. Not everyone is using a monitor with a nice big display on it. For whatever reason,
 * changing the font sizes used throughout a program is often a useful thing to do. This is very easy to do with Swing, 
 * as every component has a <b>{@code setFont}</b> method that can easily be used to set the size of the font that the
 * component uses.
 * 
 * <ol start="6">
 *     <li>
 *     Now uncomment the three sections of code (by removing the {@literal /*} and *&#47;) that read like this:
 *     
 *     <pre>
 *     {@literal /*}
 *         // GUI components for font size adjustment
 *         fontLabel = new JLabel("Font size:", SwingConstants.RIGHT);
 *         ...
 *         topPanel.add(fontSizeChoice);
 *     *&#47;
 *     
 *     {@literal /*}
 *         // This method adjusts...
 *         private void setComponentFonts(int fontSize)
 *         ...
 *             convertTemp.setFont(f);
 *         }
 *     *&#47;
 *     
 *     {@literal /*}
 *         if (e.getSource() == fontSizeChoice)    // if a choice...
 *         {
 *             ...
 *         }
 *     *&#47;
 *     </pre>
 *     
 *     Have a look at what is happening in this code. First set up an array of <b>{@link java.lang.String Strings}</b>.
 *     These are going to be the font choices available for the program. Then we use that array to set up something called a
 *     <b>{@link javax.swing.JComboBox JComboBox}</b>, which is a widget you should have seen before in many interfaces,
 *     where there's a small arrow with a drop-down menu you can choose from. Once the
 *     <b>{@link javax.swing.JComboBox JComboBox}</b> is set up, the program is then registered as an
 *     <b>{@link java.awt.event.ActionListener ActionListener}</b> (so that <b>{@code actionPerformed}</b> is called
 *     whenever an item is selected), and the <b>{@code JComboBox}</b> is added to the top panel.
 *     <b>{@code actionPerformed}</b> detects the item selection event, and calls the helper method
 *     <b>{@link Converter#setComponentFonts setComponentFonts}</b>.
 *     
 *     Compile and run. You should now have a combox box in the top panel!! Try changing the selected font size. What
 *     happens?
 *     
 *     You should find that only the <b>{@code Convert}</b> button changes its font. Looking in the
 *     <b>{@link Converter#setComponentFonts setComponentFonts}</b> method, that's exactly what's happening.
 *     We need to set the fonts for the other four components, <b>{@code resultLabel}</b>, <b>{@code fontLabel}</b>,
 *     <b>{@code tempF}</b> (the text field) and even <b>{@code fontSizeChoice}</b> itself.
 *     </li>
 *     <li>
 *     Add four more lines in the <b>{@link Converter#setComponentFonts setComponentFonts}</b> method to do
 *     just that. The other components have their fonts set in an identical way to the <b>{@code convertTemp}</b> button.
 *     Compile and run to check that the font changing works!
 *     </li>
 *     <li>
 *     Set the initial size of the font to <b>12</b> by adding the line
 *     
 *     <br /><br />
 *     
 *     <b>{@code fontSizeChoice.setSelectedIndex(3);}</b> to the GUi set up code, <i>after the line creating</i>
 *     <b>{@code fontSizeChoice}</b> (but before the line that adds the action listener.) This is more consistent with
 *     the user's mental model by ensuring that the component always shows the correct font size. Why did we set it to
 *     <b>3</b>?
 *     
 *     Try altering the array yourself to change the range of font sizes available.
 *     </li>
 * </ol>
 * 
 * <p><b>Pictures on Buttons</b></p>
 * 
 * Sometimes, text on buttons isn't always the clearest. "Convert" is only half clear: what does it convert from? What
 * does it convert to? What we can do, is to set up the button with a picture on it instead.
 * 
 * <ol start="9">
 *     <li>
 *     <i>
 *     Comment out</i> the current line which creates the button <b>{@code convertTemp}</b>, and instead uncomment the line
 *     that reads
 *     
 *     <br /><br />
 *     
 *     <b>{@code convertTemp = new JButton(new ImageIcon("fc.gif"));}</b>
 *     
 *     <br /><br />
 *     
 *     Now try compiling and running the program. What happens?
 *     
 *     You should have instead a picture on your button. Is that clearer or not, in your opinion?
 *     
 *     Well maybe for sighted people, but for blind people, it would still help to have a text description.
 *     
 *     What we can do for Swing buttons is to give them <i>tooltips</i>. These behave very much like the ALT attribute in
 *     web pages, or tooltips in other Windows applications.
 *     </li>
 *     <li>
 *     Add in the following line, after the line creating the button in the first place:
 *     
 *     <br /><br />
 *     
 *     <b>{@code convertTemp.setToolTipText("Convert from Fahrenheit to Celsius");}</b>
 *     
 *     <br /><br />
 *     
 *     Now try compiling and running the program and testing the tooltip by hovering your mouse over the button. Does it
 *     work?
 *     </li>
 * </ol>
 * 
 * <p><b>Keyboard Shortcuts</b></p>
 * 
 * We can go further with accessibility. It is possible to provide keyboard shortcuts for many components, so that only
 * keyboard presses are required to operate the component. We will look at one small example for providing a shortcut with
 * our <b>{@code convertTemp}</b> button.
 * 
 * <ol start="11">
 *     <li>
 *     Next to the line setting the tooltip text above, add the following line of code:
 *     
 *     <br /><br />
 *     
 *     <b>{@code convertTemp.setMnemonic(KeyEvent.VK_C);}</b>
 *     
 *     <br /><br />
 *     
 *     This makes Alt-C a keyboard shortcut. Try compiling and running the program.
 *     
 *     What happens if you type in a number to convert, and then press the Alt key with the C key on the keyboard?
 *     You should find that you've managed to press the button without the help of the mouse!
 *     </li>
 * </ol>
 * 
 * <p><b>Unicode and Internationalization</b></p>
 * 
 * As programmers, when we are thinking "Know Thy User", one thin we should always think about is
 * "<i>What nationality/culture are the users of my program?</i>"
 * 
 * In the Java tutorial (available from Oracle's web pages), there are offered some helpful guidelines about what you
 * should check in an interface, because these may be culturally dependent. This is quoted from their guidelines.
 * (Note these guidelines can also apply to web pages!)
 * 
 * <br /><br />
 * 
 * <img src="{@docRoot}/resources/week11-prac9b-img02.jpg" alt ="week11-prac9b-img02" height="50%" width="50%" />
 * 
 * <p><b>Unicode character families</b></p>
 * 
 * As far as languages and text messages go, Java does provide help with languages, in the from of representing characters
 * in Unicode. Unicode is an extremely extensive character set: see below for the families of character it represents!
 * See www.unicode.org for further details of the exact characters represented in each family.
 * 
 * <br /><br />
 * 
 * <img src="{@docRoot}/resources/week11-prac9b-img03.jpg" alt ="week11-prac9b-img03" height="50%" width="50%" />
 * 
 * <br /><br />
 * 
 * With all these characters available, Java can easily allow a programmer to write in nearly any language they want!
 * 
 * We;re not going to into details about customising for different languages or characters, but we will look at a little
 * example of Unicode just to show you how it can be used.
 * 
 * The degree symbol for temperature, like ° in °C, is Unicode character 00B0. If you look at the top of the program
 * you'll see that this character has been defined for you in the <b>{@code char}</b> variable <b>{@code deg}</b>.
 * 
 * For this, you need to know that you can use a <b>{@code char}</b> variable to join with strings in the usual way, so
 * that (for example) if variable <b>{@code letter}</b> contained character {@code 'e'}, then <b>{@code "H"+letter+"llo"}</b>
 * would be the string <b>{@code "Hello"}</b>.
 * 
 * <ol start="12">
 *     <li>
 *     In the <b>{@code actionPerformed}</b> method in <b>{@code ConversionListener}</b> there is the line
 *     
 *     <br /><br />
 *     
 *     <b>{@code resultLabel.setText(tempF.getText()+" degrees F converts to 
 *                                                         "+tempInC+" degrees C");}</b>
 *     
 *     Change this line, using <b>{@code deg}</b>, so that when you run the program, instead of saying
 *     
 *     <p>{@code 350 degrees F converts to 180 degrees C}</p>
 *     
 *     It would say
 *     
 *     <p>{@code 350°F converts to 180°C}</p>
 * </ol>
 *     
 * Compile and run your program to make sure it works.
 */
package week11.practical9b;