package week11.practical9b;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.Serializable;

/**
 * CSCU9A2 - Week 11 <br />
 * Practical 9B <br />
 * <code>Converter.java</code>
 * 
 * <p>
 * This is a very small application which converts temperature in ˚F to ˚C.
 * 
 * <br /><br />
 * 
 * If it converted back again too, that would be more useful. This is a small application, but it does illustrate
 * a few Swing widgets, and shows what you can do with them.
 * </p>
 * 
 * @author  Simon Jones
 * @author  Michael Sammels
 * @version 28.03.2019
 * @since   1.0
 */

public class Converter extends JFrame implements ActionListener, Serializable {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = 5056503679208970634L;

    /**
     * Constructor.
     */
    public Converter() {
        // Now to create the GUI interface, using Swing components
        guiPanel = new JPanel();
        
        // Using Borderlayout means that widgets can either be added to the centre of this panel or round the edges
        guiPanel.setLayout(new BorderLayout());
        
        /*
         * This "invisible panel" will hold the button and the text field, and it will lay components out from
         * left to right
         */
        topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());
        
        convertTemp = new JButton(new ImageIcon("fc.gif"));
        System.out.println(System.getProperty("user.dir"));
        convertTemp.setToolTipText("Convert from Fahrenheit to Celsius");
        convertTemp.setMnemonic(KeyEvent.VK_C);
        
        convertTemp.addActionListener(this);                // Now we can listen to events from our button
        topPanel.add(convertTemp);                          // Adding the convertTemp button to the top panel
        
        tempF = new JTextField(10);                         // Creating the text field tempF
        tempF.addActionListener(this);                      // Now we can listen to events from our text field 
        topPanel.add(tempF);                                // Adding it to the top panel
        
        // GUI components for font size adjustment
        fontLabel = new JLabel("Font size: ", SwingConstants.RIGHT);
        topPanel.add(fontLabel);

        fontSizeChoice.setSelectedIndex(3);                 // The initially selected font size
        
        // Listens out for action events which get generated when we select from our choices
        fontSizeChoice.addActionListener(this); 
        
        topPanel.add(fontSizeChoice);        
        guiPanel.add(topPanel, BorderLayout.NORTH);         // This adds the top panel to the top of the GUI
        
        // This label is where the result of the temperature conversion will appear
        resultLabel = new JLabel("", SwingConstants.CENTER);
        guiPanel.add(resultLabel,BorderLayout.CENTER);      // The result label will appear in the centre of the GUI
        getContentPane().add(guiPanel);                     // Adds the panel to the application
    }
    
    /*
     * Some "invisible panels" or "layout areas".
     */
    
    /**
     * The top layout panel.
     */
    JPanel topPanel;
    
    /**
     * The GUI layout panel.
     */
    JPanel guiPanel;
    
    /**
     * A text field to type in the temperature.
     */
    JTextField tempF;
        
    /*
     * Labels to display various states.
     */
    
    /**
     * A label to display the converted temperature.
     */
    JLabel resultLabel;
    
    /**
     * A font label for later, when font size choice is implemented. 
     */
    JLabel fontLabel;
    
    /**
     * The idea is, when you press the button, it converts the temperature for you.
     */
    JButton convertTemp;
    
    /**
     * Firstly setting up an array of font sizes we might use.
     */
    String[] fontSizes = {"8", "10", "11", "12", "14", "16", "18", "20"};
    
    /**
     * This JComboBox offers a drop-down menu of choices.
     */
    JComboBox<String> fontSizeChoice = new JComboBox<>(fontSizes);
    
    /**
     * A useful Unicode character for use later on.
     */
    char deg = '\u00B0';
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        Converter myConv = new Converter();     // Creates Swing frame and container
        myConv.setTitle("Temperature Converter");
        myConv.setSize(400, 300);
        myConv.setDefaultCloseOperation(EXIT_ON_CLOSE);
        myConv.setVisible(true);
    }
    
    /**
     * This method adjusts the font size for various components.
     * @param fontSize  the selected font size.
     */
    private void setComponentFonts(int fontSize) {
        // Sets the font on the button
        Font f = new Font("Helvetica", Font.BOLD, fontSize);
        tempF.setFont(f);
        resultLabel.setFont(f);
        fontLabel.setFont(f);
        convertTemp.setFont(f);
        fontSizeChoice.setFont(f);
    }

    /**
     * React to a button press, or Enter in the text field.
     */
    public void actionPerformed(ActionEvent e) {
        // If the button is pressed or if the return key is pressed (event from the text field)
        if (e.getSource() == convertTemp || e.getSource() == tempF) {
            try {
                // Interpret (parse) degrees Fahrenheit as a double and convert to Celsius
                double tempInF = Double.parseDouble(tempF.getText());
                int tempInC = (int) ((tempInF - 32) / 1.8);

                // Set resultLabel to new value
                resultLabel.setText(tempF.getText() + deg + "F converts to " + tempInC + deg + "C");
            } catch (Exception ex) {
                // Catch to handle the case where the  user didn't type in an understandable number
                System.err.println("No number in textfield");
            }
        }

        if (e.getSource() == fontSizeChoice) {      // If a choice has been made from the JComboBox
            int fontSize = Integer.parseInt((String) fontSizeChoice.getSelectedItem());
            setComponentFonts(fontSize);
        }
    }
}
