/**
 * <p>Practicals 7A and 7B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 7A</b><br />
 * During this practical session you will be looking at web design and accessibility. Specifically, you'll take an
 * example of a typical sort of web page for selling merchandise, and improve the design and make it more accessible.
 * This will:
 * 
 * <ul>
 *     <li>
 *     Increase your understanding of the sorts of design and accessibility issues involved in creating web pages.
 *     </li>
 *     <li>Give you experience in how to cope with a few of the practical problems faced.</li>
 * </ul>
 * 
 * <b>Practical 7B</b><br />
 * In this practical you will do some timing experiments on the Selection Sort and Quick Sort algorithms.
 * 
 * @author  Michael Sammels
 * @version 11.03.2019 - 14.03.2019
 * @since   1.0
 */
package week9;