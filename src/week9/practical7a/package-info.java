/**
 * <p><b>UID 2: Web Design Principles</b></p>
 * <p><b>Web Page for the Red Hat Company</b></p>
 * 
 * The scenario is that the Red Hat Company makes hats. All sorts of hats. Big ones, little ones, high fashion hats
 * for Ascot, cowboy hats, riding hats, bowler hats... we are going to look at one page that displays the information
 * about a particular brand of baseballs caps that the company sells. Just image that it is part of a much bigger
 * website (none of the rest of the pages of the website are implemented, by the way). Imagine that you came to look
 * at this page by navigating from the entry page of the site. Or maybe you went looking for baseballs caps on a search
 * engine and arrived at this page. Anyway, however you got to the page, let's go and have a look at it.
 * 
 * <ol>
 *     <li>
 *     Navigate to <code><b>Groups on Wide\CSCU9A2\</b></code> and you'll find a folder there called
 *     <code>RedHat</code>. Inside there, click on the file <b>{@code hat.html}</b> to bring it up in the web browser.
 *     You can also find it in the source folder for Week 9.
 *     
 *     <br /><br />
 *     
 *     Looks good, huh?
 *     
 *     <br /><br />
 *     
 *     Actually, <b><i>no</i></b>! It might <i>seem</i> to look okay, but actually it contains a lot of mistakes (of
 *     the sort which are unfortunately all too common in pages on the internet these days), and so needs a lot of
 *     improvement.
 *     
 *     <br /><br />
 *     
 *     You might like to pause for a moment now, to see whether you can guess what any of these mistakes might be,
 *     before we look at them in detail.
 *     
 *     <br /><br />
 *     
 *     We're going to concentrate on the improvements to accessibility first, then concentrate on the general design
 *     features (optional - but recommended).
 *     </li>
 *     <li>
 *     You will need a copy of the files involved so that you can work on them. Copy the <code>RedHat</code> folder
 *     into your <code><b>H:\Web</b></code> folder (it is important you copy it to there and not somewhere else!).
 *     As above, you can also copy the <code>RedHat</code> folder from here.
 *     
 *     <br /><br />
 *     
 *     Now make sure that the access permissions are correctly set for your <code>Web</code> folder by double clicking
 *     on the {@code setWeb.cmd} icon in <code>Groups on Wide\CSCU9A2</code>. If you cannot find this file, create
 *     the cmd file manually, using the following code:
 *     
 *     <pre>
 *     {@literal @echo off}
 *     set dir=%homedrive%%homepath%\Web
 *     mkdir %dir%
 *     cacls %dir% /t /e /g Everyone:R
 *     echo %dir% is now accessible to the Web server
 *     pause
 *     </pre>
 *     </li>
 *     <li>
 *     No go back to your browser and enter the URL for your own copy of the page, which will be of the form:
 *     
 *     <br /><br />
 *     <code>http://www.students.stir.ac.uk/~mis00096/RedHat/hat.html</code>
 *     
 *     <br /><br />
 *     
 *     with <code>mis00096</code> <i>replaced</i> by your student username. Make sure that the page comes up as you
 *     saw it before. [Note that this URL can be used from <i>anywhere on the internet</i> to access your web page!]
 *     </li>
 * </ol>
 * 
 * <p><b>Accessibility Testing: WebAIM's WAVE Accessibility Checker</b></p>
 * 
 * WebAIM (webaim.org) provides a free "Accessibility Checker" service called WAVE to  help web developers identify
 * and repair significant accessibility faults.
 * 
 * <ol start="4">
 *     <li>
 *     Visit <a href="http://wave.webaim.org/">http://wave.webaim.org</a> to see whether your page meets WAVE's
 *     approval. Type in the URL as in the previous step (or use copy and paste, since it's a horrible URL to get
 *     right), and press Enter, or click the arrow, to get your page analysed.
 *     </li>
 *     <li>
 *     The report is given in the form of annotations on your web page, and additional information in the panel on
 *     the left hand side. Icons have been added to see what WAVE has found: some indicate things are done
 *     (e.g. <img src="{@docRoot}/resources/week9-prac7a-img01.jpg" alt ="week9-prac7a-img01" />), some are
 *     possible problems (e.g. <img src="{@docRoot}/resources/week9-prac7a-img02.jpg" alt ="week9-prac7a-img02" />),
 *     and some are definite problems (e.g. 
 *     <img src="{@docRoot}/resources/week9-prac7a-img03.jpg" alt ="week9-prac7a-img03" />).
 *     </li>
 *     <li>
 *     The left panel gives a summary of errors, alerts, and so on. If you click the small flag 
 *     <img src="{@docRoot}/resources/week9-prac7a-img04.jpg" alt ="week9-prac7a-img04" /> on the left then you see
 *     more information - and if you click the icons there, then they flash on the main page display.
 *     </li>
 *     <li>
 *     If you click the <code>{@literal <code>}</code> tag at the bottom of the main page, than an extra panel appears
 *     showing the HTML behind the page. Clicking the annotation icons (as above) causes the relevant part of the
 *     HTML to be highlighted.
 *     </li>
 * </ol>
 * 
 * Note that such analysis cannot possibly find all the accessibility problems on a web page, so it is very important
 * that you also examine your page by eye to ensure it meets accessibility guidelines. Can you see anything that might
 * be bad about this page that has not been picked up by WAVE?
 * 
 * <p><b>Improving Accessibility</b></p>
 * 
 * WAVE has identified one definite accessibility error, but there are more problems than that. Now we will identify
 * and fix these errors. You will need to use:
 * <ol>
 *     <li>A graphical browser (such as Firefox or Internet Explorer) open at the web page, and</li>
 *     <li>
 *     A text editor for editing the page <code>hat.html</code>, such as TextPad (or Notepad or WordPad). You can find
 *     these by right clicking on the <code>hat.html</code> icon (<b>Open with...</b>) or via the Start menu. Load
 *     the HTML page from the file <code>hat.html</code>
 *     </li>
 * </ol>
 * 
 * Feel free to go and have a look around the HTML for the page first, before we start making alterations.
 * 
 * <br /><br />
 * 
 * Note: there are sometimes network delays which mean that viewing the changes to your web page via the external
 * URL (<code>http://www.students.stir.ac.uk/~mis00096/RedHat/hat.html</code>) can be delayed. If you find this
 * happens, then it might be easier to open the file locally in the browser instead while you're making rapid changes
 * to the HTML (ask if you do not know how to do this).
 * 
 * <p><b>Images</b></p>
 * 
 * One of the biggest problems if you were to view this page in a text-only browser or with a screen reader is that
 * the paragraph of description about the baseball caps will not get displayed. This is because the paragraph <i>isn't
 * really text at all</i>, it's an <i>image</i> of some text, contained in the file <code>description.gif</code>, but
 * there is not "alt" attribute providing alternative text. This is the accessibility <b><i>error</i></b>
 * <img src="{@docRoot}/resources/week9-prac7a-img03.jpg" alt ="week9-prac7a-img03" /> identified by WAVE.
 * 
 * <br /><br />
 * 
 * In general, images containing text are not a problem <i>per se</i>, but they <b>must</b> have alternative text
 * provided. It's a priority to <i>provide alternative text for all images</i> in the W3C accessibility guidelines.
 * 
 * <br /><br />
 * 
 * In <code>hat.html</code> the image <code>description.gif</code> does not have any alternative text provided. But
 * the image is actually a whole paragraph of text! It would make for a very long "alt" attribute in the HTML. A much
 * better solution would be <i>to not have the paragraph as an image at all</i>, but simply as text. This will not
 * only allow anyone using a screen reader to access the text, but it will improve the page for everyone: text is
 * <i>much</i> smaller in size than an image, so it will speed up page downloading time.
 * 
 * <ol start="8">
 *     <li>
 *     Get rid of the textual image as follows. Scroll nearly to the bottom of the HTML and look for the line where
 *     it says:
 *     
 *     <pre>{@literal <!-- See Practical Change 1}</pre>
 *     Just above there, is the line
 *     <pre>
 *     {@literal <img class="hats" src="description.gif" width="405"
 *                       height="221" />}
 *     </pre>
 *     Remove this line, and also remove the single line containing
 *     <pre>{@literal <!-- See Practical Change 1}</pre>
 *     and also the single line lower down
 *     <pre>{@literal See Practical Change 1 -->}</pre>
 *     This just removes the HTML comments, so that you should be left with the text for the paragraph.
 *     
 *     <br /><br />
 *     
 *     Check that it worked: save your file, then refresh/reload your page in the graphical browser and see what the
 *     difference is. Note that "ordering" links should appear in a nicer place now too! You can re-check with WAVE
 *     too! <br />
 *     [Note: if the page doesn't refresh immediately, try holding down the CTRL key as you press the refresh button.
 *     This will force the browser to refresh the page.]
 *     </li>
 * </ol>
 * 
 * The other problem with the images is that for some of the images the alternative text is unsuitable! The WAVE
 * report shows the alternative text and you may be able to see the "alt" text come up in a graphical browser, when
 * you hover your mouse over the images (depending on the browser and its settings). WAVE indicates some text it
 * does not like, but it cannot in general work out whether the alternative text is sensible or not. <i>So you do
 * have to check this for yourself</i>.
 * 
 * <ol start="9">
 *     <li>
 *     In particular, there is no sensible alternative text for the logo name (<code>redhat.jpg</code>). WAVE reports
 *     it as "Suspicious alternative text"!
 *     
 *     <br /><br />
 *     
 *     Find the "img" tag that mentions <code>redhat.jpg</code>, towards the top of the file, in the line
 *     
 *     <pre>
 *     {@literal <img class="logo" src="redhat.jpg" width="156" height="76" 
 *     alt="Logo" />}
 *     </pre>
 *     
 *     We now need to think of a suitable alternative text for the logo. This is currently just "Logo", which isn't
 *     very helpful! It doesn't tell you what the logo picture is, nor of what company it is a logo for! I suggest
 *     changing the "alt" text (2 lines down from the "img" tag) to something more descriptive, like "Picture of
 *     Red Hat (logo)". Do this now.
 *     
 *     <br /><br />
 *     
 *     Reload the page in your graphical browser and ensure the "alt" tag has changed. (You could test it in WAVE
 *     too).
 *     </li>
 *     <li>
 *     The second problematic "alt" text is the one for the picture of the baseball caps. Scroll to find the line
 *     
 *     <pre>
 *     {@literal <img class="hats" src="twohats.jpg" width="260" height="240"
 *                            alt="Caps" />}
 *     </pre>
 *     
 *     Simply "Caps" isn't very helpful as an alternative text - although WAVE did <i>not</i> find this "suspicious".
 *     The site sells lots of caps! How do we know which caps are depicted? We need to describe the picture a bit
 *     better.
 *     
 *     <br /><br />
 *     
 *     Replace the "alt" text with a more appropriate phrase, such as "Photo of Programming Geek Baseball Caps, Item
 *     IX476".
 *     </li>
 *     <li>Save your file again, then go and test the page once more.</li>
 * </ol>
 * 
 * <p><b>Title</b></p>
 * 
 * A page author should always think about what title should be used for a page, and choose a meaningful one. After
 * all, if a user bookmarks that page (adds it to their Favourites), the <i>title</i> of the page is <i>what gets
 * used for the bookmark</i>!. So if a page is untitled, it will look meaningless in someone's bookmarks.
 * 
 * <br /><br />
 * 
 * The title of this web page is currently "Red Hat Co.", as you should be able to see from the title in the very top
 * of your browser window. It's not bad as a meaningful title, but it could be better. After all, it conveys that
 * that it's a page at the Red Hat Co's website, but not <i>which</i> page it is of the site. A more meaningful title
 * would be something like "Red Hat Co.: Programming Geeks Baseballs Caps".
 * 
 * <ol start="12">
 *     <li>
 *     Change the title of the web page. You'll need to change the line at the top of the HTML file that reads:
 *     
 *     <pre>{@literal<title>}Red Hat Co.{@literal </title}></pre>
 *     
 *     Check that your change worked okay.
 *     </li>
 * </ol>
 * 
 * <p><b>Meaningful Link Names</b></p>
 * 
 * Recall that blind people read by pages by <i>first</i> listing all the links in the page, <i>then</i> following the
 * one they want, to quickly navigate to the page of interest; only then do they bother reading the whole page. So
 * let's have a look at the links we have on the page:
 * 
 * <br /><br />
 * 
 * <ul style="list-style-type: none; font-family:'Lucida Console', monospace;">
 *     <li><u>Baseball Caps</u></li>
 *     <li><u>Cowboy Hats</u></li>
 *     <li><u>High Fashion</u></li>
 *     <li><u>Top Hats</u></li>
 *     <li><u>click here</u></li>
 *     <li><u>click here</u></li>
 * </ul>
 * 
 * Any idea what "click here" could refer to from just that list above? No, neither have I. An accessibility guideline
 * is: <i>Create link phrases that make sense when read out of context</i>.
 * 
 * <ol start="13">
 *     <li>
 *     Replace the two lines (in between <code>{@literal <li>}</code> and <code>{@literal </li>}</code> tags),
 *     
 *     <pre>
 *     To order one in black, {@literal <a href="notimplemented.html">click here</a>}.
 *     To order one in yellow, {@literal <a href="notimplemented.html">click here</a>}.
 *     </pre>
 *     
 *     with
 *     
 *     <pre>
 *     {@literal <a href="notimplemented.html">}Order in black{@literal </a>}
 *     {@literal <a href="notimplemented.html">}Order in yellow{@literal </a>}
 *     </pre>
 *     
 *     respectively. Check that the changes worked.
 *     </li>
 * </ol>
 * 
 * <p><b>Colour</b></p>
 * 
 * Remember that a major colour design guideline is to have strong light/dark contrasts between text and background.
 * This is also one of the guidelines: "<i>Check that the foreground and background colors contrast sufficiently
 * with each other</i>".
 * 
 * <ol start="14">
 *     <li>Go and look at what text/background colours there are on the site. Are they all okay, or do some of them
 *     need changing?
 *     </li>
 * </ol>
 * 
 * The colour contrasts between text and background you should have seen (unless you've got unusual settings on your
 * browser!) are:
 * 
 * <br /><br />
 * 
 * <table style="width: 100%;">
 *     <caption>Design Guidelines</caption>
 *     <tr>
 *         <td>Black or dark red text on white background (the links at the top)</td>
 *         <td>dark/light</td>
 *         <td>&#10004;</td>
 *     </tr>
 *     <tr>
 *         <td>Black text ("Baseball Caps") on a light grey background</td>
 *         <td>dark/light</td>
 *         <td>&#10004;</td>
 *     </tr>
 *     <tr>
 *         <td>Black text on a red background ("...for Programming Geeks")</td>
 *         <td>dark/medium</td>
 *         <td><b>x</b></td>
 *     </tr>
 *     <tr>
 *         <td>White text on a red background (the description)</td>
 *         <td>light/medium</td>
 *         <td><b>x</b></td>
 *     </tr>
 * </table>
 * 
 * <br /><br />
 * 
 * The first two are okay, but the second two are not. It may be that to <i>you</i>, with your eyesight, on the
 * monitor you're using, the contrast between the red and the white/black if sufficient. But people vary in how they
 * see colour, and monitors and printers especially vary in how they present colour. That's why a very strong contrast
 * is recommended. So we need to fix the text on the red background. There are various possible ways to do this.
 * One possible way, which matches the "grey tab" graphic, is to use black text on the same light grey ("silver")
 * background.
 * 
 * <ol start="15">
 *     <li>
 *     To make the change to the text colour, you need to edit the file <code>redhat.css</code> (this is the one whose
 *     icon has little cogs in it, rather than the picture one). In <code>redhat.css</code> find near the top of the
 *     file the part of the stylesheet that says
 *     
 *     <pre>
 *     p.small {
 *     color: white;    {@literal /*} white coloured text *&#47;
 *     font-size: 10pt;
 *     font-family: lucida;
 *     }
 *     </pre>
 *     
 *     Change the mention of "white" to black, so that you now have
 *     
 *     <pre>
 *     p.small {
 *     color: black;    {@literal /*} black coloured text *&#47;
 *     font-size: 10pt;
 *     font-family: lucida;
 *     </pre>
 *     
 *     Also make the same changes to {@code ul.order}, just below {@code p.small}.
 *     
 *     <br /><br />
 *     
 *     (This changes the style so that all paragraphs are written with black text, and since the only text we want
 *     to change is in a paragraph, this will do for this particular HTML file.)
 *     </li>
 *     
 *     <li>
 *     Also in <code>redhat.css</code> find:
 *     
 *     <pre>
 *     div.content {
 *     border: solid silver;
 *     background-color: red;    {@literal /*} red coloured background *&#47;
 *     height: 240px;
 *     }
 *     </pre>
 *     
 *     and change the background colour of red to silver.
 *     
 *     <br /><br />
 *     
 *     Save your CSS file and check that your changes worked by refreshing <code>hat.html</code> in your browser.
 *     </li>
 * </ol>
 * 
 * A guideline is: <i>if you use color to convey information, make sure the information is also represented another
 * way</i>. In particular, remember about problems with certain colours for colour-blind people. Whoever implemented
 * this page has fallen foul of this guideline - see the next paragraph!
 * 
 * <br /><br />
 * 
 * Take a look at the information listed about the baseball caps. See the little coloured square before the ordering
 * links? The designer of the website originally had the thought to inform the customer whether an item is currently
 * in stock or not, which is a perfectly reasonable idea. But, the designer also thought to represent an item being
 * in stock with a little green square, and it being out of stock with a little red square. This is <i>not</i> a
 * reasonable idea, as it leaves anyone red-green colour blind unsure just from looking at the square exactly what
 * colour it is. It would be much better to not use colour at all to represent this information, or to provide an
 * alternative, such as a text message too.
 * 
 * <ol start="17">
 *     <li>
 *     Find the lines that read
 *     
 *     <pre>
 *     {@literal <li><a href="notimplemented.html">Order in black</a></li>}
 *     {@literal <li><a href="notimplemented.html">Order in yellow</a></li>}
 *     </pre>
 *     
 *     and just before each {@code </li>} add the text:
 *     
 *     <pre>(currently in stock)</pre>
 *     
 *     Much clearer! And n need to remember what colour means what, either. <br />
 *     Save your file and check that the change worked.
 *     </li>
 * </ol>
 * 
 * <p><b>Fonts</b></p>
 * 
 * Another consideration for web page authors is fonts. The web page currently specifies the {@code Lucida} family
 * of fonts, which may or may not be visible on your browser, depending on what fonts your browser has, and what
 * the browser preferences are set to. Generally the {@code Lucida} font is reasonably readable on-screen, so we
 * don't need to make any further changes there, but you may (depending on your eyesight, your monitor and your
 * browser settings) be able to see why we need to make changes to the font size - it's rather small!
 * 
 * <br /><br />
 * 
 * Currently the web page specifies fixed font sizes for some of its text in the CSS file, <code>redhat.css</code>,
 * for example:
 * 
 * <pre>
 * p.small {
 * color: white;    {@literal /*} white coloured text *&#47;
 * font-size: 10pt;
 * font-family: lucdia;
 * </pre>
 * 
 * Specifying fixed font sizes, like 10pt, is <b><i>a bad idea</i></b>, because people with poor vision may have
 * difficulty making out the words in a particular size. Instead, it's better to specify larger or smaller fonts
 * relative to the standard size for the document (which may be set by the user).
 * 
 * <ol start="18">
 *     <li>
 *     Change the font for {@code p.small} to use the standard size (whatever that might be), by specifying:
 *     
 *     <pre>font-size: 100%;</pre>
 *     
 *     and make this same change to the "order" text by changing the font specification for {@code ul.order}
 *     (further down the page in <code>redhat.html</code>).
 *     </li>
 *     <li>
 *     Now change the font for {@code p.big} to use 120% of the standard size (instead of 12pt), by specifying:
 *     
 *     <pre>font-size: 120%;</pre>
 *     
 *     Check that these changes have worked: save your CSS file, then refresh/reload your HTML page in the graphical
 *     browser and see whether that makes the text more readable.
 *     </li>
 * </ol>
 * 
 * <p><b>WAVE Testing (again)</b></p>
 * 
 * <ol start="20">
 *     <li>
 *     Go back to WAVE and test your page again. You should hopefully by now have fixed all the problems WAVE
 *     identified!
 *     
 *     <br /><br />
 *     
 *     Note that the WAVE report does not contain things that WAVE cannot test automatically. For example, it can't
 *     tell just by looking at the HTML, whether you've relied on colour to convey information. You should have
 *     fixed this problem earlier. If you've followed all the steps above then your page should comply with the
 *     issues that WAVE raises.
 *     </li>
 * </ol>
 * 
 * @author  Michael Sammels
 * @version 11.03.2019
 * @since   1.0
 */
package week9.practical7a;