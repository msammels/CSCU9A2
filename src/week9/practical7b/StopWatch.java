package week9.practical7b;

/**
 * CSCU9A2 - Week 9 <br />
 * Practical 7B <br />
 * <code>StopWatch.java</code>
 * 
 * <p>
 * A stop watch that accumulates time when it is running. You can repeatedly start and stop the stop watch. You can
 * use a stop watch to measure the running time of a program.
 * </p>
 * 
 * @author  Michael Sammels
 * @version 14.03.2019
 * @since   1.0
 */

public class StopWatch {
    /**
     * Constructor.
     */
    public StopWatch() { reset(); }
    
    /**
     * Variables to hold the elapsed time and the start time.
     */
    
    /**
     * Variable to hold the elapsed time.
     */
    private long elapsedTime;
    
    /**
     * Variable to hold the start time.
     */
    private long startTime;
    
    /**
     * Variable to check whether or not we are currently running.
     */
    private boolean isRunning;
    
    /**
     * Starts the stop watch. Time starts accumulating now.
     */
    public void start() {
        if (isRunning) { return; }
        
        isRunning = true;
        startTime = System.currentTimeMillis();
    }
    
    /**
     * Stops the stop watch. Time stops accumulating and is added to the elapsed time.
     */
    public void stop() {
        if (!isRunning) { return; }
        
        isRunning = false;
        long endTime = System.currentTimeMillis();
        elapsedTime = elapsedTime + endTime - startTime;
    }
    
    /**
     * Returns the total elapsed time.
     * @return  The total elapsed time.
     */
    public long getElapsedTime() {
        if (isRunning) {
            long endTime = System.currentTimeMillis();
            return elapsedTime + endTime - startTime;
        } else {
            return elapsedTime;
        }
    }
    
    /**
     * Stops the watch and resets the elapsed time to 0.
     */
    private void reset() {
        elapsedTime = 0;
        isRunning = false;
    }
}
