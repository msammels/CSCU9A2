/**
 * <p><b>Sorting Experiments</b></p>
 * <p><b>Timing Selection Sort</b></p>
 * 
 * <ol>
 *     <li>
 *     In a slight variation of what you have done in previous practicals, copy the <b><i>folder</i>
 *     <code>SelectionSort</code></b> from <code><b>Groups on Wide (V:)\CSCU9A2\Java</b></code> to your CSCU9A2
 *     working folder (the code will not be listed in this documentation as there are four different files of varying
 *     length - they can still be seen, completed here:
 *     
 *     <ul>
 *         <li>{@link ArrayUtil#ArrayUtil ArrayUtil}</li>
 *         <li>{@link QuickSortTimer#QuickSortTimer QuickSortTimer}</li>
 *         <li>{@link SelectionSortTimer#SelectionSortTimer SelectionSortTimer}</li>
 *         <li>{@link StopWatch#StopWatch StopWatch}</li>
 *     </ul>
 *     
 *     Launch BlueJ. Using the <b>Project</b> menu, <b>Open Non BlueJ...</b> create a BlueJ project in your
 *     <code><b>SelectionSort</b></code> folder - remember, when you navigate to it click <i>once</i> on the folder
 *     name then Open in BlueJ - do not double click to open the folder.
 *     
 *     <br /><br />
 *     
 *     Look at the classes: the structure is identical to the selection sort timer example seen in the lectures:
 *     <b>{@link ArrayUtil#ArrayUtil ArrayUtil}</b> is a general purpose <i>library</i> of useful methods, seen in lectures.
 *     <b>{@link StopWatch#StopWatch StopWatch}</b> is a special purpose helper class to carry out timing (it is not
 *     necessary to read and understand this class). <code><b>SelectionSortTimer</b></code> is the main program - it
 *     contains a <b>{@code main}</b> method that can be launched, and the
 *     <b>{@link SelectionSortTimer#selectionSort selectionSort}</b> method.
 *     
 *     <br /><br />
 *     
 *     <b>{@code SelectionSortTimer}</b> invites the user to enter the size of the array to be sorted, creates a new
 *     array of the appropriate size, fills it with random integers, and then runs the
 *     <b>{@link SelectionSortTimer#selectionSort selectionSort}</b> method to sort it, displaying the unsorted and
 *     sorted arrays in the terminal window. A new <b>{@link StopWatch#StopWatch StopWatch}</b> is created and used to time
 *     the sorting
 *     process: it is <i>started</i> just <i>before</i> the call of {@link SelectionSortTimer#selectionSort selectionSort},
 *     is <i>stopped</i> immediately <i>afterwards</i>, and the elapsed time (in milliseconds) is reported in the terminal
 *     window. Placing <b>{@code start}</b> and <b>{@code stop}</b> just before and after the <b>{@code selectionSort}</b>
 *     call means that JVM start-up and program loading times are <i>not included</i> in the measurement of the
 *     elapsed time. The main program has no GUI, it just runs in BlueJ's terminal window - it must be launched
 *     repeatedly to carry out experiments.
 *     
 *     <br /><br />
 *     
 *     Compile and run the program - it should be fine - and carry out the experiments below.
 *     </li>
 *     <li>
 *     To see the basic action of the program: launch the <b>{@code SelectionSortTimer#main}</b> method, enter 10 at
 *     the prompt in the terminal window, and look at the resulting output. Repeat this, entering 10 at the prompt
 *     again: notice that the array contains <i>different</i> numbers from the last time - the random number generator
 *     is working well! Repeat several times, entering, say 20, 30, 100 for the array size. Everything should be fine.
 *     You will see that the elapsed time is (probably) reported as 0 milliseconds - that is not an error, the computer
 *     is simply so fast that it takes less than 1/1000 of a second to sort the array!
 *     </li>
 *     <li>
 *     Run <b>{@link SelectionSortTimer#SelectionSortTimer SelectionSortTimer}</b> again, this time entering 1000. The
 *     terminal window output starts to get a bit silly: many, many random numbers being displayed that are not actually
 *     very interesting, followed by the elapsed time which <i>is</i> interesting in this practical. Locate the two lines
 *     in the main method that use <b>{@code System.out.println}</b> to display the array, and <i>comment them out</i>.
 *     </li>
 *     <li>
 *     Now for some real timings: the best array sizes here depend on how fast your computer actually is, so
 *     experimentation may be needed. Try array sizes 000, 10000, 100000 (and maybe some values in between) until you
 *     get a time that is perhaps around 100 or so milliseconds (the exact time does not matter) – on my PC it is an
 *     array size of 8000. In the instructions below, the size that you have found is referred to as the
 *     <b><i>"basic size"</i></b>.
 *     </li>
 *     <li>
 *     Repeat the timing for the basic size <i>five separate times</i>, and record the elapsed times. You should see
 *     that the timing varies quite a lot: this is because firstly, the JVM may be doing other management activities
 *     "behind the scenes" that add unpredictably to the time, and secondly Windows itself may be doing the same.
 *     This is a common problem with trying to <i>measure true execution times</i> by recording <i>overall elapsed
 *     times</i>. We will assume that an average of the measured times is a good representative of actual execution
 *     time. In some sorting algorithms, the actual time also varies with the precise collection of random numbers
 *     generated, but selection sort does not have that characteristic.
 *     
 *     <br /><br />
 *     
 *     So, calculate the average time to selection sort the basic size of an array.
 *     </li>
 *     <li>
 *     Now try a range of array sizes building on that basic size, such as 2x the basic size, 5x, 10x, 20x, 50x,
 *     100x, etc. For each size, repeat the measurement, say, five times, <b><i>write down the time taken for each,
 *     and then calculate the average time for each array size</i></b>.
 *     </li>
 *     <li>
 *     Sketch a graph showing your results with the array size along the horizontal x axis, and the time taken up the
 *     y axis. You should be able to see clearly that a line joining the points curves upwards towards the right.
 *     </li>
 *     <li>
 *     Compare various pairs of average timings for array sizes <i>where one is double the other</i>, for example
 *     the basic size and 2x, or 10x and 20x, etc. In each case the ratio should be roughly 4 - meaning that if you
 *     double the amount of data that <b>{@code selectionSort}</b> has to sort, then it takes about 4 times as long.
 *     </li>
 * </ol>
 * 
 * <br /><hr />
 * 
 * <p><b>An Introduction to QuickSort</b></p>
 * 
 * QuickSort is a <i>recursive</i> sorting method, rather like merge sort - more details below. The merge sort
 * algorithm splits an array into two equal partitions (copying into two new arrays), recursively merge sorts each
 * partition, and then merges the sorted partitions back into the original array. The performance of both QuickSort
 * and merge sort is stated as O(n log n) - slower than a "linear" O(n) algorithm (there are no general purpose
 * sorting algorithms that are that efficient), but <i>faster</i> than a "quadratic" O(n<sup>2</sup>) algorithm
 * such as selection sort.
 * 
 * <br /><br />
 * 
 * QuickSort works roughly like this to sort an array:
 * 
 * <ol>
 *     <li>
 *     Pick some value, the <i>first</i> value in the array is convenient (although not always the best) - this is
 *     called the "pivot".
 *     </li>
 *     <li>
 *     In one (intricate) scan of the array, rearrange the values (within the array, no new arrays needed), so that
 *     all values less than the pivot are in lower indication locations, and all values greater than the pivot are
 *     in higher index locations with the pivot value in between. This gives two partitions that are already in the
 *     correct region of the array and never need to be compared with each other.
 *     </li>
 *     <li>Recursively QuickSort the two partitions.</li>
 *     <li>Job done.</li>
 * </ol>
 * 
 * The QuickSort algorithm has already been coded for you in this practical.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Timing QuickSort</b></p>
 * 
 * <ol>
 *     <li>
 *     In the same way as above, copy the <b><i>folder</i> <code>QuickSort</code></b> from <code><b>Groups on Wide
 *     (V:)\CSCU9A2\Java</b></code> to your CSCU9A2 working folder.
 *     
 *     <br /><br />
 *     
 *     Launch BlueJ. Using the <b>Project</b> menu, <b>Open Non BlueJ...</b> create a BlueJ project in your
 *     <code><b>QuickSort</b></code> folder - remember, when you navigate to it click once on the folder name then
 *     Open in BlueJ - do not double click to open the folder.
 *     
 *     <br /><br />
 *     
 *     Look at the classes: the structure is identical to the selection sort timer example above. This time the main
 *     program is <b>{@link QuickSortTimer#QuickSortTimer QuickSortTimer}</b>. <b>{@code QuickSortTimer}</b> is slightly
 *     different from <b>{@code SelectionSortTimer}</b>: <i>within one run</i>, it repeatedly prompts the user for the size
 *     of an array, constructs a new array of that size containing random integers, measures the time for QuickSort to
 *     process the array and then reports the time to the user. It is easier to carry out repeated timings. The
 *     repetition is coded as a "{@code while (true)}" loop - automatically an infinite loop, but containing an
 *     {@code if} statement that "{@code break}s" the loop if a negative array size is requested. There is a short
 *     commented-out section that builds the array to be sorted in a different way, for use later.
 *     
 *     <br /><br />
 *     
 *     Compile and run the program - it should be fine - and carry out the experiments below.
 *     </li>
 *     <li>
 *     Now for some timings: again the best array sizes here depend on how fast your computer actually is, so
 *     experimentation may be needed. Try array sizes 1000, 10000, 100000, 1000000 (and maybe some values in between)
 *     until you get a time that is perhaps around 100 or so milliseconds (the exact time does not matter) – on my PC
 *     it is about 1000000. You should already get the feeling that this is very different from selection sort! In the
 *     instructions below, this is referred to as the <b><i>"basic size"</i></b>.
 *     </li>
 *     <li>
 *     Now try a range of array sizes building on that basic size, such as 2x the basic size, 5x, 10x, 20x, 50x, 100x,
 *     etc. For each size, repeat the measurement, say, five times, <b><i>write down the time taken for each, and then
 *     calculate the average time for each array size</i></b>.
 *     </li>
 *     <li>
 *     Sketch a graph showing your results. Remember that QuickSort is an O(n log n) algorithm – the graph should just
 *     curve gently upwards. Add to the graph a sketch of what an O(n<sup>2</sup>) algorithm would look like: take the
 *     timing for the basic size, multiply it by 4 to get what the timing for 2x would be, by 16 to get what the
 *     timing for 4x would be and so on. Compare the two lines on the graph.
 *     
 *     <br /><br />
 *     
 *     Try to estimate how long selection sort would take to sort the largest array that you tried with QuickSort.
 *     Compare the two timings.
 *     </li>
 *     <li>
 *     Now open the <b>{@link QuickSortTimer#QuickSortTimer QuickSortTimer}</b> class in BlueJ’s editor, comment out the
 *     line that creates an array of random values, and uncomment the five lines below it that create an already sorted
 *     array of values.
 *     
 *     <br /><br />
 *     
 *     Try doing your sorting timings again. For large arrays it should fail disastrously with an <i>exception</i> –
 *     if you scroll to the top of the exception report, you will see that it is a “{@code stack overflow}”: there
 *     have been too many recursive method calls inside each other for the JVM to keep track of and it has run out
 *     of memory space and has given up! There is no easy way to fix this in BlueJ, so open a command window at the
 *     folder with your project (review the earlier lab practical if you cannot remember how), and run your program
 *     like this:
 *     
 *     <pre>java –Xss100M QuickSortTimer</pre>
 *     
 *     The –Xss100M tells the JVM to allocate 100Mbytes of memory for its “thread stack”, and you should now be able
 *     to repeat some of your timing measurements without getting exceptions.
 *     
 *     <br /><br />
 *     
 *     Record some timings and sketch the graph – it is bad: QuickSort has now become an O(n<sup>2</sup>) algorithm!
 *     It only behaves well if the starting array is randomly ordered, otherwise all values fall into just one
 *     partition, and it behaves similarly to selection sort! For best behaviour QuickSort is <i>supposed</i> to use
 *     the median value for partitioning the array, and not the first value (then it always performs well) but finding
 *     the median value is hard and time consuming!
 *     </li>
 * </ol>
 * 
 * @author  Michael Sammels
 * @version 14.03.2019
 * @since   1.0
 */
package week9.practical7b;