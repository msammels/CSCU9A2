package week9.practical7b;

import java.util.Scanner;

/**
 * CSCU9A2 - Week 9 <br />
 * Practical 7B <br />
 * <code>QuickSortTimer.java</code>
 * 
 * <p>
 * This program measures how long it takes to sort an array of user-specified size with the QuickSort algorithm.
 * 
 * <br /><br />
 * 
 * The core of the program is a loop that allows repeated timings until the user enters a negative number as the
 * array size.
 * </p>
 * 
 * @author  Michael Sammels
 * @version 14.03.2019
 * @since   1.0
 */

public class QuickSortTimer {
    /**
     * Constructor.
     */
    public QuickSortTimer() {}
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        System.out.println("Timing QuikSort\n");
        try (Scanner in = new Scanner(System.in)) {
            while (true) {
                System.out.print("Enter size of random array to sort, negative to quit: ");
                int n = in.nextInt();
                
                if (n < 0) {    // Stop now?
                    break;      // Yes, escape from the while loop
                }
                
                // Construct random array: n random values, each in the range of 0 to n-1
                int[] a = ArrayUtil.randomIntArray(n, n);
                
                // Use StopWatch to time selection sort
                StopWatch timer = new StopWatch();
                
                timer.start();
                quickSort(a);
                timer.stop();
                
                System.out.println("To sort " + n + " elements:\n "
                        + "Elapsed time: " + timer.getElapsedTime() + " milliseconds\n");
            }
        }
        
        System.out.println("\nFinished");
    }
    
    /**
     * Sorts an array, using Quick Sort.
     * @param a the array to sort.
     */
    private static void quickSort(int[] a) {
        quickSort(a, 0, a.length - 1);
    }
    
    /**
     * Sorts a portion of an array, using Quick Sort.
     * @param a     the array to sort.
     * @param from  the first index of the portion to be sorted.
     * @param to    the last index of the portion to be sorted.
     */
    private static void quickSort(int[] a, int from, int to) {
        if (from >= to) { return; }
        int p = partition(a, from, to);
        quickSort(a, from, p);
        quickSort(a, p + 1, to);
    }
    
    /**
     * Partitions a portion of an array.
     * @param a     the array to partition.
     * @param from  the first index of the portion to be partitioned.
     * @param to    the last index of the portion to  be partitioned.
     * @return      The last index of the first partition.
     */
    private static int partition(int[] a, int from, int to) {
        int pivot = a[from];
        int i = from - 1;
        int j = to + 1;
        
        while (i < j) {
            i++; while (a[i] < pivot) { i++; }
            j--; while (a[j] > pivot) { j--; }
            
            if (i < j) {
                ArrayUtil.swap(a,  i,  j);
            }
        }
        return j;
    }
}
