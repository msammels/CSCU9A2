package week9.practical7b;

import java.util.Random;

/**
 * CSCU9A2 - Week 9 <br />
 * Practical 7B <br />
 * <code>ArrayUtil.java</code>
 * 
 * <p>This class contains utility methods for array manipulation.</p> 
 * 
 * @author  Michael Sammels
 * @version 14.03.2019
 * @since   1.0
 */

public class ArrayUtil {
    /**
     * Constructor.
     */
    public ArrayUtil() {}
    
    /**
     * Generates a random integer.
     */
    private static final Random generator = new Random();
    
    /**
     * Creates an array filled with random values.
     * @param length    the length of the array.
     * @param n         the number of possible random values.
     * @return          An array filled with numbers with a length between 0 and n-1.
     */
    public static int[] randomIntArray(int length, int n) {
        int[] a = new int[length];
        for (int i = 0; i < a.length; i++) {
            a[i] = generator.nextInt(n);
        }
        return a;
    }
    
    /**
     * Swaps two entries of an array.
     * @param a the array.
     * @param i the first position to swap.
     * @param j the second position to swap.
     */
    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    
    /**
     * Finds the smallest element in a tail range of the array.
     * @param a     the array to search.
     * @param from  the first position in the array to compare.
     * @return      The position of the smallest element in the range a[from] ... a[a.length - 1].
     */
    public static int minimumPosition(int[] a, int from) {
        int minPos = from;
        for (int i = from + 1; i < a.length; i++) {
            if (a[i] < a[minPos]) {
                minPos = i;
            }
        }
        return minPos;
    }
}
