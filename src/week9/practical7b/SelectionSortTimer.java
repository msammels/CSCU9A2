package week9.practical7b;

import java.util.Arrays;
import java.util.Scanner;

/**
 * CSCU9A2 - Week 9 <br />
 * Practical 7B <br />
 * <code>SelectionSortTimer.java</code>
 * 
 * <p>
 * This program measures how long it takes to sort an array of a user-specified size with the selection sort
 * algorithm.
 * </p>
 * 
 * @author  Michael Sammels
 * @version 14.03.2019
 * @since   1.0
 */

public class SelectionSortTimer {
    /**
     * Constructor.
     */
    public SelectionSortTimer() {}
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Enter array size: ");
            int n = in.nextInt();
            
            // Construct random array of n values, each in the range of 0 to n-1
            int[] a = ArrayUtil.randomIntArray(n, n);
            
            // Display the new array on the terminal
            System.out.println(Arrays.toString(a) + "\n\n");
            
            // Use stop watch to time selection sort
            StopWatch timer = new StopWatch();
            
            timer.start();
            selectionSort(a);
            timer.stop();
            
            // Display the sorted array on the terminal
            System.out.println(Arrays.toString(a) + "\n\n");
            
            System.out.println("Elapsed time: " + timer.getElapsedTime() + " milliseconds");
        }
    }
    
    /**
     * Sorts an array using selection sort.
     * @param a the array to sort.
     */
    private static void selectionSort(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            int minPos = ArrayUtil.minimumPosition(a, i);
            ArrayUtil.swap(a, minPos, i);
        }
    }
}
