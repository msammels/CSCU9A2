/**
 * <p><b>Brookshear Machine Programming</b></p>
 * 
 * Launch a browser and visit <a href="http://joeledstrom.github.io/brookshear-emu/">this</a> page. This shows a simple
 * Brookshear Machine simulator:
 * 
 * <ul>
 *     <li>
 *     The main memory is in the centre, with byte address 00 at the top left, 01 next to it on the right, 02 next
 *     and so on on. If you hover the mouse over a memory location, the inspector shows you its contents. You can
 *     alter the contents of a memory location by highlighting it with the mouse, typing two hexadecimal digits and
 *     then <b>Enter</b> - the highlight moves automatically to the next location ready for altering that.
 *     </li>
 *     <li>
 *     On the far left the contents of sixteen general purpose registers (GPRs) are displayed. They cannot be edited.
 *     </li>
 *     <li>
 *     Next to the registers you can see the Program Counter (PC), Instruction Register (IR), and various control
 *     buttons.
 *     </li>
 *     <li>
 *     To use the Brookshear Machine, you must enter machine instructions from memory address 00 onwards, and then
 *     the CPU should <b>Clea</b>red, which puts 00 into the PC, and then you click either <b>Step</b> or <b>Run</b>.
 *     <b>Run</b> allows the Brookshear-Fetch-Execute cycle to run freely until a Halt instruction is encountered.
 *     Clicking <b>Step</b> repeatedly steps the Brookshear Machine through the stages of the Fetch-Execute cycle so
 *     that you can see what is happening.
 *     </li>
 * </ul>
 * 
 * The Brookshear Machine reference sheet is attached (you can see this in the repository).
 * 
 * <br /><br />
 * 
 * Here is a simple program to add the two numbers in memory locations 80 and 81 together and leave the result in
 * location 82: the memory address is on the left, the assembly code is on the right and the corresponding
 * hexadecimal bytes are in the middle:
 * 
 * <pre>
 * 00    1A80    MOV [80] -> RA
 * 02    1B81    MOV [81] -> RB
 * 04    5CAB    ADDI RA, RB -> RC
 * 06    3C82    MOV RC -> [82]
 * 08    C000    HALT
 * </pre>
 * 
 * Enter the hexadecimal codes into the memory bytes from memory address 00: so 1A into address 00, 80 into the next
 * location, 1B into the next, 81 the next, and so on.
 * 
 * <br /><br />
 * 
 * Before running the program, it needs some data to work on, so enter, say 03 into memory location 80, and 06 into
 * location 81. [So the eventual result should be 09 in location 82.]
 * 
 * <br /><br />
 * 
 * Click <b>Clear</b> and then <b>Run</b> (or the single <b>Clear and Run</b> button). Things happen quickly, and
 * fairly soon the machine halts with the current result in location 82 - check that it is correct!
 * 
 * <br /><br />
 * 
 * Alter the contents of location 82 back to 00.
 * 
 * <br /><br />
 * 
 * Click <b>Clear</b> (the PC goes back to 00), and then this time click <b>Step</b> repeatedly, but quite slowly
 * - after each click look carefully at the CPU: at the PC, the IR and the contents of the registers A, B and C.
 * Make sure you understand what is happening.
 * 
 * <br /><br />
 * 
 * Here is another Brookshear Machine program, this time in assembly language:
 * 
 * <pre>
 * 00    MOV 01 -> RA
 * 02    MOV [8] -> RB
 * 04    ADDI RA, RB -> RC
 * 06    MOV RC -> RB
 * 08    MOV RC -> [80]
 * 0A    JMPEQ 04, R0
 * 0C    HALT
 * </pre>
 * 
 * Work out what this program does. Why is the {@code HALT} instruction not really necessary?
 * 
 * <br /><br />
 * 
 * Translate ("assemble") the program into hexadecimal codes, and enter it into the memory.
 * 
 * <br /><br />
 * 
 * Clear the CPU and run or step through the program. Watch carefully, and check whether your understanding of the
 * program was correct.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>Writing your own Brookshear Program</b></p>
 * 
 * Here is the description of a task computable by the Brookshear Machine:
 * 
 * <ul>
 *     <li>
 *     There will be two numbers in memory locations 80 and 81: let's refer to them as {@code m} and {@code n}. The
 *     number {@code m} <i>must</i> be less than the number {@code n} - for example 01 and 09.
 *     </li>
 *     <li>The program is to compute a result and store it in memory location 82.</li>
 *     <li>
 *     The result should be the sum of the numbers from {@code m} to {@code n} inclusive:
 *     {@code m + (m + 1) + (m + 2) + ... + n}. For example, when {@code m} is 01 and {@code n} is 04 the result
 *     should be {@code A0} (that is ten).
 *     </li>
 * </ul>
 * 
 * Write a Brookshear program to carry out this computation. The first instruction should be at address 00, and there
 * should be a {@code HALT} instruction at the end. You should write the program in Brookshear assembly language and
 * then translate it.
 * 
 * <br /><br />
 * 
 * Hints:
 * 
 * <ul>
 *     <li>Work it out on paper first!</li>
 *     <li>
 *     Think about using a register to count up from {@code m} to {@code n}, adding its value onto the contents of
 *     location 82 at each step.
 *     </li>
 *     <li>Location 82 will need to be started at 00! (Why?)</li>
 *     <li>
 *     Counting up needs a loop <i>very similar</i> to the second Brookshear program in the first part of this
 *     practical sheet.
 *     </li>
 * </ul>
 * 
 * Translate ("assemble") the program into hexadecimal codes, and enter it into the memory.
 * 
 * <br /><br />
 * 
 * Enter your program in the Brookshear Machine. Clear the CPU, insert some initial data into locations 80 and 81, and
 * run or step through the program. Watch carefully, and check whether your program is correct!
 * 
 * @author  Michael Sammels
 * @version 31.01.2019
 * @since   1.0
 */
package week3.practical2b;