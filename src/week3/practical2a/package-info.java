/**
 * <p><b>Structured Development</b></p>
 * 
 * Introduction: one approach to <i>problem solving</i> that often works is the technique called "stepwise refinement"
 * or "structured development". In this approach, a complex problem is first decomposed (or "refined") into a series
 * of simpler subtasks - these subtasks are themselves not yet fully solved, but if they were completed, they would
 * clearly solve the overall problem. We then tackle refining <i>each of the subtasks independently</i>: for each we
 * might decide that it is simple enough that we can produce a complex solution immediately; alternatively we might
 * decide that it is sufficiently complex that the best thing to do is to decompose it into a further series of
 * simpler subtasks. This process continues until there are no subtasks that have not been solved.
 * 
 * <br /><br />
 * 
 * The web page <a href="http://c2.com/cgi/wiki?StepwiseRefinement">here</a> is interesting to read, and shows a
 * stepwise refinement of an initial problem "Brush teeth".
 * 
 * <br /><br />
 * 
 * Stepwise refinement in programming usually works like this: the goal is to produce a <i>method</i> to solve some
 * problem. Assume the problem is reasonably complex, so the method's body is a decomposition of the problem into
 * simpler steps, each of which is a <i>call of a further method</i> that will solve a particular subtask. At this
 * point, declarations of those further methods can be added to the program with method headers that match the way the
 * method is called, but with <i>empty method bodies</i> (just a pair of {}). For each method we will also have a
 * description of what its purpose is (preferably as a comment in the program). We then tackle each method
 * <i>separately</i>: for each method, perhaps we can design the full Java code for its body reasonably easily, but
 * alternatively, if the method's purpose is sufficiently complex, then we may refine its body to a series of
 * further method calls (and also adding corresponding empty method declarations to the program). When all methods
 * have fully programmed bodies, then the program is complete - it might even have been partially executable at
 * intermediate stages.
 * 
 * <br /><br />
 * 
 * You are going to carry out a stepwise refinement of a problem based on one that appeared in Horstmann's lecture
 * slides on arrays and problem solving: <i>The program reads a student's marks into an array, discards the lowest
 * mark, adds up the remaining marks and displays the average.</i> [Discarding the lowest mark before averaging is
 * a neat way to ensure that one bad piece of work does not affect the final outcome.]
 * 
 * <br /><br />
 * 
 * <i>You can carry out this practical using either BlueJ or at the command prompt with TextPad/Notepad. It is up
 * to you!</i>
 * 
 * <br /><br />
 * 
 * Open <b>My Computer</b>, and then open the <code><b>CSCU9A2 Groups on Wide</b></code> folder, and then the
 * <code><b>Java</b></code> folder.
 * Take a copy of the file <code><b>Marks.java</b></code> into your own file space in a suitable <i>new folder</i>.
 * 
 * <br /><br />
 * 
 * If you cannot access the resource, you can copy and paste the code from here:
 * 
 * <pre>
 * import java.util.Scanner;
 * 
 * {@literal /**}
 *  * This program reads a student's marks into an array, discards the lowest mark,
 *  * adds up the remaining marks and displays the average.
 *  *
 *  * It is an exercise in a top-down structured development of the methods in a program.
 *  * 
 *  * {@literal @author} SBJ Spring 2014
 *  *&#47;
 * public class Marks
 * {
 *     {@literal /**}
 *      * The launch method
 *      *
 *      * {@literal @param}  args   Command line parameters - not used
 *      *&#47;
 *     public static void main(String[] args)
 *     { // Step 1 - Break down the problem into subtasks
 *         int numberOfMarks = readInteger("How many marks?");
 *         int[] marks = new int[numberOfMarks];
 *         readMarks(marks);
 *         discardLowestMark(marks);
 *         float average = averageTheMarksExceptTheLastOne(marks);
 *         System.out.println("The average mark is " + average);
 *     }
 *              
 *     {@literal /**}
 *      * Repeatedly read integers from the user, to fill all elements of the given marks array
 *      *
 *      * {@literal @param} marks    An array of marks to be filled up
 *      *&#47;
 *     private static void readMarks(int[] marks)
 *     { // Step 1 - outline of subtask as a method
 *     }
 *     
 *     // More methods need adding...
 *     
 *     {@literal /**}
 *      * A standard method:
 *      *
 *      * Display the prompt and then read lines repeatedly until a valid integer is found, and
 *      * return it.
 *      *
 *      * {@literal @param} prompt   The prompt to be displayed
 *      * {@literal @result}         A valid integer
 *      *&#47;
 *     private static int readInteger(String prompt)
 *     {
 *         Scanner scan = new Scanner(System.in);
 *         System.out.println(prompt);
 *         while (!scan.hasNextInt()) // while non-integers are present...
 *         {
 *             scan.next();           //...read and discard input, then prompt again
 *             System.out.println("Bad input. Please enter an integer");
 *         }
 *         int input = scan.nextInt();
 *         return input;
 *     }
 * }
 * </pre>
 * 
 * <strong>
 * <i>Remember that it is good practice to keep each programming project in its own, separate folder</i>
 * </strong>.
 * 
 * <br /><br />
 * 
 * <code><b>Marks.java</b></code> will <i>not</i> compile as it stands, as it has methods missing.
 * 
 * <br /><br />
 * 
 * Open <code><b>Marks.java</b></code> and look at what is there:
 * 
 * <ul>
 *     <li>
 *     There is the <b>{@code main}</b> method that asks the user how many marks are to  be processed, then declares
 *     an array of the correct size to hold the marks. The method body then contains the main processing in the form
 *     of calls of three further methods who names indicate their function: read the marks, discard the lowest, and
 *     calculate the average - and it should be clear that they identify three processing steps that together solve
 *     the overall problem. <i>This method is complete and correct</i>.
 *     </li>
 *     <li>
 *     At the end of the program is a standard <b>{@code readInteger}</b> method, with the addition of a parameter that
 *     is used as an initial prompt to the user. <i>This is complete and correct</i>.
 *     </li>
 *     <li>
 *     In between is the header for one method called from <b>{@code main}</b>, <b>{@code readMarks}</b>, but with an
 *     empty method body. This method placeholder is here because it is a subtask introduced in the refinement of the
 *     <b>{@code main}</b> method body. At a later stage, its body will be completed.
 *     </li>
 * </ul>
 * 
 * <br /><br />
 * 
 * Now for some work:
 * 
 * <ul>
 *     <li>
 *     Two other methods are called from <b>{@code main}</b>: <b><i>{@code discardLowestMark}</i></b> and
 *     <b><i>{@code averageTheMarksExceptTheLastOne}</i></b>. Add two further placeholders for these two methods -
 *     the method headers should match how the methods are called in <b>{@code main}</b>. The descriptions of those
 *     two methods as devised by the designer of <b>{@code main}</b> are:
 *     
 *     <ul>
 *         <li>
 *         <i><b>{@code discardLowestMark}</b>: Assume that the given array is full of marks (integers). The lowest
 *         mark is discarded by moving all the following marks down one element in the array. This leaves valid marks 
 *         in all except the last element of the array.</i>
 *         </li>
 *         <li>
 *         <i><b>{@code averageTheMarksExceptTheLastOne}</b>: Add all the marks in the given array together, excluding
 *         the very last array element (as that element contains junk left over from discarding the lowest mark).
 *         Return the average as the result.</i>
 *         </li>
 *     </ul>
 *     
 *     The program might compile now, but doesn't do much yet!
 *     </li>
 *     <li>There are three outstanding tasks: three methods with empty bodies.</li>
 *     <li>
 *     Focus on <b>{@code readMarks}</b>: you can write the Java for <b>{@code readMarks}</b> body quite easily
 *     (it's <b>{@code readArray}</b> from previously). Compile, and <b>{@code readMarks}</b> should run correctly.
 *     [But how can you test that it runs correctly? One possibility is to temporarily add into <b>{@code main}</b>,
 *     after the call of <b>{@code readMarks}</b>, a short {@code for} loop that outputs each array element to
 *     <b>{@code System.out.println}</b>];
 *     </li>
 *     <li>
 *     Now focus on method <b>{@code discardLowestMark}</b>: that looks a bit tricky, so we'll refine it into two
 *     further subtasks (without coding their full details just now). The two steps are: work out where the lowest
 *     mark is, then remove it. Insert this into the body of <b>{@code discardLowestMark}</b>, which introduces two
 *     further methods:
 *     
 *     <pre>
 *     int position = findLowestMark(marks);
 *     removeMark(position, marks);
 *     </pre>
 *     </li>
 *     
 *     <li>
 *     Then add two placeholders for the two new methods. You'll come back to them later. Their descriptions are:
 *     
 *     <ul>
 *         <li>
 *         <i><b>{@code findLowestMark}</b>: Search the given array of marks, and return the index of the lowest
 *         mark (or an equal lowest mark).</i>
 *         </li>
 *         <li>
 *         <i><b>{@code removeMark}</b>: Remove the value at index position in array of marks by moving all further
 *         values down one element.</i>
 *         </li>
 *     </ul>
 *     
 *     </li>
 *     <li>
 *     Now focus on method <b>{@code averageTheMarksExceptTheLastOne}</b>: you can write the Java for the method body
 *     quite easily - a loop to add, and then calculate and return the average (the principle was in a previous
 *     practical). Compile and run: you should be able to enter marks and see some kind of average (but, of course, it
 *     is not the proper required answer, as the method to discard the lowest mark currently does nothing!).
 *     
 *     <br /><br />
 *     
 *     <strong>Remember: your code must be well formatted and clearly commented.</strong>
 *     </li>
 *     <li>
 *     Finally, focus separately on the two remaining methods: <b>{@code findLowestMark}</b>, and 
 *     <b>{@code removeMark}</b>, and program their method bodies. You may need to view CSCU9A1 material for ideas
 *     here.
 *     </li>
 *     <li>Compile, run and <b><i>test carefully</i></b>.</li>
 * </ul>
 * 
 * <strong>Remember: your code must well formatted and clearly commented.</strong>
 * 
 * @author  Michael Sammels
 * @version 28.01.2019
 * @since   1.0
 */
package week3.practical2a;