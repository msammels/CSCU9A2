/**
 * <p>Practicals 2A and 2B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 2A</b><br />
 * In this practical you will practice structured problem solving as an approach to developing Java programs.
 * 
 * <br /><br />
 * 
 * <b>Practical 2B</b><br />
 * In this practical you will practice using a very simple Brookshear machine simulator. In the second part you can
 * try your skill at writing a Brookshear program for yourself.
 * 
 * @author  Michael Sammels
 * @version 28.01.2019 - 31.01.2019
 * @since   1.0
 */
package week3;