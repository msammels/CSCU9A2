package week12.practical10a;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * CSCU9A2 - Week 12 <br />
 * Practical 10A <br />
 * <code>StudentRecord.java</code>
 *
 * <p>A class to hold and manage a student record's data.</p>
 *
 * Note that this class does not have any direct appearance on the screen, and "understands" no events - that
 * is the responsibility of the associated main program GUI class.
 * 
 * <ul>
 *     <li>the GUI parts are in this class</li>
 *     <li>the parts managing the student's record are in class @{link StudentRecord}</li>
 * </ul>
 *
 * One record is displayed, for Pythagoras, and the address and credits obtained details can be updated.
 *
 * @author  SBJ April 2014
 * @author  MIS Spring 2019
 * @version 01.04.2019 - 05.04.2019
 * @since   1.0
 */

public class Database extends JFrame implements ActionListener {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = 7674537388413514334L;
    
    /**
     * Constructor.
     */
    public Database() {}
    
    /**
     * Click to set the student's address from the text field.
     */
    private JButton changeAddress;
    
    /**
     * Click when another module has been passed.
     */
    private JButton modulePassed;
    
    /**
     * Adding two credits.
     */
    private JButton twoCredits;
    
    /**
     * Adding three credits.
     */
    private JButton threeCredits;
    
    /**
     * For entering a new address.
     */
    private JTextField  addressEntry;
    
    /**
     * For displaying the student's details.
     */
    private JTextArea   display;

    /**
     * To hold the instance of class {@link StudentRecord}.
     */
    private StudentRecord record;
    
    /**
     * To hold the instance of class {@link StudentRecord}.
     */
    private StudentRecord record1;
    
    /**
     * To hold the instance of class {@link StudentRecord}.
     */
    private StudentRecord record2;

    /**
     * The main method that is called when the program is run.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        Database frame = new Database();
        frame.setSize(600, 500);
        frame.createGUI();
        frame.setUpData();
        frame.setVisible(true);
    }

    /**
     * Sets up the graphical user interface.
     */
    private void createGUI() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new FlowLayout());

        changeAddress = new JButton("Change address to:");              // Change address button
        window.add(changeAddress);
        changeAddress.addActionListener(this);

        addressEntry = new JTextField("Enter new address here", 20);    // New address field
        window.add(addressEntry);

        modulePassed = new JButton("Passed a module");                  // New credit button
        twoCredits = new JButton("Add two credits");
        threeCredits = new JButton("Add three credits");
        window.add(modulePassed);
        window.add(twoCredits);
        window.add(threeCredits);
        modulePassed.addActionListener(this);
        twoCredits.addActionListener(this);
        threeCredits.addActionListener(this);

        display = new JTextArea(20, 30);                                // To display the student's details
        window.add(display);
        display.setEditable(false);
        display.setFont(new Font("Sans Serif", Font.BOLD, 14));
    }

    /**
     * Sets up the initial student's details.
     */
    private void setUpData() {
        // Set up the student record
        record = new StudentRecord("Pythagoras", "00214159", "Basket Weaving");
        record1 = new StudentRecord("Aristotle", "00214160", "Philosophy");
        record2 = new StudentRecord("Plato", "00214161", "Physics");


        displayDetails();   // Update the display with current details
    }

    /**
     * Sets up the display area.
     */
    private void displayDetails() {
        display.setText("Student record for: " + record.getName() + " \n");
        display.append("Registration number: " + record.getRegistrationNo() + "\n");
        display.append("Degree programme: " + record.getDegree() + "\n");
        display.append("Address: " + record.getAddress() + "\n");
        display.append("Credits: " + record.getCreditsObtained() + "\n");
        display.append("\n");

        display.append("Student record for: " + record1.getName() + " \n");
        display.append("Registration number: " + record1.getRegistrationNo() + "\n");
        display.append("Degree programme: " + record1.getDegree() + "\n");
        display.append("Address: " + record1.getAddress() + "\n");
        display.append("Credits: " + record1.getCreditsObtained() + "\n");
        display.append("\n");

        display.append("Student record for: " + record2.getName() + " \n");
        display.append("Registration number: " + record2.getRegistrationNo() + "\n");
        display.append("Degree programme: " + record2.getDegree() + "\n");
        display.append("Address: " + record2.getAddress() + "\n");
        display.append("Credits: " + record2.getCreditsObtained() + "\n");
        display.append("\n");


    }

    /**
     * Respond appropriately to a button click.
     * @param e the event.
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == changeAddress) {   // Change address: obtain address and set it into record
            record.setAddress(addressEntry.getText());
            addressEntry.setText("");
        }

        if (e.getSource() == modulePassed) {    // Module passed: increment the number of credits
            record.addACredit();
        }

        if (e.getSource() == twoCredits) {      // Add two credits
            record1.addMultipleCredits(2);
        }

        if (e.getSource() == threeCredits) {    // Add three credits
            record2.addMultipleCredits(3);
        }

        displayDetails();
    }
}
