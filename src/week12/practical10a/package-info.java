/**
 * <p><b>Object Orientated Programming 1</b></p>
 * <p><b>Using the BlueJ Object Workbench to experiment with Classes and Objects</b></p>
 * 
 * Although classes such as the <b>{@link StudentRecord#StudentRecord StudentRecord}</b> class from lectures cannot be
 * "run" in the usual way, BlueJ does allow us to exercise their capabilities in a neat way - to help you understand them
 * and to test them.
 * 
 * <ul>
 *     <li>
 *     Create a new project folder called <b>{@code StudentRecords}</b> in your <b>{@code CSCU9A2}</b> folder on
 *     <b>{@code H:}</b>
 *     </li>
 *     <li>
 *     Copy <b>{@code StudentRecord.java}</b> from <b>{@code V:\CSCU9A2\Java}</b> to the new project folder.
 *     </li>
 *     <li>
 *     Launch BlueJ and create a new BlueJ project in your <b>{@code StudentRecords}</b> folder.
 *     </li>
 *     <li>
 *     Open the <b>{@link StudentRecord#StudentRecord StudentRecord}</b> class in the BlueJ editor and read it carefully.
 *     This is the <b>{@code StudentRecord}</b> class studied in lectures - each instance of the class is designed to hold
 *     the information about one student. Actually, there is <i>one extra public method</i>, right at the end called
 *     <b>{@code toString}</b>. This is a standard extra method that can be added to any class: when called it simply
 *     returns, and here it is the name, registration number, and number of credits. This can be useful in testing: if
 *     the variable <b>{@code student}</b> contains an instance of <b>{@link StudentRecord#StudentRecord StudentRecord}</b>,
 *     then {@code System.out.println(student);} will print a summary of the student's details on the terminal,
 *     automatically calling <b>{@code student.toString()}</b>. (Without declaration of <b>{@code toString}</b> that
 *     statement would produce something strange: a representation of the memory address <i>reference</i> in the variable
 *     <b>{@code student}</b>! Try it!)
 *     </li>
 *     <li>
 *     For the moment there is no "main program" that can be executed in the usual way, but BlueJ will allow you to "play"
 *     with the <b>{@link StudentRecord#StudentRecord StudentRecord}</b> on its own - in effect, going through the actions
 *     that a main program might take in using that class.
 *     </li>
 *     <li>
 *     Make sure that the <b>{@link StudentRecord#StudentRecord StudentRecord}</b> class has been compiled.
 *     </li>
 *     <li>
 *     Right-click the <b>{@link StudentRecord#StudentRecord StudentRecord}</b> class icon in the BlueJ window. The pop up
 *     menu shows you various actions that you could choose in red, and also all of the <i>methods</i> applicable to that
 *     item. The only method is <b>{@code new StudentRecord(..., ...)}</b> - this is the constructor for
 *     <b>{@link StudentRecord#StudentRecord StudentRecord}</b>, indicating that we can ask BlueJ to build an instance of
 *     <b>{@code StudentRecord}</b> for us. Select <b>{@code new StudentRecord(..., ...)}</b> and BlueJ will start to create
 *     a new instance of the class.
 *     </li>
 *     <li>
 *     A dialogue box appears, showing the constructor header (and the Javadoc comment if there is one), and suggesting a
 *     name to give to the new instance object (probably <b>{@code studentR1}</b>). The constructor expects parameters, so
 *     there is also a section for entering actual parameter values. Either change or accept the suggested name, enter two
 *     strings (including the quotes, e.g. "Pythagoras" and "00214159") in the two parameter value boxes and click OK.
 *     </li>
 *     <li>
 *     A red icon representing the new object appears in the "object workbench" at the bottom of the BlueJ window, labelled
 *     with the chosen name, and the name of the class that it is an instance of.
 *     </li>
 *     <li>
 *     When you right-click on the object, a pop up menu appears offering various options, including the <i>public</i>
 *     methods of the object, plus other options: <i>Inspect</i>, <i>Remove</i> and <i>Inherited from...</i> (ignore the
 *     last one). <i>Remove</i> discards the object - try it and then re-instantiate the class. <i>Inspect</i> pops up a
 *     dialogue box with information about the object - in this case the values held in the four instance variables - look
 *     at what it displays, make sure that you understand <i>why</i> each variable has the value shown (ask if you do not
 *     understand), and then click OK.
 *     </li>
 *     <li>
 *     Instantiate two more <b>{@code StudentRecord}</b> objects – with different names and registration numbers.
 *     <i>Inspect</i> each to make sure that they have been built correctly.
 *     </li>
 *     <li>
 *     To exercise the individual methods in a <b>{@code StudentRecord}</b> object, select the method from the object’s
 *     right-click pop up menu:
 *     </li>
 *     <li>
 *     For one of the <b>{@code StudentRecord}</b> objects, select <b>{@code getName:}</b> This method expects no
 *     parameters, so BlueJ immediately calls it for that object and pops up a box showing the result. Try various
 *     <b>{@code get}</b> methods from the various objects.
 *     </li>
 *     <li>
 *     For one of the <b>{@code StudentRecord}</b> objects, select <b>{@code setAddress:}</b> This method expects 
 *     parameter (the student's new address), so a dialogue box appears allowing you to enter a string that will be passed
 *     to the method as its actual parameter. Enter a new address and click OK. The dialogue box will simply disappear –
 *     the method has been called, has updated the <b>{@code address}</b> instance variable in the object, but returns no
 *     result.
 *     </li>
 *     <li>
 *     To check that the address has been updated correctly: either <i>Inspect</i> the object or call its
 *     <b>{@code getAddress}</b> method – try both.
 *     </li>
 *     <li>
 *     <i>Inspect</i> the <i>other</i> objects to make sure that <i>their</i> addresses have NOT changed. This illustrates
 *     and emphasises that each object has its <i>own</i> copy of instance variables, and a method called for one object
 *     accesses <i>only the variables for that object</i>.
 *     </li>
 *     <li>
 *     In a similar way, exercise <b>{@code addACredit}</b>.
 *     </li>
 *     <li>
 *     You should be able to keep several object inspector dialogues open, moved aside from the main BlueJ window, and
 *     watch their details as you call the "mutator" methods <b>{@code setAddress}</b> and <b>{@code addACredit}</b>.
 *     Try this.
 *     </li>
 *     <li>
 *     Try calling <b>{@code toString}</b> from each object.
 *     </li>
 * </ul>
 * 
 * <p><b>Extending the {@code StudentRecord} class</b></p>
 * 
 * Here are some useful additions to the <b>{@code StudentRecord}</b> class for you to implement:
 * 
 * <ul>
 *     <li>
 *     Add an instance variable to hold the name of the student's degree programme (for example "Computing Science" or
 *     "Basket Weaving").
 *     </li>
 *     <li>
 *     Either extend the constructor with one more parameter to receive an initial degree name, or provide a
 *     "<b> {@code set}</b>" method to update the degree name (and make it blank initially) – you choose.
 *     </li>
 *     <li>
 *     Add a new public method, <b>{@code addMultipleCredits}</b>: this should receive an integer parameter, which should be
 *     added to the credits obtained.
 *     </li>
 *     <li>
 *     Test your new features carefully using the object workbench. Remember that your code should be neatly formatted and
 *     commented.
 *     </li>
 * </ul>
 * 
 * <p><b>A {@link StudentRecord#StudentRecord StudentRecords} main program</b></p>
 * 
 * <ul>
 *     <li>
 *     Now copy <b>{@code Database.java}</b> from <b>{@code V:\CSCU9A2\Java}</b> to your <b>{@code StudentRecords}</b>
 *     project folder.
 *     </li>
 *     <li>
 *     Close and then re-open the BlueJ project. The new class should appear in the BlueJ window.
 *     </li>
 *     <li>
 *     Open the <b>{@link Database#Database Database}</b> class. This is a standard main program with a GUI (the one seen
 *     in lectures): It instantiates and manages one student record, displays the student's details in a text area, and
 *     allows the address to be updated and credits to be added (one at a time).
 *     </li>
 *     <li>
 *     Try out the <b>{@link Database#Database Database}</b> program in the usual way. <b>NOTE</b>: If you extended the
 *     <b>{@link StudentRecord#StudentRecord StudentRecord}</b> <i>constructor</i> above, then you will need to edit line
 *     71 in <b>{@link Database#Database Database}</b> where the constructor is used (add a degree programme).
 *     </li>
 *     <li>
 *     Here are some extensions to make to the <b>{@link Database#Database Database}</b> class (no further changes are
 *     required in the <b>{@link StudentRecord#StudentRecord StudentRecord}</b> class):
 *     <ul>
 *         <li>
 *         In <b>{@code displayDetails}</b>, now include the degree programme name in the display.
 *         </li>
 *         <li>
 *         Declare two more variables to hold <b>{@code StudentRecord}</b> objects. In <b>{@code setUpData}</b> instantiate
 *         two new <b>{@code StudentRecord}</b> objects for those variables (you invent the details).
 *         </li>
 *         <li>
 *         In <b>{@code displayDetails}</b>, now include the two new students' details below the first student's.
 *         </li>
 *         <li>
 *         Add two buttons: when one is clicked then <i>two</i> credits should be added to the <i>second</i> student's
 *         record, when the other is clicked then <i>three</i> credits should be added to the <i>third</i> student's record
 *         (use <b>{@code addMultipleCredits}</b>). Of course, the display should update automatically.
 *         </li>
 *     </ul>
 * </ul>
 * 
 * Remember that your code should be neatly formatted and commented.
 */
package week12.practical10a;