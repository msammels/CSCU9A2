/**
 * <p>Practicals 10A and 10B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 10A</b><br />
 * This practical introduces you to experimenting with classes and objects on BlueJ's "object workbench", and to the
 * simple application of classes and objects.
 * 
 * <br /><br />
 * 
 * <b>Practical 10B</b><br />
 * This practical develops an object orientated version of the Cash Register application from
 * {@link week5.practical4a Practical 4A}.
 * 
 * @author  Michael Sammels
 * @version 01.04 2019 - 04.04.2019
 * @since   1.0
 */
package week12;