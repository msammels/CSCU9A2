/**
 * <p><b>Object Orientated Programmer 2</b></p>
 * <p><b>
 * Developing an object orientated version of the Cash Register application from
 * {@link week5.practical4a Practical 4A}.
 * </b></p>
 * 
 * <p><b>The Cash Register as a separate class from the GUI</b></p>
 * 
 * <ul>
 *     <li>
 *     Copy the folder <b>{@code CashRegisterOO}</b> from <b>{@code V:\CSCU9A2\Java}</b> to your <b>{@code CSCU9A2}</b>
 *     folder on <b>{@code H:}</b>.
 *     </li>
 *     <li>
 *     Launch BlueJ and create a new BlueJ project in your <b>{@code CashRegisterOO}</b> folder.
 *     </li>
 *     <li>
 *     Open the <b>{@link CashRegister#CashRegister CashRegister}</b> class in the BlueJ editor and read it carefully.
 *     This is closely based on the <b>{@code CashRegister}</b> example studied in lectures – each instance of the class is
 *     designed to hold the information about one supermarket cash register: the current total price so far for the current
 *     customer, and the number of items purchased. The two private instance (global) variables {@code totalPrice} and
 *     {@code itemCount} hold the key data, and there are three public methods to be called by a main program to manage the
 *     data.
 *     </li>
 *     <li>
 *     Compile the <b>{@link CashRegister#CashRegister CashRegister}</b> class. Note: the <b>{@code CashRegisterGUI}</b>
 *     will <b>not</b> compile correctly yet, but that will not prevent you exercising <b>{@code CashRegister}</b>.
 *     </li>
 *     <li>
 *     Take a few minutes to exercise the <b>{@link CashRegister#CashRegister CashRegister}</b> class using <i>BlueJ's
 *     object workbench</i> (see {@link week11.practical9b practical worksheet 9B}): Instantiate a couple of cash register
 *     objects, add some items, check the totals, and look at the objects using the object inspector.
 *     </li>
 *     <li>
 *     Open <b>{@link CashRegisterGUI#CashRegisterGUI CashRegisterGUI}</b> in the BlueJ editor. This is the main program,
 *     read it carefully: It creates a user interface for adding items to the cash register and displaying the total price
 *     and item count so far. It is very closely based on the <b>{@code CashRegister}</b> application from
 *     {@link week5.practical4a Practical 4A}. However, the parts of the previous application responsible for the cash
 *     register data (some variables and methods) have been moved to the new
 *     <b>{@link CashRegister#CashRegister CashRegister}</b> class, and the main program instead declares a
 *     <b>{@code CashRegister}</b> variable on line 40, then places a new instance of <b>{@code CashRegister}</b> in
 *     that variable on line 102 (in {@code setUpData}, called from {@code main}).
 *     </li>
 *     <li>
 *     While you are reading <b>{@link CashRegisterGUI#CashRegisterGUI CashRegisterGUI}</b>, notice that, in
 *     {@link CashRegisterGUI#createGUI createGUI}, a {@link java.awt.GridLayout GridLayout} is configured for the window
 *     instead of {@link java.awt.FlowLayout FlowLayout}. This will arrange the GUI widgets in a two column, five row
 *     layout: in the order in which they are added to the window, they fill the top row from left to right, then the
 *     second row, etc. In two places, dummy (empty) <b>{@link javax.swing.JLabel JLabels}</b> have been added so that
 *     some places in the grid are not used. Widgets are automatically resized to fill their place in the grid.
 *     {@link java.awt.GridLayout GridLayout} gives some more control over some aspects of window layout, but it is quite
 *     simple and the effect is sometimes a bit ugly due to the resizing. (<i>Sometime: Try a Google search for
 *     "Java layouts" to find information about other layout managers.</i>)
 *     </li>
 *     <li>
 *     Now try compiling <b>{@link CashRegisterGUI#CashRegisterGUI CashRegisterGUI}</b>. There is an error on line 115:
 *     BlueJ reports "<i>cannot find symbol – method addItem(double)</i>". Although there is a method called
 *     {@link CashRegister#addItem addItem}, the compiler is correct as it is looking for it in the
 *     <b>{@link CashRegisterGUI#CashRegisterGUI CashRegisterGUI}</b> class, whereas it is actually
 *     in the <b>{@link CashRegister#CashRegister CashRegister}</b> class. You should correct the problem by
 *     inserting {@code cashRegister}. in front of the method call in line 115 – {@code cashRegister} is the name of a
 *     variable containing a <b>{@code CashRegister}</b> object, and the compiler ({@literal &} JVM) will now <i>look in
 *     that object</i> for the {@code addItem} method to call.
 *     </li>
 *     <li>
 *     Compile <b>{@link CashRegisterGUI#CashRegisterGUI CashRegisterGUI}</b> again. There are a few more similar errors
 *     to correct. One of the errors, however, is more serious as there is a required method <i>missing</i> from the 
 *     <b>{@code CashRegister}</b> class, and adding {@code cashRegister.} in front of the method call does not
 *     completely solve the problem. You should add a suitable new method to the
 *     <b>{@link CashRegister#CashRegister CashRegister}</b> class (it is a very simple method).
 *     </li>
 *     <li>
 *     Now the program should compile and run correctly. You can add a few items, view the results, and see the effect of
 *     the {@link java.awt.GridLayout GridLayout}.
 *     </li>
 *     <li>
 *     Now add one more button ("New customer") into the <i>right column in the fifth row</i> of the window layout: This is
 *     to be clicked when starting to process a new customer's purchases, and should set the total price and item count in
 *     the cash register (and in the display) to 0. You will need to declare the button, create it, add it into the correct
 *     place in the GUI, and handle its click in {@link CashRegisterGUI#actionPerformed actionPerformed} (so, some
 *     {@code if} statements are needed). You will also need to add one or two methods to
 *     <b>{@link CashRegister#CashRegister CashRegister}</b> for 0ing the totals, to be called from {@code actionPerformed}.
 *     </li>
 * </ul>
 * 
 * <b>Remember that your code should be neatly formatted and commented</b>.
 * 
 * <p><b>Adding Another Cash Register</b></p>
 * 
 * Now you will add a second cash register to the main application. Using the Power of Object Orientation, you will
 * <i>not</i> need to duplicate the cash register key data variables and methods – <b>you will only need to make changes
 * to the GUI</b>. [Perhaps the application should now be called SupermarketGUI rather than CashRegisterGUI, but you do not
 * need to change it!]
 * 
 * <ul>
 *     <li>
 *     Declare one more <b>{@code CashRegister}</b> instance variable to hold the details of a second cash register.
 *     </li>
 *     <li>
 *     In {@link CashRegisterGUI#setUpData setUpData}, assign the new variable a new instance of CashRegister.
 *     </li>
 *     <li>
 *     Display its total price and item count in the right column, alongside the first cash register's totals.
 *     </li>
 *     <li>
 *     Add a new button "Add item to register 2" in the right column alongside the "Add item button". When this button is
 *     clicked, the item being entered should be added to the second cash register (and the display should be updated).
 *     </li>
 *     <li>
 *     Finally, arrange for two "New customer" buttons, clearing their respective cash registers.
 *     </li>
 * </ul>
 * 
 * <b>Remember that your code should be neatly formatted and commented.</b>
 */
package week12.practical10b;