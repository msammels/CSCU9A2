package week12.practical10b;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * CSCU9A2 - Week 12 <br />
 * Practical 10B <br />
 * <code>CashRegisterGUI.java</code>
 *
 * <p>This program displays a cash register graphical user interface.</p>
 * <p>The actual cash register data is held in a separate class/</p>
 *
 * @author  Michael Sammels
 * @version 25.03.2019
 * @since   1.0
 */

public class CashRegisterGUI extends JFrame implements ActionListener {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = 4542592910317358340L;
    
    /**
     * Constructor.
     */
    public CashRegisterGUI() {}
    
    /**
     * Configuration constants for the frame size and position.
     */
    private static final int FRAME_WIDTH = 400;
    
    /**
     * The frame height should be roughly similar to its width.
     */
    private static final int FRAME_HEIGHT = 300;
    
    /**
     * The frame position (x coordinate).
     */
    private static final int FRAME_X = 200;
    
    /**
     * The frame position (y coordinate).
     */
    private static final int FRAME_Y = 200;

    /**
     * The label which displays the price.
     */
    private JLabel priceLabel;
    
    /**
     * The label which allows input of price.
     */
    private JTextField priceField;
    
    /**
     * Button for adding an item.
     */
    private JButton addItemButton;
    
    /**
     * Button for adding an item.
     */
    private JButton addItemButton2;
    
    /**
     * Button for adding a customer.
     */
    private JButton newCustomerButton;
    
    /**
     * Button for adding a customer.
     */
    private JButton newCustomerButton2;
    
    /**
     * Label for displaying the total.
     */
    private JLabel totalDisplayR1;
    
    /**
     * Label for displaying the total.
     */
    private JLabel totalDisplayR2;
    
    /**
     * Label for the item count.
     */
    private JLabel itemCountDisplayR1;
    
    /**
     * Label for the item count.
     */
    private JLabel itemCountDisplayR2;

    /**
     * Hold the cash register key data.
     */
    private CashRegister cashRegister;
    
    /**
     * Hold the cash register key data.
     */
    private CashRegister cashRegister2;

    /**
     * The main method that is called when the program is run.
     * @param args command line arguments (unused).
     */
    public static void main(String[] args) {
        CashRegisterGUI frame = new CashRegisterGUI();
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setLocation(FRAME_X, FRAME_Y);
        frame.setTitle("My Cash Register");
        frame.createGUI();
        frame.setUpData();
        frame.setVisible(true);
    }

    /**
     * This method builds the graphical user interface.
     */
    public void createGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new GridLayout(5, 2));     // 5 rows and 2 columns
        setResizable(false);

        // First row
        priceLabel = new JLabel("Item price: ");
        window.add(priceLabel);

        final int FIELD_WIDTH = 5;
        priceField = new JTextField(FIELD_WIDTH);
        priceField.setText("");
        window.add(priceField);

        window.add(new JLabel());       // Dummy widget to add space

        // Second row
        addItemButton = new JButton("Add item");
        addItemButton2 = new JButton("Add item [2]");
        addItemButton.addActionListener(this);
        addItemButton2.addActionListener(this);
        window.add(addItemButton);
        window.add(new JLabel());       // Dummy widget to add space
        window.add(addItemButton2);

        // Third row
        totalDisplayR1 = new JLabel("R1 Sales total: 0");
        totalDisplayR2 = new JLabel("R2 Sales total: 0");
        window.add(totalDisplayR1);
        window.add(new JLabel());       // Dummy widget to add space
        window.add(totalDisplayR2);

        // Fourth row
        itemCountDisplayR1 = new JLabel("R1 # items: 0");
        itemCountDisplayR2 = new JLabel("R2 # items: 0");
        window.add(itemCountDisplayR1);
        window.add(new JLabel());       // Dummy widget to add space
        window.add(itemCountDisplayR2);

        // Fifth row
        newCustomerButton = new JButton("New customer");
        newCustomerButton2 = new JButton("New customer [2]");
        newCustomerButton.addActionListener(this);
        newCustomerButton2.addActionListener(this);
        window.add(newCustomerButton);
        window.add(new JLabel());       // Dummy widget to add space
        window.add(newCustomerButton2);
        //window.add(new JLabel());       // Dummy widget to add space
    }

    /**
     * This method sets up a single instance of the {@link CashRegister} class.
     */
    private void setUpData() {
        cashRegister = new CashRegister();
        cashRegister2 = new CashRegister();
    }

    /**
     * React to click on Add item button by adding a new item to the cash register and updating the display. The
     * item's price is taken from the priceField. For the user's convenience, the price field is cleared, and the
     * input focus is returned to the price field ready for the next input.
     * @param e the event.
     */
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == addItemButton) {
            // Get the new item price and record it
            double price = Double.parseDouble(priceField.getText());
            cashRegister.addItem(price);
        }

        if (e.getSource() == addItemButton2) {
            // Get the new item price and record it
            double price = Double.parseDouble(priceField.getText());
            cashRegister2.addItem(price);
        }

        if (e.getSource() == newCustomerButton) {
            cashRegister.clearValues();
        }

        if (e.getSource() == newCustomerButton2) {
            cashRegister2.clearValues();
        }

        // Update the cash register display
        totalDisplayR1.setText("R1 Sales total:  " + cashRegister.getTotal());
        itemCountDisplayR1.setText(" R1 # items:  " + cashRegister.getCount());
        priceField.setText("");             // Clear the old entry
        priceField.requestFocusInWindow();  // And put the input focus back into the price field

        totalDisplayR2.setText("R2 Sales total:  " + cashRegister2.getTotal());
        itemCountDisplayR2.setText("R2 # items:  " + cashRegister2.getCount());
        priceField.setText("");             // Clear the old entry
        priceField.requestFocusInWindow();  // And put the input focus back into the price field

    }
}
