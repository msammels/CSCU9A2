/**
 * <p>Practicals 4A and 4B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 4A</b><br />
 * In this practical you will build a GUI for a simple Cash Register, as you might find in a shop. Then you will
 * improvise some graphical drawing.
 * 
 * <br /><br />
 * 
 * <b>Practical 4B</b><br />
 * In this practical you will get some exercise "provoking" and tracking down the bugs and imperfections in a program
 * that <b>almost</b> works. It is also a program that uses an array in a special, though quite common, way to hold
 * a collection of numbers that does not necessarily fill the array.
 * 
 * @author  Michael Sammels
 * @version 11.02.2019 - 14.02.2019
 * @since   1.0
 */
package week5;