package week5.practical4a;

import java.awt.*;
import java.io.Serializable;

import javax.swing.*;

/**
 * CSCU9A2 - Week 5 <br />
 * Practical 4A <br />
 * <code>Drawing.java</code>
 * 
 * <p>A frame that shows a blank drawing area.</p>
 * 
 * @author  Michael Sammels
 * @version 11.02.2019
 * @since   1.0
 */

public class Drawing extends JFrame implements Serializable {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = -5559854261420866552L;

    /**
     * Constructor.
     */
    public Drawing() {}
    
    /**
     * Configuration constant for the x coordinate.
     */
    private static final int FRAME_X = 200;
    
    /**
     * Configuration constant for the y coordinate.
     */
    private static final int FRAME_Y = 200;
    
    /**
     * Configuration constant for the frame width.
     */
    private static final int FRAME_WIDTH = 500;
    
    /**
     * Configuration constant for the frame height.
     */
    private static final int FRAME_HEIGHT = 500;
    
    /**
     * Configuration constants for the drawing area width.
     */
    private static final int DRAWING_WIDTH = 340;
    
    /**
     * Configuration constants for the drawing area height.
     */
    private static final int DRAWING_HEIGHT = 450;
    
    /**
     * The main method is the main launch action for the drawing program.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        Drawing frame = new Drawing();
        frame.setTitle("A drawing area");
        frame.setLocation(FRAME_X, FRAME_Y);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.createGUI();
        frame.setVisible(true);
    }
    
    /**
     * This method builds the graphical user interface - just one simple drawing panel in this application.
     */
    private void createGUI() {
        // Set up main window characteristics
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container window = getContentPane();
        
        window.setLayout(new FlowLayout());
        
        // Create the panel for drawing on
        JPanel drawingArea = new JPanel() {
            private static final long serialVersionUID = 4356647123294344189L;

            // paintComponent is called automatically when a screen refresh is needed
            public void paintComponent(Graphics g) {
                // g is cleared panel area
                super.paintComponent(g);    // Paint the panel's background
                paintScreen(g);             // Then the required graphics
            }
        };
        drawingArea.setPreferredSize(new Dimension(DRAWING_WIDTH, DRAWING_HEIGHT));
        drawingArea.setBackground(Color.white);
        window.add(drawingArea);
    }
    
    /**
     * Redraw the drawing panel when the screen is refreshed.
     * @param g the panel to be drawn to.
     */
    private void paintScreen(Graphics g) {
        // To draw outer house
        g.drawLine(50, 300, 375, 300);      //  Line 1      Flooring of the house
        g.drawLine(80, 300, 80, 150);       //  Line 2      Left wall of outer house
        g.drawLine(200, 300, 200, 150);     //  Line 3      Right wall of outer house
        g.drawLine(50, 170, 140, 110);      //  Line 4      Left inclination of the roof
        g.drawLine(140, 110, 230, 170);     //  Line 5      Right inclination of the roof
        
        // To draw entrance(door frame)
        g.drawLine(110, 220, 110, 300);     //  Line 6      Entrance left
        g.drawLine(170, 220, 170, 300);     //  Line 7      Entrance right
        g.drawLine(110, 220, 170, 220);     //  Line 8      Entrance top
        
        // To draw the man
        g.drawLine(320, 280, 300, 300);     //  Line 9      Left leg
        g.drawLine(320, 280, 340, 300);     //  Line 10     Right leg
        g.drawLine(320, 280, 320, 250);     //  Line 11     Body
        g.drawLine(300, 270, 320, 250);     //  Line 12     Left hand
        g.drawLine(340, 270, 320, 250);     //  Line 13     Right hand
        g.drawOval(310, 230, 20, 20);       //  Line 14     Head
    }
}
