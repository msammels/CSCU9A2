package week5.practical4a;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

/**
 * CSCU9A2 - Week 5 <br />
 * Practical 4A <br />
 * <code>CashRegister.java</code>
 * 
 * <p>This program displays a cash register graphical user interface.</p>
 * 
 * @author  Michael Sammels
 * @version 11.02.2019
 * @since   1.0
 */

public class CashRegister extends JFrame implements ActionListener, Serializable {
    /**
     * The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a
     * Serializable class.
     */
    private static final long serialVersionUID = -4157074554131350436L;

    /**
     * Constructor.
     */
    public CashRegister() {}
    
    /**
     * Configuration constants for the frame width.
     */
    private static final int FRAME_WIDTH = 400;
    
    /**
     * Configuration constants for the frame height.
     */
    private static final int FRAME_HEIGHT = 300;
    
    /**
     * Configuration constants for the text area (rows).
     */
    private static final int AREA_ROWS = 10;
    
    /**
     * Configuration constants for the text area (columns).
     */
    private static final int AREA_COLUMNS = 30;
    
    /**
     * The label to hold the name of the items and the price.
     */
    private JLabel totalLabel;
        
    /**
     * Used for inputting the price.
     */
    private JTextField priceField;
    
    /**
     * Used for inputting the description.
     */
    private JTextField descField;
    
    /**
     * The text area where the items are displayed.
     */
    private JTextArea itemsArea;
    
    /**
     * The variable which holds the total price of all the items.
     */
    private double totalPrice = 0;
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        CashRegister frame = new CashRegister();
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setTitle("Cash Register");
        frame.setLocationRelativeTo(null);
        frame.createGUI();
        frame.setVisible(true);
    }
    
    // --------------------------------------------------------------------------------------------
    
    /**
     * Adds an item to this cash register.
     * @param   price the price of the item.
     */
    private void addItem(double price) {
        itemCount++;
        totalPrice = totalPrice + price;
    }

    /**
     * Adds multiple items to this cash register.
     * @param price     the price of a single item.
     * @param number    the number of items to be added.
     */
    public void addMultipleItems(double price, int number) {
        itemCount = itemCount + number;
        totalPrice = totalPrice + price * number;
    }

    /**
     * Clears the item count and the total.
     */
    public void clear() {
        itemCount = 0;
        totalPrice = 0;
    }

    /**
     * This method builds the graphical user interface.
     */
    private void createGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new FlowLayout());
        
        /**
         * The label used to display the item description.
         */
        JLabel descriptionLabel = new JLabel("Item description: ");
        
        /**
         * The label used to display the item price.
         */
        JLabel priceLabel = new JLabel("Item price: £");
        
        final int FIELD_WIDTH = 5;
        descField = new JTextField(FIELD_WIDTH);
        descField.setText("");
        descField.addActionListener(this);
        window.add(descriptionLabel); window.add(descField);
        
        priceField = new JTextField(FIELD_WIDTH);
        priceField.setText("");
        priceField.addActionListener(this);
        window.add(priceLabel); window.add(priceField);
        
        totalLabel = new JLabel("Sales total: £");
        window.add(totalLabel);
        
        JButton addItemButton = new JButton("Add item");
        addItemButton.addActionListener(this);
        window.add(addItemButton);
        
        itemsArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
        itemsArea.setText("");
        itemsArea.setEditable(false);
        
        JScrollPane scrollPane = new JScrollPane(itemsArea);
        window.add(scrollPane);        
    }
    
    /**
     * Gets the number of items in the current sale.
     * @return  The item count.
     */
    public int getCount() {
        return itemCount;
    }

    /**
     * Gets the price of all items in the current sale.
     * @return  The total price.
     */
    private double getTotal() {
        return totalPrice;
    }

    /**
     * React to click on Add item button by adding a new item to the cash register and updating the display.
     * The item's price is taken from the priceField. For the user's convenience, the price field is cleared,
     * and the input focus is returned to the price field ready for the next input.
     */
    public void actionPerformed(ActionEvent event) {
        double price = Double.parseDouble(priceField.getText());
        
        itemsArea.append(descField.getText() + "\t");
        addItem(price);
        itemsArea.append("£" + price + "\n");
        totalLabel.setText("Sales total: £" + getTotal());
        priceField.setText("");             // Clear the old entry
        priceField.requestFocusInWindow();  // And put the input focus back into the price field
    }
    
    /**
     * A simulated cash register that tracks the item count.
     */
    private int itemCount = 0;
}
