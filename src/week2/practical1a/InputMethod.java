package week2.practical1a;

import java.util.Scanner;

/**
 * CSCU9A2 - Week 2 <br />
 * Practical 1A <br />
 * <code>InputMethod.java</code>
 * 
 * @author  Michael Sammels
 * @version 21.01.2019
 * @since   1.0
 */

public class InputMethod {
    /**
     * Constructor.
     */
    public InputMethod() {}
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        int i, n, s = 0;    // i = integer (for loop), n = number (readInteger), s = sum of numbers
        double average;
        
        // Here we are going to calculate the sum of all the numbers entered, up to 10 numbers
        for (i = 0; i < 10; i++) {
            n = readInteger();
            
            s += n; // A fancier way of saying "s = s + n"
        }
        
        /*
         * Once we have the sum of the numbers, let's find the average. We will need to cast to a double for precision
         * We divide s (the sum of the numbers) by the amount of digits entered (in this case, 10)
         */
        average = (double) s / 10;
        
        // Displaying the final results (average) to the terminal
        System.out.println("The average of the numbers is: " + average);
    }
    
    /**
     * Read integers from the user and return the result.
     * 
     * <br /><br />
     * 
     * Do not display the valid input. <br />
     * Do not count the invalid inputs.
     * @return The result.
     */
    private static int readInteger() {
        try (Scanner scan = new Scanner(System.in)) {
            // Ask the user to enter an integer
            System.out.print("Enter an integer: ");
            while (!scan.hasNextInt()) {            // While non-integers are present...
                scan.next();                        // ...read and discard input, then prompt again
            }
            
            // Store the input in a variable
            return scan.nextInt();
        }
    }
}
