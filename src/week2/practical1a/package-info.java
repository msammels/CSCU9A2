/**
 * <p><b>InputLoop</b></p>
 * 
 * Open <b>My Computer</b>, and then open the <code><b>CSCU9A2 Groups on Wide</b></code> folder, and then the
 * <code><b>Java</b></code> folder. This is where the Java programs and fragments will be placed <i>for you to
 * <b>copy</b> for practical work - you cannot</i> use them where they are. Open <b>{@code InputLoop.txt}</b> with
 * Notepad (or see the following code).
 * 
 * <pre>
 * // For Practical 1A: Contains many syntax errors...
 * 
 * import java.util.Scanner;
 * 
 * public class InputLoop
 * {
 *     public static void main (String() args);
 *     {
 *         Scanner scan = new Scanner(System.in)
 *         System.out.printLn ("Enter an integer");
 *         while (!scan.hasNextInt()) // While non-integers are present...
 *         {
 *             Scan.next();           //...read and discard input, then prompt again
 *             System out println ("Bad input. Enter an integer"); 
 *         }
 *         int input = scan.nextInt();     
 *         System.out.print1n ("You entered ", input, "!");
 *     }
 * }
 * </pre>
 * 
 * We recommend that you keep your folders and files organised very carefully. Suggestion: create a new folder called
 * <code><b>CSCU9A2</b></code> in your home folder, and keep each individual BlueJ project in a separate folder inside
 * <code><b>CSCU9A2</b></code>.
 * 
 * <br /><br />
 * 
 * Launch BlueJ and create a new BlueJ project called <b>{@link InputLoop#InputLoop InputLoop} <i>in it's own new folder
 * in your <code>CSCU9A2</code> folder</i></b>, create a new class and paste into it the contents of the file
 * <b>{@code InputLoop.txt}</b> that you opened above (<i>select and copy</i> in Notepad, and then <i>paste</i>
 * in BlueJ) - or copy the code above.
 * 
 * <br /><br />
 * 
 * Try compiling the project. There are many syntax errors in the Java code, so  you will need to go around the
 * compile/correct cycle until the errors are gone. <i>Be patient!</i>
 * 
 * <br /><br />
 * 
 * The <b>{@link InputLoop#InputLoop InputLoop}</b> program shows how we can use a loop for input validation, displaying an
 * error message if a user types in the wrong kind of input and inviting them to enter another input. Try it out to make
 * sure that it works properly.
 * 
 * <br /><br />
 * 
 * Add a declaration of a new variable to act as a counter, and appropriate statements to count each
 * <i>invalid (bad)</i> input. When a valid integer is finally entered, display the number of invalid inputs that
 * had been entered, as well as the final valid integer. Try it out to make sure that it works properly.
 * 
 * <br /><br />
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * <br /><br /><hr />
 * 
 * <p><b>InputMethod</b></p>
 * 
 * Save a copy your <b>{@link InputLoop#InputLoop InputLoop}</b> <i>project folder</i> as
 * <b>{@link InputMethod#InputMethod InputMethod}</b> (use BlueJ's <b>Project</b> menu, <b>Save As...</b>). You are going
 * to work on <b>{@link InputMethod#InputMethod InputMethod}</b> for a while.
 * 
 * <br /><br />
 * 
 * Introduce a new method into your program called {@code readInteger} with this method header:
 * 
 * <pre>private static int readInteger()</pre>
 * 
 * It receives no parameters, and should <i>return</i> the next available integer typed by the user, inviting them
 * repeatedly to input an integer until they do so. You can obtain the Java for this method's body by copying from
 * the body of the {@code main} method, with minor edits. Note: the method should <i>not display</i> the valid
 * integer once it has been received and does not need to count invalid inputs - it should simply return the integer.
 * 
 * <br /><br />
 * 
 * Now <i>replace</i> the statements in the body of the {@code main} method with statements using a {@code for} loop
 * to input ten integers (using a single statement in the loop body that calls {@code readInteger}, to add them
 * together as they are input, and to display their average after the input process has finished.
 * 
 * <br /><br />
 * 
 * Try out your <b>{@link InputMethod#InputMethod InputMethod}</b> program to make sure that it works correctly.
 * 
 * <br /><br />
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * <br /><br /><hr />
 * 
 * <p><b>InputArray</b></p>
 * 
 * Save a copy of your <b>{@link InputMethod#InputMethod InputMethod}</b> project as
 * <b>{@link InputArray#InputArray InputArray}</b>. You are now going to work on
 * <b>{@link InputArray#InputArray InputArray}</b>.
 * 
 * <br /><br />
 * 
 * Introduce a new method into your program called {@code readArray} with this method header:
 * 
 * <pre>private static void readArray(int[] a)</pre>
 * 
 * It receives a single parameter, an array of {@code int}, and should then input ten integers from the user,
 * assigning them to consecutive elements of the received array parameter. The method returns no result - the
 * information to be returned has been assigned to the array elements. You can obtain the Java for this method's body
 * by copying from the body of the {@code main} method, with minor edits.
 * 
 * <br /><br />
 * 
 * Now <i>replace</i> the statements in the body of the {@code main} method with the declaration of an array of
 * ten {@code int}, a call of {@code readArray} to fill the declared array with data, and finally an enhanced
 * {@code for} loop to add all the array elements together and display their average.
 * 
 * <br /><br />
 * 
 * Hint: you declare and set up an array in Java like this:
 * 
 * <pre>int[] myList = new int[size];</pre>
 * 
 * Try our your <b>{@link InputArray#InputArray InputArray}</b> program to make sure that it works properly.
 * 
 * <br /><br />
 * 
 * <strong>Finally make sure that your program is formatted neatly, and clearly commented.</strong>
 * 
 * @author  Michael Sammels
 * @version 21.01.2019
 * @since   1.0
 */
package week2.practical1a;