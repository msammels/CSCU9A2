package week2.practical1b;

import java.util.Scanner;

/**
 * CSCU9A2 - Week 2 <br />
 * Practical 1B <br />
 * <code>InputLoop.java</code>
 * 
 * @author  Michael Sammels
 * @version 24.01.2019
 * @since   1.0
 */

public class InputLoop {
    /**
     * Constructor.
     */
    public InputLoop() {}
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            // Ask the user to enter an integer
            System.out.println("Enter an integer: ");
            while (!scan.hasNextInt()) {                    // While non-integers are present...
                scan.next();                                // ...read and discard input, then prompt again
            }
            
            // Store the input in a variable
            int input = scan.nextInt();
            
            // Output the results to the terminal
            System.out.println("You entered " + input + "!");
        }
    }
}
