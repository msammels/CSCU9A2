/**
 * <p><b>Basic use of the Command prompt</b></p>
 * 
 * First, launching a command prompt window, customising its appearance and then some basic navigation and file
 * manipulation at the command prompt:
 * 
 * <br /><br />
 * 
 * <i>Double clicking with your mouse in the usual way</i>: Open <b>My Computer</b>, then <code><b>C:\</b></code>, then 
 * <code><b>Users</b></code>, then the folder labelled with your username. This show your "profile folder" on
 * <i>this</i> PC in an Explorer window, which on University Lab PCs is not very useful as it does not "move around"
 * from PC to PC with you. On your personal PC it <i>is</i> useful - it contains your documents, desktop, your browser
 * favourites, etc. Just look at it for the moment: a collection of folders and files.
 * 
 * <br /><br />
 * 
 * Click <i>once</i> in the blank area towards the right of the current location address bar at the top of the window:
 * it will change from a "breadcrumbs view" to a full "path name" location for the current folder, for example:
 * <code><b>C:\Users\msammels</b></code> (you can recover the breadcrumbs view by typing the <b>Esc</b> key).
 * 
 * <br /><br />
 * 
 * Move the Explorer window to one side of the screen.
 * 
 * <br /><br />
 * 
 * Click the Windows <b>Start</b> icon 
 * <img src="{@docRoot}/resources/week2-prac1b-img01.jpg" alt ="week2-prac1b-img01" height="23" width="23" />, and
 * type <b>cmd</b> into the Search box. Windows should find <b>cmd.exe</b> and show it's name (although the 
 * <b>.exe</b> extension might be hidden.) Press <b>Enter</b> or click on <b>cmd.exe</b>. A command prompt window
 * will appear.
 * 
 * <br /><br />
 * 
 * Click on the icon at the top left of the command prompt window. Choose Properties from the menu that pops up. This
 * dialogue box allows you to customise the command window - try out some of the customisations.
 * 
 * <br /><br />
 * 
 * When you are working at the command prompt, the system always has a specific "working directory" or "current folder"
 * that will be the default focus of any actions taken - analogous to viewing a specific folder in a desktop Explorer
 * window. By default, command windows display the current working directory "path name" followed by the command prompt
 * symbol {@code >} whenever the system is waiting for you type a command. The window that has opened should show
 * something like <code><b>C:\Users\msammels ></b></code>
 * 
 * <br /><br />
 * 
 * Type in the command <b>{@code dir}</b> (in any mixture of upper and lower case, Windows does not care) then
 * <b>Enter</b>. The directory contents will be listed on the screen: the name of each item in the directory, with
 * various additional details. Additional details include the file size for files, and the indicator {@code <DIR>}
 * for subfolders (subdirectories). Look carefully: the listed items <i>should be exactly the same as the ones shown
 * in the Explorer window that opened earlier</i>. There will be two unusual items listed: {@code .} means "this
 * directory", and {@code ...} means "the enclosing" or "parent" directory.
 * 
 * <br /><br />
 * 
 * The <b>{@code dir}</b> command has many alternative options: two ways to find out what is available: type
 * <b>{@code help dir}</b>, or type <b>{@code dir /?}</b> - these work for many commands. <b>{@code help}</b> on its
 * own lists all the built in commands. If you want more help on any command, look to Google.
 * 
 * <br /><br />
 * 
 * Try some options: for example <b>{@code dir /w}</b> gives a directory listing with just folder/file names (folder
 * names are enclosed in <code><b>[]</b></code>). <b>{@code dir /ah}</b> shows normally hidden files.
 * 
 * <br /><br />
 * 
 * Try <b>{@code dir /s}</b> - this could give a dramatic effect: it lists all the files, subfolders, the contents of
 * subfolders, subsubfolders, etc. If it appears to be going wild with large number of folders and files
 * flashing up on the screen, then don't panic - it's just obeying your command. (If it does not go wild, then there is
 * a step later where it certainly will!). However, you can tame the wildness in various ways, try them:
 * 
 * <ul>
 *     <li>
 *     Hold down the <b>Ctrl</b> key and press <b>C</b> - this should interrupt the process immediately and cancel
 *     its operation.
 *     </li>
 *     <li>Or type <b>Ctrl+S</b> to pause the output, then <b>Ctrl+S</b> to resume, etc.</li>
 *     <li>
 *     Or when you know that this may happen, add <b>{@code | more}</b> to the command: <b>{@code dir /s | more}</b>
 *     (the output from <b>{@code dir}</b> is "piped" into <b>{@code more}</b>, which "paginates" the output). The
 *     output will stop after every screenful; you type <b>Space</b> for the next screen, <b>Enter</b> to see one
 *     more line, <b>q</b> to quit (or <b>Ctrl+C</b>). [The <code><b>|</b></code> is often on the same as \ in the
 *     lower left corner of the keyboard. It may look like a vertical bar with a small gap in the middle!]
 *     </li> 
 * </ul>
 * 
 * Another useful trick is "output redirection" - this allows you to specify that the output from a command is sent to
 * a file instead of to the screen. Try <b>{@code dir > files.txt}</b>. This will create a new file in the current
 * folder - it should automatically appear in the Explorer window on your desktop. Type <b>{@code dir}</b> again and
 * you will see the new file listed.
 * 
 * <br /><br />
 * 
 * Here are three ways to view the contents of a file, try them:
 * 
 * <ul>
 *     <li>The command <b>{@code type files.txt}</b> will display the contents of the file in the command window.</li>
 *     <li>The command <b>{@code notepad files.txt}</b> will launch Notepad to view/edit the file.</li>
 *     <li>
 *     The command <b>{@code start files.txt}</b> will launch the appropriate application to view/edit a <b>.txt</b>
 *     file (usually Notepad).
 *     </li>
 * </ul>
 * 
 * Notepad was easy to launch because the executable application is in the file <code><b>notepad.exe</b></code> which
 * is in the folder <code><b>C:\Windows\System32</b></code>, which is mentioned in the system's "path" - a semicolon
 * separated list of folder locations. To see the path list, type <b>{@code path}</b> at the command prompt.
 * 
 * <br /><br />
 * 
 * The University Lab PCs also have a better text editor called TextPad installed (nt a Microsoft product). This is not
 * locatable by using the path list (the system admins have not bothered putting its installation folder in the path).
 * However, any application be launched if we know where it is located: TextPad happens to be installed in
 * <code><b>C:\Program Files (x86)\TextPad5\textpad.exe</b></code>, but it is not necessary to <i>type</i> all that:
 * try the following steps, which include using the <b>Tab</b> key to prompt Windows to attempt "filename completion"
 * (do not press the <b>Enter</b> key until the step that says to do so):
 * 
 * <ul>
 *     <li>
 *     At the command prompt, type <code><b>C:\Pro</b></code> then press the <b>Tab</b> key. The command expands to
 *     "<code><b>C:\Program Files</b></code>" - not the correct folder, but it is the <i>first</i> one matching
 *     "<code><b>C:\Pro</b></code>".
 *     </li>
 *     <li>
 *     Press <b>Tab</b> again. This time the command expands to "<code><b>C:\Program Files (x86)</b></code>" - the
 *     <i>second</i> folder matching "<code><b>C:\Pro</b></code>".
 *     </li>
 *     <li>Then type <b>{@code \Text}</b> then press the <b>Tab</b> key.</li>
 *     <li>Then type <b>{@code \Text}</b> and then press the <b>Tab</b> key (yes, again).</li>
 *     <li>
 *     You should now see the full path name location for TextPad, with quotes "" around it, because it contains
 *     spaces. Type a <b>space</b> and then <code><b>files.txt</b></code> and finally press <b>Enter</b>.
 *     </li>
 *     <li>TextPad should be launched, viewing/editing <code><b>files.txt</b></code>.
 * </ul>
 * 
 * When you have finished with <code><b>files.txt</b></code>, you can delete it by using <b>{@code del files.txt}</b>
 * 
 * <br /><br />
 * 
 * Now for some navigation: view a directory listing with <b>{@code dir}</b>. Choose one of the subdirectories, say
 * <code><b>Documents</b></code> (something else if there is no <code><b>Documents</b></code>). Type
 * <b>{@code cd Documents}</b> to move into the <code><b>Documents</b></code> subdirectory (subfolder).
 * <b>{@code cd}</b> stands for <b>c</b>hange <b>d</b>irectory. You should see that the command prompt line has
 * changed to indicate a different working directory, and the output from <b>{@code dir}</b> will show different
 * files/folders (or none if the folder is empty).
 * 
 * <br /><br />
 * 
 * Type <b>{@code cd ..}</b> to move back to the enclosing directory - verify by inspecting the command prompt and
 * typing <b>{@code dir}</b>
 * 
 * <br /><br />
 * 
 * Move all the way to the top directory on the <code><b>C:</b></code> drive by typing <b>{@code cd \}</b> - verify by
 * inspecting the command prompt. Type <b>{@code dir}</b>. Now <b>{@code dir /s}</b> will certainly give a dramatic
 * output!
 * 
 * <br /><br />
 * 
 * Move directly to <i>any directory</i> on the <code><b>C:</b></code> by typing its location after <b>{@code cd}</b>.
 * Using <b>Tab</b> for filename completion helps with this. Try this one: <b>{@code cd C:\Windows\System32}</b>
 * 
 * <br /><br />
 * 
 * Now, Windows can access file stores on various "drives". By convention, the main hard disk drive is called
 * <code><b>C:</b></code>, but in the University labs you probably also have <code><b>H:</b></code> connected to
 * your personal central file store, and <code><b>V:</b></code> connected to the shared
 * <code><b>Groups on Wide</b></code> file store. You can see which drives you have by typing the command
 * <b>{@code net use}</b>. Windows tracks a separate "working directory" on each drive. You can switch which drive you
 * are currently working on by typing the drive letter after the command prompt, for example
 * <code><b>H: Enter</b></code>. Note, the <b>{@code cd}</b> command does not change the current drive.
 * 
 * <br /><br />
 * 
 * So, now switch to your <code><b>H:</b></code> drive and use <b>{@code cd}</b> to navigate to the directory where you
 * are keeping your BlueJ project folders. Use <b>{@code dir}</b> to verify that you are in the right place.
 * 
 * <br /><br />
 * 
 * There is a quicker way to immediately open a command window at a specific folder if you can see its icon in a
 * desktop Explorer window: hold down the <b>Shift</b> key, right click the mouse on the folder icon and select
 * <b>Open command window here</b> from the pop-up menu. Try it now for one of your home file store folders. Look at
 * the working directory in the command prompt, and list the directory to verify that the command windows has been
 * opened correctly.
 * 
 * <br /><br /><hr />
 * 
 * <p><b>InputLoop - again!</b></p>
 * 
 * Now you will carry out the first part of last week's practical sheet, but doing everything (almost) at the
 * command prompt:
 * 
 * <br /><br />
 * 
 * Open a command window at your folder where you will keep your individual <code><b>CSCU9A2</b></code> project
 * folders, or re-use an existing command window by changing to the current folder. If you did not create such a
 * folder last week, then do so now either in a desktop Explorer window, or using the commands below...
 * 
 * <br /><br />
 * 
 * Create new directory, say <code><b>InputLoop2</b></code>, for a new project by typing the command: 
 * <b>{@code md InputLoop2}</b> - <b>{@code md}</b> stands for <b>m</b>ake <b>d</b>irectory.
 * 
 * <br /><br />
 * 
 * Use <b>{@code cd}</b> to move into the new directory.
 * 
 * <br /><br />
 * 
 * Use the <b>{@code copy}</b> command to fetch a new copy of the file <code><b>InputLoop.txt</b></code> from
 * <code><b>Groups on Wide</b></code> and to rename it as a Java file in one step: 
 * <b>{@code copy V:\CSCU9A2\Java\InputLoop.txt InputLoop.java}</b>, or as before copy the code below. Note: this
 * <b>{@code copy}</b> command does not indicate a <i>destination directory</i> (see <b>{@code help copy}</b>), so
 * the new, renamed copy is placed in the current working directory. Remember that you can <b>Tab</b> for folder/file
 * name completion.
 * 
 * <pre>
 * // For Practical 1A: Contains many syntax errors...
 * 
 * import java.util.Scanner;
 * 
 * public class InputLoop
 * {
 *     public static void main (String() args);
 *     {
 *         Scanner scan = new Scanner(System.in)
 *         System.out.printLn ("Enter an integer");
 *         while (!scan.hasNextInt()) // while non-integers are present...
 *         {
 *             Scan.next();           //...read and discard input, then prompt again
 *             System out println ("Bad input. Enter an integer"); 
 *         }
 *         int input = scan.nextInt();     
 *         System.out.print1n ("You entered ", input, "!");
 *     }
 * }
 * </pre>
 * 
 * Use <b>{@code dir}</b> and <b>{@code type}</b> to make sure that the file has been copied. You should also be able
 * to see the new folder and file in an Explorer window.
 * 
 * <br /><br />
 * 
 * Launch <b>TextPad</b> (as above) giving <code><b>InputLoop.java</b></code> as the file to be opened. Move the
 * window to one side for the moment.
 * 
 * <br /><br />
 * 
 * Compile <code><b>InputLoop.java</b></code> using the command <b>{@code javac InputLoop.java}</b>. You will see
 * many errors reported on the screen - you can scroll backwards and forwards to see them all if you necessary.
 * <b>{@code javac}</b> processes the entire file and generates as much error feedback as it can, but BlueJ cuts
 * this down and shows us only error at a time (that's possibly good, and possibly bad!).
 * 
 * <br /><br />
 * 
 * Using the TextPad window, correct the errors in <code><b>InputLoop.java</b></code>. You can <b>Save</b> from
 * TextPad and recompile it in the command window without needing to close down TextPad. At the command prompt, use
 * the up-arrow key to select the previous <b>{@code javac}</b> command to re-execute it.
 * 
 * <br /><br />
 * 
 * Use the <b>{@code dir}</b> to check that the <code><b>.class</b></code> file has been generated.
 * 
 * <br /><br />
 * 
 * Run your now correct program with the command <b>{@code java InputLoop}</b> - note that you should <i>not</i> add
 * <code><b>.class</b></code> to the filename.
 * 
 * <br /><br />
 * 
 * Try some of the options that <b>{@code javac}</b> and <b>{@code java}</b> offer:
 * 
 * <ul>
 *     <li>
 *     Type <b>{@code javac}</b> and <b>{@code java}</b> on their own at the command prompt: you will see many
 *     options listed.
 *     </li>
 *     <li>
 *     Use <b>{@code md}</b> to make a new subdirectory called <b>bin</b>. "bin" is the traditional name for a
 *     directory containing executable files - it is short for "binary" and is a reference to machine code.
 *     </li>
 *     <li>
 *     Direct the compiler to put the bytecode for <code><b>InputLoop</b></code> into the <code><b>bin</b></code>
 *     directory like this: <b>{@code javac -d bin InputLoop.java}</b>
 *     </li>
 *     <li>Check the bytecode is there using <b>{@code dir bin}</b>.</li>
 *     <li>
 *     Direct the JVM to look for bytecode to execute in the <b>bin</b> directory like this:
 *     <b>{@code java -classpath bin InputLoop}</b>
 *     </li>
 *     
 *     <li>Download an image from somewhere, and try the <b>{@code -splash}</b> option in <b>{@code java}</b>.
 * </ul>
 * 
 * <strong>Remember: your code must be well formatted and clearly commented.</strong>
 * 
 * <br /><br /><hr />
 * 
 * <p><b>InputWords</b></p>
 * 
 * <p><b>Working at the command prompt:</b></p>
 * 
 * Create a new project folder <code><b>Words</b></code> and a subfolder <code><b>bin</b></code>, to be used as above.
 * 
 * <br /><br />
 * 
 * Make a copy of your <code><b>InputLoop.java</b></code> in the new folder, rename it
 * <code><b>InputWords.java</b></code>.
 * 
 * <br /><br />
 * 
 * Using <b>TextPad</b>, <b>{@code javac}</b> and <b>{@code java}</b>, design and implement the following changes
 * to the program:
 * 
 * <br /><br />
 * 
 * Modify the program so that instead of discarding inputs until an integer is found, it discards integers until a
 * non-integer (a "word") is found.
 * 
 * <br /><br />
 * 
 * Declare an array to hold ten <b>{@code String}</b> - these will be words input by the user.
 * 
 * <br /><br />
 * 
 * Input ten words from the user - the code that is in the main method, wrapped in a {@code for} loop and adapted
 * very slightly, will do this.
 * 
 * <br /><br />
 * 
 * Then input one further word.
 * 
 * <br /><br />
 * 
 * Search for the one further word in the array of words already input. Report back to the user whether or not the
 * word was found.
 * 
 * <br /><br />
 * 
 * Make sure that your program is formatted neatly, is clearly commented, and makes appropriate use of new methods,
 * and loops.
 * 
 * @author  Michael Sammels
 * @version 24.01.2019
 * @since   1.0
 */
package week2.practical1b;