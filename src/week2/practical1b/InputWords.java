package week2.practical1b;

import java.util.Scanner;

/**
 * CSCU9A2 - Week 2 <br />
 * Practical 1B <br />
 * <code>InputWords.java</code>
 * 
 * @author  Michael Sammels
 * @version 24.01.2019
 * @since   1.0
 */

public class InputWords {
    /**
     * Constructor.
     */
    public InputWords() {}
    
    /**
     * The main launcher method.
     * @param args  command line arguments (unused).
     */
    public static void main(String[] args) {
        // Setup the array to store the words in
        String[] myList = new String[10];
        
        readArray(myList);      // Function to read data into the array
        searchArray(myList);    // Function to search the array and output the results
    }
    
    /**
     * Read strings from the user and store the result.
     * 
     * <br /><br />
     * 
     * Do not display the valid input. <br />
     * Do not count invalid input.
     * @param a the array to read in.
     */
    private static void readArray(String[] a) {
        try (Scanner scan = new Scanner(System.in)) {
            // Loop 10 times
            for (int i = 0; i < a.length; i++) {
                // Ask for user input
                System.out.print("Enter word " + (i + 1) + ": ");
                
                // Store it in the array at index + 1
                a[i] = scan.nextLine();
            }
        }
    }
    
    /**
     * Search strings in the array and return the result case-insensitive.
     * 
     * <br /><br />
     * 
     * Returns word searched for to the terminal.
     * @param a the array to read in.
     */
    private static void searchArray(String[] a) {
        try (Scanner scan = new Scanner(System.in)) {
            int idx = 0;            // idx = index of the current string
            boolean flag = false;   // Flag to determine whether or not the string searched for was found
            
            // Ask for user input
            System.out.println("Search for word in the array: ");
            String search = scan.nextLine();
            
            for (int i = 0; i <= a.length - 1; i++) {
                if (search.equalsIgnoreCase(a[i])) {    // If the search (case-insensitive) has been found...
                    flag = true;                        // ...set the flag to true...
                    idx = i;                            // ...and store the current index
                }
            }
            
            if (flag) {
                // If the search was found, report back with the word searched for and the index it was found at...
                System.out.println("Your search for \"" + search + "\" was found at index  " + idx);
            } else {
                // ...otherwise, report back with the word searched for, and a "failed" message
                System.out.println("Your search for \"" + search + "\" was not found in the array");
            }
        }
    }
}
