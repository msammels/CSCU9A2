/**
 * <p>Practicals 1A and 1B</p>
 * <p><b>Aims:</b></p>
 * 
 * <b>Practical 1A</b><br />
 * This practical leads you through some Java exercises based on the practical classes from CSCU9A1, to give you the
 * opportunity to recall your Java knowledge and how to use BlueJ.
 * 
 * <br /><br />
 * 
 * <b>Practical 1B</b><br />
 * This practical introduces you to using Windows 7 at the command prompt, and gives you some Java exercises to be
 * carried out mainly at the command prompt.
 * 
 * @author  Michael Sammels
 * @version 21.01.2019 - 25.01.2019
 * @since   1.0
 */
package week2;